

Monitoring Component Analysis
*****************************

The *Monitoring Component* is responsible for collecting and forwarding state machine information. On a conceptual level, this is relatively simple, as figure :ref:`monitoring-component-analysis` indicates.


.. _monitoring-component-analysis:

.. figure:: /../images/MonitoringComponentAnalysis.pdf
    :width: 70%

    Monitoring Component Objects

	Monitoring component objects in an exemplary application with two state machines.


The state machines, when dispatching an event, forward the designated information to the *Event Log Collector*. That object filters and structures the information and creates *Event Log Items*, which are in the following delivered to the configured *Event Log Item Sender* and *Event Log File Writer*. 

The *Event Log Item Sender* is responsible for transmission of *Event Log Items*, one at a time, to the *Monitoring Application*. The transport mechanism used by the *Event Log Item Sender*, as demanded in section :ref:`Requirements - Subsystem Coupling<observatory-requirements-subsystem-coupling>`, is implemented in the *Event Log Item Sender* and not perceivable from the outside. Thus, there may exist different *Event Log Item Senders* that implement different transport technologies. 

The *Event Log File Writer* just stores all received *Event Log Items* in a file. That file can later be used in a replay session.

An important fact is that the *Monitoring Component* must perform its task in an efficient and fast manner, since it is running inside a potentially time- and space-restricted application.


Monitoring Application Analysis
*******************************

The *Monitoring Application* runs on a desktop computer and is thus not restricted by real-time conditions; an intuitive user interface and comprehensive state machine analysis capabilities are more important.


.. _observatory-domain-model:

Domain Model
============

The purpose of the *Monitoring Application* is to visualize state machine behavior in an application. Information about state machines include the state hierarchy and event types as structural properties, and reactions to events with associated state changes and action executions as behavioral data. 

The structural properties are defined in the model and are only partially available at runtime. Thus, they must be extracted from the model to be provided to the *Monitoring Application*.

The behavioral data specifies the reactions of a set of state machines to a given input stimulus. This is the data that is captured and transmitted from the *Monitoring Component* to the *Monitoring Application* for visualization, or restored from a recorded session.

Figure :ref:`monitoring-app-domain-model` shows the domain model of the *Monitoring Application*. The blue classes denote structural data, whereas the red classes signify behavioral data.

.. _monitoring-app-domain-model:

.. figure:: /../images/MonitoringAppDomainModel.pdf
	:width: 90%
	
	Monitoring Application Domain Model

	Structural data is shown in blue, behavioral data in red.


A *Session* is a collection of *StateMachines* that participate in an execution of a reactive application. The *StateMachines* hold their *StateMachineStructure* and a list of *StateMachineInstances*, whereof each represents one instance of a *StateMachine* class. Since different instances of the same state machine potentially run on different threads, *ThreadInfo* objects are attached to *StateMachineInstance* rather than *StateMachine*.

Each *StateMachineInstance* contains a list of *Reactions*, where a *Reaction* denotes a single run to completion step. Consequently, a *Reaction* has a list of *LogItems* such as «dispatch started», «action executed» or «guard evaluated», as described in section :ref:`Requirements — Captured State Machine Info<captured-state-machine-info>`.

*Reactions* are filled with *LogItems* as the *Monitoring Component* continuously supplies data. A complete *Reaction* consists of at least the *LogItems* «dispatch started» and «dispatch completed». If the state machine did handle the respective event, there is an arbitrary number of additional *LogItems* in between.


User Interface
==============

The *Monitoring Application* will be displaying a potentially large amount of data to the user. That data has time semantics and inter-state machine relations, so simply listing it in a tabular form seems not to do justice. Thus, it makes sense to think about data presentation early, so that no decisions are made in the process that could prevent a suitable and powerful user interface. 


.. _monitoring-app-timeline-sketch:

.. figure:: /../images/TimelineSketch.jpg
	:width: 100%
	
	Timeline Sketch


Maybe the most appropriate interface for analyzing temporal data is some kind of cartesian coordinate system with time on the x-axis. Since there are multiple state machines, they could be listed on the y-axis, which would provide an overview of the reactions of all state machines with appropriate visualization of the temporal relationships of the single reactions. 

Figure :ref:`monitoring-app-timeline-sketch` outlines that concept. The list on the left shows the state machines in the system along with the current state they are in. The timeline with the reactions is situated on the right side, where each reaction corresponds to one box, indicating the start time and duration of the reaction.


.. _monitoring-app-reaction-detail-sketch:

.. figure:: /../images/ReactionDetailSketch.jpg
	:width: 80%
	
	Reaction Detail Sketch


Detailed information about a single reaction could be shown when clicking or hovering the respective reaction box. This idea is shown in figure :ref:`monitoring-app-reaction-detail-sketch`.

Typically, the timeline will be used with a resolution of few milliseconds per centimeter, so that temporal differences between single reactions become tangible. Therefore, the timeline can grow very large in horizontal direction, rendering a typical scrollbar with its tiny handle useless; a more sophisticated scrolling mechanism is required for horizontal scrolling. A drag-move scroll area, for example, would most certainly be more user-friendly and intuitive than a standard scrollbar.

Another consequence of the very detailed resolution is that indicators must be provided so that the user does not get lost: A label containing the currently displayed time frame or a kind of a zoomed-out overview that shows the whole session would represent good orientation guides. Also, the ability to zoom in and out along the time axis could improve the user experience.


UI Technology Selection
=======================

Since the UI part of the *Observatory Monitor* has major importance, choosing an appropriate UI toolkit is critical. The UI technology of choice must support the following:

- Creation of complex custom widgets with reasonable effort.

- Reuse and customization of existing controls where possible.

- Good performance when displaying large data sets.

Two popular UI frameworks that fulfill these requirements are *Qt* and *Windows Presentation Foundation (WPF)*. Both of them are powerful frameworks, however, they also have some drawbacks:

*Qt* is implemented in C++ and requires the application to be written in C++, too. This has the benefit of making the resulting applications fast. But it also means that developing the application will be slower than with managed languages, because C++ is inherently settled on a lower level than managed languages like Java or C#. 

*WPF*, on the other hand, runs on the *.Net* platform, *WPF* applications can be written in C#. Nonetheless, *WPF* uses the GPU when rendering user interfaces, so it has no perceivable performance drawback. Developing the application in C# on the managed *.Net* platform is expected to be considerably faster than developing in C++. Also, there is an rich tool chain for both the programming tasks and the UI design.

But, *WPF* runs currently only on Windows, rendering the application useless for Linux and Mac users. 

For the application at hand it seems that the drawbacks of *Qt* outbalance those of *WPF*. Thus, *WPF* is the UI toolkit of choice for the *Observatory Monitor*.




