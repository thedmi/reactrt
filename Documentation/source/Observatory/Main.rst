
.. _observatory:

Observatory
###########


One of the main goals of this thesis is to investigate possibilities of monitoring reactive systems. This sounds trivial at first, given all the advanced debugging tools available in modern IDEs. However, the classical debugging approach is not well suited for reactive systems.

Time critical systems cannot just be suspended in the debugger, because the target system may rely on parts of the software. A suspended application cannot control a motor anymore, for example. Also, in cases where classical debugging is possible, problems in reactive systems are often not easy to detect, because they arise from the interaction of multiple state machines. 

State machine interaction cannot be seriously monitored in the debugger, because the debugger is on the wrong level of abstraction. Once the execution framework is in place, a developer does no longer want to fiddle around with event queues, threading boundaries and dispatch loops: What is of actual interest are the run to completion steps, executed actions, and performed state changes.

This chapter presents a solution to these problems: A monitoring system called *Observatory*.


.. toctree::
	:glob:

	Requirements
	Analysis
	Design

