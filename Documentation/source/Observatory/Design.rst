
:math:`\pagebreak`


Data Flow Design
****************

As indicated earlier, some information will be provided by the *Monitoring Component*, whereas other information must be extracted from the model. Figure :ref:`observatory-data-flow` shows the data flows from the model and the *Monitoring Component* to the *Monitoring Application*.


.. _observatory-data-flow:

.. figure:: /../images/ObservatoryDataFlow.pdf
    :width: 90%

    Observatory Data Flow


For the sake of this chapter, the connection carrying the dynamic data is called the *dynamic flow*, whereas the static state machine information transfer is called the *static flow*.


Dynamic Flow
============

The *dynamic flow* is produced in a potentially time-critical application on a potentially resource-scarce platform. Therefore, it is important that data processing and forwarding is lightweight.

A suitable data format would be a line-based delimiter-directed syntax. The following grammar specifies the proposed format:

.. code-block:: text

    event-item         = timestamp " " state-machine ": " event 
    
    state-machine      = class-name " ( " instance-id " '" [instance-name] "' ) "

    event              = event-type " [ " event-params " ] "

    timestamp          = unsigned-64bit-integer
    instance-id        = hex-string
    class-name         = alphanumeric-string
    instance-name      = alphanumeric-string
    event-type         = alphanumeric-string
    event-params       = alphanumeric-string


Since the language defined by this grammar is in fact regular, processing in the application can be performed by using the following regular expression (PCRE), which already includes group parentheses to select the interesting portions of the input string:


.. code-block:: text

    ^(\d+) (\w+) \(\s?([a-zA-Z0-9]+) '([^']*)'\s?\): (\w+) \[ (.*) \]


Here is an example message to clarify the format:

.. code-block:: text

    12949098661173240 Lamp ( 2e2700 'readyLamp' ): dispatchStarted [ sigOff ]


The timestamp is provided in microseconds according to the Unix ``timeval`` structure, which contains seconds (32 bit) followed by microseconds (32 bit) since Unix epoch. This resolution enables an accurate reproduction of the state machine behavior. 

The instance ID unambiguously identifies state machine instances. This is required because there may be multiple objects of the same state machine class.

The instance name may be an empty string and is thus optional. It can be provided by the user to have additional object information at hand.

The reason for preferring a custom data format over established standards like XML or JSON is the fact that the simple format above has no context that spans multiple lines. Thus, a receiver can hook on at an arbitrary line.


Static Flow
===========

As explained above, the *static flow* does not pass through the target application and must thus be provided "out of band" to the *Monitoring Application*. This is realized by generating the state machine information into an XML file, as outlined by figure :ref:`observatory-data-flow`.

The structure of the generated XML file is straight-forward. The following listing gives an example:


.. code-block:: xml
    :linenos:

    <?xml version="1.0" encoding="UTF-8"?>
    <reactRtStateMachines>
      <stateMachine name="StateMachine1" namespace="Example">
        <state name="State1" />
        <state name="State2" >
          <state name="State3" />
        </state>
      </stateMachine>
      <stateMachine name="StateMachine2" namespace="Example::SubPackage">
        <state name="On" />
        <state name="Off" />
      </stateMachine>
    </reactRtStateMachines>


Apart from the ``namespace`` attribute, everything should be self-explanatory. The ``namespace`` attribute can be used to find a state machine faster in a large model. It denotes the package path where the containing class is located.

Information on how and when this XML file is generated can be found in section :ref:`observatory-state-machine-instrumentation`.



Monitoring Component Design
***************************


With an purely object-oriented approach, the state machines need to provide access to the event log information. Since this information is largely event-driven itself, polling it is not a solution. Thus, every state machine, be it in the concrete class or one of the base classes, would contain observer callback code, even when there is no need for observation. Since the state machines perform potentially time critical tasks, this may be too much overhead.

Fortunately the state machines are generated, which opens up another possibility; conditionally generate observer callback code when it is needed. With that approach, the developer decides at code generation time whether observer code should be generated or not.

One fact to keep in mind with this approach is that the manually created code should not need to be changed when monitoring is switched on or off. For example, dependencies required for monitoring cannot be passed in through the constructor of the state machine classes.

The next few sections discuss the design of the *Monitoring Component* itself, followed by a section that provides information on the generator that establishes the instrumentation of the actual state machines.

.. _component-state-machine-observer:

State Machine Observer
======================

Figure :ref:`state-machine-observer` shows the design of the collaboration between state machines and the ``EventLogCollector``. 


.. _state-machine-observer:

.. figure:: /../images/StateMachineObserver.pdf
    :width: 90%

    State Machine Observer


This figure gives a hint that this is a nonstandard application of the observer pattern, because the observed objects acquire the observer instance themselves through the ``ObservatoryRegistry``, instead of the observer registering at the observables. In the case at hand this is the better solution, because there is no sensible way for the ``EventLogCollector`` to find all state machine objects otherwise.

Note that the state machines have no knowledge of the interface implementation, however; they see only the ``StateMachineObserver`` interface.


Event Log Forwarder
===================

Once the log data reaches the ``EventLogCollector``, it is structured and converted into ``EventLogItems``. These items are then passed to the registered instances implementing the ``EventLogForwarder`` interface. Figure :ref:`event-log-forwarder` shows the relations of the involved classifiers.

.. _event-log-forwarder: 

.. figure:: /../images/EventLogForwarder.pdf
    :width: 60%

    Event Log Forwarder


There are two classes realizing ``EventLogForwarder``. The ``EventLogFileWriter`` writes all incoming ``EventLogItems`` to a configured file, whereas the ``EventLogUdpBroadcaster`` broadcasts them over a network connection to a specified port. UDP is preferred over TCP for this application because the *Monitoring Component* can just "fire and forget", without managing connections.


RAII
====


To get the observer instance, the state machines need some kind of access point. As signified in figure :ref:`state-machine-observer`, this is realized using a singleton whose sole purpose is to act as a static access point. Solutions without a singleton seem unreasonable, because the state machines would need to get the observer as constructor or setter parameter, both of which require modifications in the client code.


.. _observatory-raii:

.. figure:: /../images/ObservatoryRaii.pdf
    :width: 90%

    Monitoring Component RAII


In order to set up the singleton as well as other resources, the monitoring component provides a single management class that sets up and tears down the component's resources. This is an application of the *Resource Acquisition Is Initialization (RAII)* idiom [Meyers05]_. 

Figure :ref:`observatory-raii` shows the interactions of the RAII class ``Observatory`` required for ``EventLogCollector`` instantiation as an example. 

.. _observatory-state-machine-instrumentation:

State Machine Instrumentation
=============================

Workflow
--------

Having the *Monitoring Component* itself in place, we can now turn to the generator, which is required to instrument the state machines so that they actually send information to the *Monitoring Component*. The instrumentation code must be generated only when the user requests it, so there must be some way of conditionally executing a part of the generator workflow. This is exactly what the ``IfComponent`` as provided by the *MWE* package does.

The parts of the workflow that are specific to the Observatory are organized in a dedicated workflow. Small workflow files that are meant to be used in a larger context are referred to as cartridges.

Consequently, the main workflow file ends up with only three additional lines of code:

.. code-block:: xml
    :linenos:

    <if cond="${addObservatoryInstrumentation}">
      <cartridge file="cartridges/ObservatoryInstrumentation.mwe" inheritAll="true" />
    </if>


Note that the condition in the ``<if>`` is a workflow variable. This enables the conditional inclusion of the cartridge based on an externally provided setting.

The observatory instrumentation cartridge itself contains two components: A ``Generator`` component required to produce the XML files for the *static flow* of state machine information, and a ``GeneratorAdvice`` component that non-intrusively extends the existing generator templates.


Templates
---------

Extending or even replacing existing generator code works through the *Aspect-Oriented Programming (AOP)* features of *Xpand*. AOP advices can be formulated by using ``AROUND`` advices for *Xpand* templates or ``around`` advices for *Xtend* functions. Every template and function definition provides an AOP join point, which can be queried using a simple point cut syntax in advices [EclipseDocsXpand]_. 

The advices for the Observatory instrumentation consist of both *Xpand* and *Xtend* advices and are located in the ``instrumentation/observatory/`` package in the generator sources.

The following listing shows the advice for the header files as an example. See the source code for the complete Observatory instrumentation templates.

.. code-block:: xpand
    :linenos:

    «AROUND templates::stateMachines::header::StateMachines::StateMachine 
            FOR uml::StateMachine -»
        «targetDef.proceed() -»
        «EXPAND Instrumentation::ObservatoryDeclarations 
            FOR instrumentedStateMachineOnly() -»
    «ENDAROUND»


Here, the string after the ``AROUND`` statement forms the point cut and selects unambiguously the ``StateMachine`` template definition in the specified package. 

In the body of the advice, the ``targetDef.proceed()`` expands the original template. Then, an additional template ``Instrumentation::ObservatoryDeclarations`` is expanded, which is an ordinary template definition. For completeness, it is given in the following listing:


.. code-block:: xpand
    :linenos:

    «DEFINE ObservatoryDeclarations FOR uml::StateMachine»
        // -- ReactRT Observatory instrumentation begin --
        private:
            «EXPAND ObserverPointerTypedef»
            «EXPAND ObserverPointer»
            «EXPAND StateMachineNameDeclaration»
            «EXPAND NotificationMethodDeclarations»
            «EXPAND SignalNameMethodDeclaration» 
        // -- ReactRT Observatory instrumentation end --
    «ENDDEFINE»


Ultimately, the generated code will be the code of the original template, followed by the additional code from the ``ObservatoryDeclarations`` template.

The whole set of Observatory advices generates the following additional code into instrumented state machine classes:

- A ``typedef`` for a shared pointer to the observer.

- A weak pointer to the observer, as outlined in section :ref:`component-state-machine-observer`. The actual observer is retrieved in the ``init()`` method of the state machine, because the constructor is provided by the user and not generated.

- A constant containing the name of the state machine. A state machine does not know its name otherwise.

- A lookup method ``observatorySignalName()``, which takes an event and returns a string corresponding to the ``enum`` name of the signal. This is required because in C++ it is not possible to resolve ``enum`` names form their values automatically.

- Notification methods, one for each kind of notification.


The instrumentation code in the header file is given in the following listing to clarify the approach. For the implementation code, see one of the examples.

.. code-block:: c++
    :linenos:

    // -- ReactRT Observatory instrumentation begin -- 
    private:
        typedef boost::shared_ptr<StateMachineObserver> ObservatoryObserverPtr;

        boost::weak_ptr<StateMachineObserver> observatoryObserver;

        static const char * OBSERVATORY_SM_NAME;

        void observatoryNotifyDispatchStarted(ConstEventPtr ev);
        void observatoryNotifyDispatchCompleted();
        // ... more notification functions ...

        const char * observatorySignalName(ConstEventPtr sig);
    // -- ReactRT Observatory instrumentation end -- 



Monitoring Application Design
*****************************

Layering
========

The *Monitoring Application* is essentially an application for data visualization. Thus, it seems appropriate to apply the somewhat standard layering approach with a layer for data access, one for the domain logic, and one for the presentation. 


.. _monitoring-app-layering:

.. figure:: /../images/MonitoringAppLayering.pdf
    :width: 70%

    Layers of the Observatory Monitor


Figure :ref:`monitoring-app-layering` shows these layers with the respective components inside. Each component represents one Visual Studio project, and each component produces one assembly.

Not shown in the figure above are the unit test component and the two launcher components, *Launcher* and *LauncherCmd*. While the former starts the user interface for an interactive session, the latter implements the command line interface. 

The two launchers have been realized as separate components for cross-platform interoperability reasons: Since the presentation layer relies on *WPF*, and *WPF* is not supported by *Mono*, it is not possible to run the UI-version of the Observatory on *Mono* [MonoCompatibility]_. However, the command line interface alone should be compatible\ [#MonoTarget]_. If it was implemented in an assembly that relies on *WPF*, as the *Launcher* obviously does, this compatibility would have been sacrificed. 

Another design decision that becomes apparent from the layer diagram above is that the data access layer provides an interface component, while the domain layer does not. This is due to the fact that the presentation layer has a comparatively tight coupling to the domain layer, because almost every property of every class is displayed in some way. Thus, introducing an interface package would create much boilerplate code. Since the user interface is not unit tested, there is no benefit in doing so.


Data Access
===========

Two data providers are implemented in the scope of this project: The ``UdpEventLogReceiver`` for live trace sessions and the ``FileEventLogReader`` for replay sessions. Both implement the ``IRawEventLogProvider`` interface. 

Forwarding event logs upwards in the layer stack works through a .NET CLR event\ [#DotNetRef]_. This event, which is called ``RawEventLogReady``, is defined in ``IRawEventLogProvider`` and carries raw event log strings as attribute. 

In order to support displaying progress information for file sources, the ``IsBulkDataProvider`` property and two events for progress notification are used in the file data provider. This information can later be visualized in the user interface as some kind of progress bar.


Domain
======

Figure :ref:`monitoring-app-design-domain-classes` shows the static structure of the domain as a reference for the following discussion of domain object interaction.

.. _monitoring-app-design-domain-classes:

.. figure:: /../images/MonitorDesignDomainClasses.pdf
    :width: 100%

    Observatory Monitor Domain Classes

    The interface to the data access layer is shown in blue (part of the data access interface component), the helper classes are shown in red. The actual domain classes are orange.


The sequence diagram in figure :ref:`monitoring-app-design-parse-sequence` outlines the propagation of raw event logs from the data access layer to the appropriate state machine.


.. _monitoring-app-design-parse-sequence:

.. figure:: /../images/MonitorDomainLogParseSequence.pdf
    :width: 100%

    Observatory Monitor Event Log Propagation

    The first two messages designate the loading of static state machine information. Then, raw event log processing is outlined for a single raw event log.


After a ``Session`` is started, the ``StateMachineSpecLoader`` is used to load the state machine structure information from an XML file. This information is thereafter stored as ``StateMachine`` objects with the corresponding tree of ``State`` objects. 

Then, event logs can be consumed. Event logs are received from an ``IRawEventLogProvider`` instance and processed by the ``Session`` object, which delegates the responsibility to parse the raw event log into a ``LogItem`` object to the ``LogItemParser``. The ``LogItemParser`` returns a new ``LogItem`` for each raw event log string.

Then, the ``Session`` looks up the state machine name of the new ``LogItem`` in its state machine dictionary. The ``LogItem`` is passed to the appropriate ``StateMachine``.


.. _monitoring-app-design-reaction-builder-sequence:

.. figure:: /../images/MonitorDomainReactionBuilderSequence.pdf
    :width: 100%

    Observatory Monitor Reaction Building

    This exemplary interaction shows the processing of two log items, whereof the second one triggers the completion of the reaction under construction.


The processing of ``LogItems`` through a ``StateMachine`` is shown in figure :ref:`monitoring-app-design-reaction-builder-sequence`. Each ``LogItem`` is forwarded to the ``ReactionBuilder`` of the appropriate ``StateMachineInstance``. The ``ReactionBuilder`` is responsible for assembling multiple ``LogItems`` into ``Reactions`` and contains the logic to decide when a ``Reaction`` is complete and a new one should be started. 

Whenever a ``Reaction`` is determined to be complete, it is forwarded by raising a ``Reaction Completed`` event. This event is handled by the ``StateMachineInstance`` that owns the respective ``ReactionBuilder``, subsequently the ``Reaction`` is added to the list of completed ``Reactions``.

The ``ReactionBuilder`` itself allocates a new, empty ``Reaction``, so that subsequent ``LogItems`` are attached to a new ``Reaction``. 

Note that this strategy for building the domain model out of the event logs is efficient, but imposes a special requirement on the parsed ``LogItems``: They must arrive in order, otherwise a ``LogItem`` may accidentally be added to the wrong ``Reaction``.

All data classes in the domain model expose their data through properties that can be accessed from the view. ``Reactions``, ``LogItems``, ``States`` and ``Sessions`` do not change once they are built. For ``LogItems``, ``States`` and ``Sessions`` this is simple, because all information is passed in through the constructor. Thus, they are effectively immutable classes. 

``Reactions`` are mutable as long as they are being filled up with ``LogItems``, but as soon as a ``Reaction`` is announced completed, it is immutable, too. This is enforced within the ``Reaction`` class. Note that the ``ReactionBuilder`` does not expose the ``Reaction`` under construction in any way. Thus, clients perceive ``Reactions`` as truly immutable at any time.

As a result, the immutable or perceived-immutable classes do not need to implement notification mechanisms for changing data. However, ``StateMachines`` and ``StateMachineInstances`` do change at runtime, namely when an instance is added or a reaction is completed. Both cases are signalled by raising an event, which enables the user interface to update its view accordingly.

Note that ``StateMachine`` and ``StateMachineInstance`` are the only two classes in the domain layer that are mutable as perceived by clients. Designing immutable data appropriately simplifies the view dramatically, because many properties must only be read once and are known not to change afterwards.
 

Presentation
============

As stated earlier, the presentation layer of the *Observatory Monitor* is realized with *WPF*. 

The views are designed with *Microsoft Expression Blend* and are coupled with the view logic through the Model-View-ViewModel (MVVM) pattern. Various implementations of this pattern exist. One of the more widespread implementations, which also has good support for *Expression Blend*, is *MVVM Light* [MvvmLight]_.

MVVM enables the separation of the view, a XAML file, from the view model, which contains the UI logic and is implemented in C#. Since MVVM is common in *WPF* applications, it is not further explained here\ [#MvvmIntro]_.


Session Selector and Main Window
--------------------------------

The *Presentation* component contains the general setup of the main window and the session selector, which is shown on startup. It enables the user to choose between Live Trace Mode and Replay Mode, and select a file containing the state machine structure (i.e. the static flow). Figure :ref:`session-selector-screenshot` shows a screenshot of the session selector.


.. _session-selector-screenshot:

.. figure:: /../images/screenshots/SessionSelector.png
    :width: 70%

    Session Selector


The main window itself contains just the menu bar and a reference to the timeline control, which encapsulates all controls that work on the state machine data.


Timeline Control Overview
-------------------------

The interesting part of the *Observatory* user interface is contained in the *TimelineControl* component. The control is fairly complex and contains various views and view models. In this section, the key parts of the timeline control are discussed.


.. _main-window-screenshot:

.. figure:: /../images/screenshots/MainWindow.png
    :width: 100%

    Main Window


Figure :ref:`main-window-screenshot` shows a screenshot of the complete *Observatory* window, which was taken while monitoring the *RailCars* example. 

The upper part makes up the control bar, where controls for live tracing and navigation are placed. Also, the current position of the cursor is displayed here, in the screenshot it is at an offset of two seconds and 522 milliseconds.

The middle section of the screen is used for the visualization of the state machine data: On the left, a legend of state machines is given, with the instances nested in a group for the state machine. The screenshot shows state machine «RailCar» expanded, «Terminal» and «SimpleTrackSection» are collapsed. A fourth state machine, «ConsoleLogger», is currently off screen. There are four «RailCar» instances, «RailCar A» through «RailCar D». 

Also, the state hierarchy is expanded for «RailCar A». The string in square brackets is the current state. This information is only interesting as long as a live trace is in progress.

The part to the right of the legend is the actual timeline.  Four reactions can be seen, two from «RailCar A», and two from «RailCar B». «RailCar A» changed its state from «Moving» to «WaitingForSectionClearance» and back again.

The background of the timeline consists of a ten millisecond grid. This makes it easy to analyze processing and event propagation delays.

The bottom part of the UI is occupied by the session overview. This is a zoomed-out view on the complete session, which is a bit longer than five seconds in this example. Each pin on the overview represents one reaction.


Reaction Details
----------------

Reactions on the timeline can be identified visually as initialization, state change or stationary processing. These three types are drawn in green, blue or red, respectively. But there is much more information available about each reaction.

By hovering a reaction, the exact time of occurrence and duration is displayed in a tool tip, both with a resolution of one microsecond. This tool tip also contains other important information, such as the trigger. 

As discussed in section :ref:`Observatory Domain Model <observatory-domain-model>`, a reaction consists of multiple log items. These log items are also available in the user interface by clicking on a reaction.


.. _reaction-detail-screenshot:

.. figure:: /../images/screenshots/ReactionDetail.png
    :width: 70%

    Reaction Details


Figure :ref:`reaction-detail-screenshot` shows the window that opens up when a reaction is clicked. This window contains all the information about a reaction that is available. The data grid lists all log items with absolute sequence number, timestamp, event type and additional event information.


Visualizing Nested Data
-----------------------

One of the bigger challenges was to visualize the instance lines and reactions on the timeline. The difficulty comes from the fact that the timeline must render multiple nested lists of data. The following tree indicates the data nesting according to the :ref:`Domain Model <observatory-domain-model>`:

- A *Session* contains many

	- *State machines* that contain many

		- *State machine instances* that contain many

			- *Reactions*

			- *State changes*

		- *State hierarchies*

So, conceptually speaking, the goal is to visualize lists of lists of lists, all in one view. Note that the state hierarchy is handled specially, because it needs to be repeated in each instance, so that the state changes can be drawn in a sensible way (see figure :ref:`main-window-screenshot`). 

The *WPF* controls for displaying list data are ``ListBox`` and ``ItemsControl``, where ``ListBox`` already contains templates that render the data into a selectable list, and ``ItemsControl`` is a kind of abstract control that must be templated. Since the data visualization on the timeline is highly customized anyway, it makes sense to use ``ItemsControl``.

The two important templates of ``ItemsControl`` are ``Template`` and ``ItemTemplate``. The former specifies how the list as a whole is rendered, the latter provides a way to define the appearance of single list items.

Since the ``ItemTemplate`` has access to the properties of the list item, another ``ItemsControl`` can be created within the ``ItemTemplate``. With this approach it is possible to create the required nesting, where the appearance each nesting level can be customized through the template.


Reaction Positioning
--------------------

The reactions and the state change lines must be positioned according to their timestamp. This is achieved by putting the reactions of each state machine instance in a ``Grid`` container that spans the whole lifetime of the instance. As a result, the ``Grid`` can grow very large in horizontal direction. Since the ``Grid`` itself does not need more memory if it gets larger, and the reactions in it are sparse, this approach works without excessive memory consumption.

Absolute positioning on the timeline is implemented with a ``Canvas`` for each reaction. The conversion between timestamp and pixel offset is implemented in a dedicated class called ``TimeConverter``. All views that display temporal data use this class, so it is guaranteed that they use the same scale.

Reactions can have a length of 0, so they may not be visible at all. To have reactions always visible on the screen, they are drawn with a minimum width of four pixels. But this fake width has the consequence that other reactions that follow right after the current may overlap. Thus, an overlap detection was implemented, which ensures that the reactions are visually drawn after each other. 

The timestamps always remain unchanged, so the user has the real timing information available.

Note that the same reasoning about object width and overlapping applies to the state change lines. The same solution has been adopted there.


Scrolling and Skipping
----------------------

Scrolling in horizontal direction is enabled in a drag-move fashion: The user grabs the background of the timeline, and drags it to the left or right. 

Since it can be cumbersome to scroll a larger area like that, a kind of scroll acceleration has been implemented. The user can grab and push the timeline, so that it continues to scroll in the indicated direction. The timeline continuously decelerates and stops eventually. This is similar to the scrolling mechanisms found in current smartphones. 

To implement this feature, the ``ScrollViewer`` class has been extended by specialization to get a ``FrictionScrollViewer``. The code for this class is based on a blog post that discusses that topic, see [Barber08]_.

Using this technique, scrolling the timeline becomes easy and intuitive. For larger scrolling distances, the user has also the possibility to change position by clicking on the desired position in the session overview, and the navigation buttons in the control bar enable skipping to the next or previous reaction.


Timeline Axis
-------------

The background of the timeline is implemented in the ``Axis`` view and view model. It provides the 10ms grid and the labels for the grid. While the grid itself looks always the same, the labels change with every 10ms increment.

At first, the naïve approach was taken: The complete session duration was computed, and then a collection of ``TimeSpan`` object was filled with as many ``TimeSpans`` as were necessary to fill the whole session with 10ms increments. For a ten seconds session, this meant that the collection held already 1000 elements.

The ``TimeSpan`` collection was then visualized in an ``ItemsControl`` that rendered each ``TimeSpan`` as a vertical line and the label, which looked exactly as expected.

However, it became clear that this approach is not a viable solution, because it obviously scales very badly. Tests showed that after two minutes in a live trace session, the application became unresponsive and was not usable anymore. Thus, a more intelligent approach to visualizing the axis was required.


.. _timeline-axis:

.. figure:: /../images/TimelineAxis.pdf
    :width: 90%

    Fixed-Size Axis Concept


An idea that proved to work out is illustrated in figure :ref:`timeline-axis`. The collection of ``TimeSpan`` objects is fixed to 30 elements. Their start position is at offset 0 (A). Then, as the user scrolls, the axis markers are moved (B) until they reach the start position of their neighbor. If that happens, the offset of the markers is set back to 0, so they jump back to their original position (C). In exactly that moment, the labels of the markers are updated with the next ``TimeSpan`` value (shown in red).

The jump and update of the marker text happens fast enough to not be perceivable by users. With this implementation, the memory required by the axis is constant instead of linear in the session duration.


Session Overview
----------------

The session overview at the bottom of the window has the unfortunate property that it needs to rescale its content every time a new reaction is added, because the last reaction should always be placed at the end of the axis arrow in the overview. Thus, the overview panel needs to be redrawn completely with each added reaction. This is by design.

There is a *WPF* control called ``ViewBox`` that has the ability to scale its child element to fit the control's size. This seemed like a good solution, because the content of the ``ViewBox`` could be laid out much like on the main timeline, and the ``ViewBox`` would scale it down to fit the overview panel automatically. Unfortunately, it scales everything, so the pins themselves were scaled down, too, squeezing them until they looked like a nothing more than a thin line.

One idea to fix this was to get the transformation matrix of the ``ViewBox``, and apply the inverse to the pins. This was not a good solution, however, because the inverse transformation would need to be applied N times whenever a reaction is added, with N denoting the number of reactions in the session.

A better approach, which is the one now implemented, is manual positioning without ``ViewBox``. Whenever a new reaction is added, all the reactions recalculate their position in the session as factor between 0 and 1. This value is then multiplied with the overview panel width to get the absolute offset, which is applied as a translate transform.

This approach is better from a performance point of view, because there is no matrix inversion involved, which would be a costly operation.

The vertical bar in the overview panel shows the current position. It also enables the user to skip to another position in the session. This is implemented using an ordinary ``Slider`` control that has been laid over the overview panel and is styled accordingly: The slider background has been removed, and the thumb replaced with a thin red line.


Command Line Interface
======================

:ref:`User Story 4 <observatory-user-stories>` requires that the *Observatory* has an automation-enabled interface, so that CI systems and test scripts can access state machine information. This is implemented through the command line interface of the *Observatory*.

The capabilities of the command line interface are basically the same as the GUI version offers. Event logs can be captured over UDP from a live trace session or from a file. The received event logs are then printed to STDOUT, so the client can analyze them.

To simplify usage in automated HIL tests, the command line interface offers the possibility to read only *n* event logs and then quit.

The following list shows the available options of the CLI:


| ``-u,  --udp``
|	Use UDP source.
|
| ``-p,  --port``
|	Listen on specified port (for UDP source only).
|

| ``-f,  --file`` 
|	Use specified file as source.
|
| ``-n``  
|	Number of raw event logs to wait for. Set to 0 for unlimited.
  

The option parser that is used to parse the specified CLI options is an open source library and is available on http://commandline.codeplex.com/.

Note that the CLI does not implement advanced event selection or filtering. After all, this is a command line interface, and there are established tools like *grep* available that are very good at that.





.. -------------------------------------------------------------------- 
.. FOOTNOTES


.. [#MonoTarget] Note that keeping the command line interface as cross-platform compatible as possible is a desirable thing in general. But, Mono is not considered a target platform in this project, and is thus untested.

.. [#DotNetRef] These events are a C# language construct and as such described in the .Net and C# reference documentation, see [DotNetRef]_.

.. [#MvvmIntro] A good introduction to the pattern can be found on http://en.wikipedia.org/wiki/Model_View_ViewModel .



