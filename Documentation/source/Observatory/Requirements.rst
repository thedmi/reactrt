
Requirements
************

To finally be more useful than an ordinary debugger, the *Observatory* must fulfill certain general requirements. These requirements cover the basic advantages over a debugger as described in the chapter introduction. After defining the general requirements, a list of user stories is presented to make the usage of the system clear. The user stories are followed by additional subsections that further detail them.


Functional Requirements
=======================

| **FR-1:** Monitored Entities
| 	The system must be able to identify state machines, instances and run to completion steps as primary monitoring entities. 
| 
| **FR-2:** Temporal Dependencies 	
| 	The system must be able to track temporal dependencies between single run to completion steps, so that state machine interactions can be analyzed.
| 	
| **FR-3:** Time Information 	
| 	The system must preserve delay information of run to completion steps and be able to visualize them according to the actual occurrence in the monitored application.


Non-Functional Requirements
===========================

| **NFR-1:** Minimal Interference
| 	The system must not delay state machines in a way that becomes significant for time critical applications. Of course, some processor time is needed, but the required computations must be delayed until the application has finished time critical processing tasks (e.g. on a separate low-priority thread).
|
| **NFR-2:** Non-Invasiveness 	
| 	It must be possible to turn the monitoring system on and off without changing client application code.


.. _observatory-user-stories:

User Stories
============


| **Story 1:** Debug Application
|	As a *Software Developer*, I want to observe the state machines of an application in real-time while debugging it, so that I can find bugs in the state machine designs.
|
| **Story 2:** Record State Machine Behavior
|	As a *Tester* or *End User*, I want to record an event log file of the state machines in a running application so that it can later be analyzed.
|
| **Story 3:** Replay State Machine Behavior
|	As a *Tester* or *Software Developer*, I want to replay a previously recorded event log file, so I can see how the state machines behaved and what actions they triggered.
|
| **Story 4:** Run Automated Hardware-in-the-Loop Test
|	As a *Software Developer* or *Continuous Integration System*, I want to execute automated HIL tests which trigger some state changes, and be able to observe those changes, so that I can verify if the state machines act as I expected.



Subsystems
==========


.. _user-stories:

.. figure:: /../images/ObservatoryUserStories.pdf
    :width: 100%

    Subsystems and User Stories


Since it must be possible to record state machine behavior at the *End Users* place as described by user story 2, there is a natural division of the *Observatory* into a *Monitoring Application* and a *Monitoring Component*; The former is used to inspect a currently running or recorded session, and the latter is used to record state machine behavior.

These two subsystems are shown in figure :ref:`user-stories`. This figure also shows which subsystem the user stories affect, and which actors perform which user stories.

|
|


Scenarios
=========

To get a deeper insight into the user stories, a main scenario is provided for each story.


Debug Application
-----------------

#. The *Software Developer* turns on monitoring support in the ReactRT generator.

#. The *Software Developer* generates the model of the debuggee and builds it, the *Monitoring Component* is linked into the debuggee.

#. The *Software Developer* starts the *Monitoring Application*.

#. The *Software Developer* starts the debuggee.

#. The *Monitoring Application* detects the *Monitoring Component* within the debuggee and establishes a connection with it.

#. The *Monitoring Application* displays information about the state machines in the debuggee.

#. The *Software Developer* performs the test actions.

#. The *Monitoring Application* continuously updates the information about the state machines in the debuggee.



Record State Machine Behavior
-----------------------------

This user story is performed by the actor *Tester* or *End User*. While the former may have some technical understanding of the application containing the *Monitoring Component*, the latter most probably has not. 

To enable the *End User* to activate the recorder as described in this scenario, the application must provide some kind of interface to access the recorder. Since this is application-specific, it is not part of the *Monitoring Component*. Here, access to the recorder for the *End User* is merely assumed.


#. The *Tester* or *End User* instructs the *Monitoring Component* to start recording state machine behavior and specifies a log target (e.g. save in a file, send through a network connection).

#. The *Monitoring Component* writes information about the state machines to the log target.

#. The *Tester* or *End User* uses the application in the normal way.

#. The *Monitoring Component* continuously writes state machine changes to the log target.

#. The *Tester* or *End User* instructs the *Monitoring Component* to stop recording, or the *Monitoring Component* detects a predefined limit is reached (e.g. log file size, elapsed time).

#. The *Monitoring Component* stops recording and signals completion to the *Tester* or *End User*.



Replay State Machine Behavior
-----------------------------

#. The *Software Developer* or *Tester* starts the *Monitoring Application* and chooses to playback a previously recorded event log.

#. The *Software Developer* or *Tester* specifies the event log.

#. The *Monitoring Application* displays the state machine information contained in the event log.



Run Automated Hardware-in-the-Loop Test
---------------------------------------

#. The *Software Developer* or *CI System* starts an automated test.

#. The test starts the testee, which contains the *Monitoring Component*, and the *Monitoring Application*. Then it begins with the test procedure.

#. The test input induces reactions in the state machines of the testee.

#. These reactions are detected by the *Monitoring Component* and reported to the *Monitoring Application*.

#. The *Monitoring Application* makes that information accessible to the test environment.

#. The test compares the information from the *Monitoring Application* with the expected values and decides if the test is a success or failure.


Note that the scope of this system is limited to the retrieval and parsing of the state machine information. Both the supply of test stimuli, as well as the interpretation of results within the CI system are application specific and thus not part of this project. The *Observatory* merely offers a suitable interface to retrieve state machine information.



Additional Requirements
=======================

.. _observatory-requirements-subsystem-coupling:

Subsystem Coupling
------------------

The two subsystems described above are connected by some message transport or a file exchange, depending on the user story. 

The message transport will usually be network based. However, since the *Monitoring Component* may run on a deeply embedded device with no network interfaces, alternative transports like RS/232 or CAN must be possible. 


.. _subsystem-coupling:

.. figure:: /../images/SubsystemCoupling.pdf
    :width: 90%

    Composite Structure of the Observatory System


Figure :ref:`subsystem-coupling` shows the two subsystems connected with a IP-based transport (shown in red). This transport should be implemented as part of this project, leaving an extension possibility for other transports through the interfaces *Event Log Sender* and *Event Log Receiver*.


Scripting Interface
-------------------

As indicated by the user story «Run Automated HIL Test», the *Monitoring Application* will be used by automated build systems to retrieve event logs. Thus, the *Monitoring Application* must have an interface that is suitable for scripting, and not just a GUI. This can be something as basic as a command line interface, though.


.. _captured-state-machine-info:

Captured State Machine Information
----------------------------------

This section gives an overview of the information captured by the *Monitoring Component*. The information items are prioritized: Very important information is shown in bold, not so important information is shown in parentheses.

Some information items are not event based. They should always be accessible, even if the state machines are not currently processing events:

- **Current state in hierarchy**

- Associated thread information

- (Event queue size)


The following information is associated with event processing, and is to be written into the event log:

- **Begin of dispatch**

- **End of dispatch**

- **Signal name**

- **State change**

- Event type (signal or time event)

- (Number of events remaining in the queue)

- (Event parameters)

- (Called actions)

- (Evaluated guards)
  






