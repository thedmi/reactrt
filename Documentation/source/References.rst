

.. [Barber08] Sacha Barber, "Creating A Scrollable Surface in WPF", Blog, 10.4.2008

    http://sachabarber.net/?p=225


.. [BoostSmartPtr] Greg Colvin, Beman Dawes, Darin Adler, "Boost Smart Pointers", 11.3.2009

	http://www.boost.org/doc/libs/1_46_1/libs/smart_ptr/smart_ptr.htm

	(retrieved 7.6.2011)


.. [Clinger81] William Douglas Clinger, "Foundations of Actor Semantics", 1.5.1981

	http://hdl.handle.net/1721.1/6935


.. [CppStdDraft11] C++ Standard Committee, "Working Draft, Stardard for Programming Language C++ (n3291)", 5.4.2011

	Section 20.7

	http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2011/n3291.pdf


.. [DotNetRef] Microsoft Corporation, "MSDN Library, .NET Framework 4.0"

	http://msdn.microsoft.com/en-us/library/w0x726c2%28VS.100%29.aspx


.. [EclipseDocsXpand] "Xpand Reference", Eclipse Helios Documentation

	http://help.eclipse.org/helios/topic/org.eclipse.xpand.doc/help/Reference.html

	(retrieved 27.4.2011)


.. [GoogleMock] Google, Inc., "Google Mock - Google C++ Mocking Framework"

	http://code.google.com/p/googlemock/

	(retrieved 8.6.2011)


.. [GoogleTest] Google, Inc., "Google Test - Google C++ Testing Framework"

	http://code.google.com/p/googletest/

	(retrieved 8.6.2011)


.. [Harel97] David Harel, Eran Gery, "Executable Object Modeling With Statecharts", July 1997

	IEEE Computer

	http://www.wisdom.weizmann.ac.il/~dharel/SCANNED.PAPERS/OOStatecharts.pdf


.. [ISO14882] C++ Standard Committee, "Programming Languages - C++"

	International Standard ISO/IEC 14882:2003(E)

	Section 12.7


.. [MdIntegrations] NoMagic, Inc., "MagicDraw Integrations User Guide", Version 17.0

	http://www.magicdraw.com/files/manuals/MagicDraw%20Integrations%20UserGuide.pdf


.. [MdOpenApi] NoMagic, Inc., "MagicDraw OpenAPI User Guide", Version 17.0

	http://www.magicdraw.com/files/manuals/MagicDraw%20OpenAPI%20UserGuide.pdf


.. [Meyers05] Scott Meyers, "Effective C++", Third Edition, 22.5.2005

	ISBN 978-0-3213-3487-9


.. [Michel11] Daniel Michel, "Code Generator for UML State Machines", 24.1.2011

	Semester Thesis, University of Applied Science Rapperswil


.. [MonoCompatibility] Novell Inc., "Mono Compatibility (Mono Website)", 6.10.2010

	http://mono-project.com/Compatibility

	(retrieved 2.5.2011)


.. [MvvmLight] Laurent Bugnion, "MVVM Light Toolkit"

	http://mvvmlight.codeplex.com/

	(retrieved 15.6.2011)


.. [OMG10] Object Management Group, "The Unified Modeling Language (Superstructure)", 
	Version 2.3, May 2010

	http://www.omg.org/spec/UML/2.3/Superstructure/PDF/


.. [RhapsodyDocs] IBM Corp., "IBM Rational Rhapsody User Guide"

	http://publib.boulder.ibm.com/infocenter/rhaphlp/v7r5/index.jsp

	(retrieved 24.5.2011)


.. [Samek08] Miro Samek, "Practical UML Statecharts in C/C++", Second Edition, 1.10.2008

	ISBN 978-0-7506-8706-5

	Chapters 6 & 7
