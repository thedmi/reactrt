
.. _testing-infrastructure:

Testing Infrastructure
######################

Unit tests are an important part of a software project, not only because they ensure the application does what it should, but also because they document how something needs to be used by client code. This fact leads to a property of unit tests that is often not paid enough attention to: Unit test code should be readable and easy to follow.

Mocking frameworks have improved this situation, because they provide a mechanism to express complex expectations rather than basic conditional tests. The expected behavior of a class can be configured on its required interface, which clearly indicates what the class is supposed to do.

However, tests for reactive applications face the problem of unreadable code for another reason: State machines are designed to run asynchronously, and concurrency in unit tests quickly leads to obscure code.

In order to counteract this situation, the *Reactor* framework is equipped with a testing support library that simplifies unit test development.

This section starts with a short introduction to the testing frameworks, followed by an introduction to the *Reactor* testing support library. Finally, continuous integration (CI) in MDSD solutions is discussed, along with the solution for CI in *ReactRT*.


Reactive Unit Tests
*******************

There are many mocking frameworks and even more unit test frameworks available for C++. Most of them offer similar functionality, so the main selection criteria were how widely used the framework is and whether it is under active development.


Unit Testing Framework
======================

*Google Test* is an established testing framework and has the advantage that *Google Mock* is specifically built to be used with *Google Test*, so the testing and mocking code integrates well. Also, it is actively maintained, has thorough documentation, and is employed in many large software projects, both within Google but also in independent projects.

Google Test organizes tests into test cases, so usually a test case for a class contains multiple tests for various features of that class. Defining a test is as simple as this:

.. code-block:: c++
    :linenos:

    TEST(MyTestCase, MyTest) {
        MyClass systemUnderTest;
        ASSERT_EQ(42, systemUnterTest.theAnswer());
    }


The ``TEST`` macro defines a new test called "MyTest" in the test case "MyTestCase". The test fails if ``systemUnderTest.theAnswer()`` does not return 42. 

*Google Test* contains a whole bunch of ``ASSERT`` macros for various types of comparisons. There are also ``EXPECT`` variants of these macros that produce a nonfatal error. This means that the test continues to run, even though the test condition specified in the ``EXPECT`` was violated [GoogleTest]_.


Mocking Framework
=================

With *Google Test* in place, it makes sense to use *Google Mock* as mocking framework.

Using a mocking framework has basically two advantages: The developers do not need to write stubs themselves, and, more importantly, the framework offers an extensive set of expectation verifiers.

Since C++ has no reflection, mock classes cannot be created automatically, but it is easy to do so manually. The following listing shows a simple interface ``ILogger`` along with a mock class for this interface: 


.. code-block:: c++
    :linenos:

    class ILogger {
    public:
        virtual void log(std::string message) = 0;
        virtual void flush() = 0;
    };

    class LoggerMock : public ILogger {
    public:
        MOCK_METHOD1 (log, void (std::string message) );
        MOCK_METHOD0 (flush, void () );
    };


The ``MOCK_METHOD1`` and ``MOCK_METHOD0`` macros are used to declare a monadic and medadic method to be mocked, respectively. They take the name of the method as first parameter, and the signature as second. Note that there are different macros for const and non-const methods, the former start with ``MOCK_CONST`` instead of just ``MOCK``.

Now that the mock is defined, it can be used in a test:


.. code-block:: c++
    :linenos:

    TEST(MyClassTest, TestStart) {
        LoggerMock logger;
        MyClass systemUnderTest(&logger);

        EXPECT_CALL(logger, log(StrEq("Started")));
        
        systemUnderTest.start();
    }


Here, the ``start()`` method of ``MyClass`` is tested. It must log the string "Started", otherwise the test fails. *Google Mock* verifies expectations just before destructing mock objects, so this test is verified at the closing brace of the test itself.

``StrEq()``, which compares the actual parameter of ``log()`` to the string "Started", is provided by *Google Mock* and is called a matcher. There are many matchers available to test different conditions on parameters [GoogleMock]_. 


Mock Generation
---------------

Note that the mock classes, such as the ``LoggerMock`` above, do not contain any additional information compared to the interface. Thus, they can be generated automatically.

The structural code generator (see :ref:`generator-for-structural-models`) has been extended for that purpose. A mock class is generated into the sub-folder ``testing`` for each interface. These classes can then be included from the unit test projects.

Since this mock generator is an extension that is not strictly required to use *ReactRT*, it is designed as cartridge that can be included from the main workflow. The respective eclipse project can be found in ``Sources/Generator/com.react-rt.gen.testing``.

The generator itself is relatively simple. It consists of a set of extension functions that determine the path, file name and class name for the mock, and the template that generates the actual code. The interesting part of the template is given in the following listing:

.. code-block:: xpand
    :linenos:

    «DEFINE TestingNamespace FOR uml::Interface»
        namespace testing {
            «EXPAND ClassDeclaration»
        }
    «ENDDEFINE»

    «DEFINE ClassDeclaration FOR uml::Interface»
        class «mockClassName()» : public «name» {
            «EXPAND MockMethods»
            
            typedef «pointerTo(mockClassName())» Ptr;
        };
    «ENDDEFINE»

    «DEFINE MockMethods FOR uml::Interface»
        public:
        «EXPAND MockMethod FOREACH mockedOperations() -»
    «ENDDEFINE»

    «DEFINE MockMethod FOR uml::Operation -»
        «mockMethodMacro()» («name», «returnTypeName()» («parameterDeclarationList()»));
    «ENDDEFINE»


The mock class is generated into the namespace ``testing``. This clearly separates it from the production code. Then the class is generated with its mock methods inside. 

The ``mockClassName()`` extension determines whether the name is interface prefixed (a large 'I' at the beginning), and removes it if so. Also, it appends the string "Mock" to the name. The mock name for the interface "ILogger", for example, becomes "LoggerMock".

The only new function in the ``MockMethod`` template itself is ``mockMethodMacro()``, the other functions are imported from the static code generator:


.. code-block:: xtend
    :linenos:

    mockMethodMacro(uml::Operation this):
        mockMacroBaseName() + arity();
        
    private mockMacroBaseName(uml::Operation this):
        let constDecl = isQuery ? "CONST_" : "":
            "MOCK_" + constDecl + "METHOD";

    private arity(uml::Operation this):
        parameters().size;


These functions produce the macro name that defines a mock method.


Reactive Testing Support Library
================================

This section discusses the helper classes that simplify and improve unit tests written for *Reactor*-based applications. All these helper classes are gathered in the library ``ReactorTesting``, which can be found in the ``Reactor`` solution in folder ``Sources/Frameworks/Reactor``.


Concurrency in Unit Tests
-------------------------

The following example shows a simple test. The system under test is a lamp, which uses a dimmer to control the brightness between 0 and 100% of the maximum. The test should verify that the reactive ``Lamp`` sets the dimmer to 0 on initialization and changes it to 100 when ``setBright()`` is called. 

This example uses smart pointers as defined in the respective ``Ptr`` typedefs, so it is closer to actual code written for the *Reactor* framework\ [#SmartPointerUsage]_.

.. code-block:: c++
    :linenos:

    TEST(LampTest, BrightTest) {
        DimmerMock::Ptr dimmerMock(new DimmerMock());
        Lamp::Ptr lamp(new Lamp(dimmerMock));

        EXPECT_CALL(* dimmerMock, setValue(Eq(0)));
        EXPECT_CALL(* dimmerMock, setValue(Eq(100)));

        lamp->start();
        lamp->setBright();
    }

First, a mock of the dimmer and the lamp are instantiated. The expectations are configured, and finally the lamp is started and used. The expectations are thereafter verified, because the mock object runs out of scope and is destructed. This seems like a simple and expressive test that should do its job as intended. 

Unfortunately this test fails, no matter if the lamp works correctly or not. This is because the lamp runs on a different thread, so the real sequence of events could be as follows:

1. The mock and lamp objects are instantiated.

2. The expectation is configured.

3. The lamp is started, so a thread is assigned to the lamp. That thread becomes ready and waits for processor time, **but does not yet run**.

4. The test scope exits, thus the mock is verified **before** the lamp thread had even a chance to run. Obviously, this verification fails.

5. The lamp thread becomes active, processes the initialization event and the ``setBright`` event, so the lamp sets the dimmer accordingly. This does however not matter anymore, because the test failed long before.

Note that the sequence depends on the scheduler strategy, so it may be different from the one given here. And this is exactly the point: The test is not reliable, because it depends on the scheduling.

There are two quick fixes for this problem: The first is to raise the priority of the lamp thread after creation, so that it always takes precedence over the test thread and preempts it. The second is to let the test thread sleep for a few milliseconds, giving the lamp thread time to perform its actions.

Both of them clutter the test with threading code that has nothing to do with the actual test. Also, the priority raise approach may not be reliable on some desktop operating systems, and the sleep approach requires a sleep after **every** call to the reactive object. 


.. _synchronous-testing:

Synchronous Testing
-------------------

The real problem of the test code above is not the scheduler, but the fact that the test is multi-threaded at all: A unit test should be a sequence of actions and verifications; concurrency is almost always a bad thing in unit tests\ [#SynchronousUnitTests]_.

Consequently, an appropriate solution should make sure the reactive object is run on the same thread as the test, making the whole test synchronous. With this idea in mind, a helper class ``Synchronous- FakeThread`` that implements ``IThread`` was created.

This thread implementation is fake because it does not really start a new thread to perform its task; the ``start()`` method is thus empty. 


.. _testing-sync-thread:

.. figure:: /../images/TestingSyncThread.pdf
    :width: 65%

    Synchronous Fake Thread Example


Figure :ref:`testing-sync-thread` shows the control flow of the lamp example when using the ``SynchronousFakeThread``. The sequence diagram clearly shows that enqueued events are immediately dispatched and processed on the same thread. Thus, the lamp is guaranteed to have all events processed when ``setBright()`` returns, so the mocks verify the intended state of the system and succeed.

Note that no event queue is required anymore in this particular example, because events are promptly dispatched. This is not true in the general case, however: If a state machine posts events to itself, a new RTC step is started out of an unfinished one. This is a violation of the RTC semantics, but can easily be fixed by queueing events when the reactive is currently processing another.

This approach leads to a reentry test. The following listing shows the ``enqueue()`` method:


.. code-block:: c++
    :linenos:

    void testing::SynchronousFakeThread::enqueue( 
            boost::shared_ptr<const rrt::Event> ev, 
            boost::weak_ptr<rrt::IEventProcessor> target ) {

        eventQueue.push_back(std::make_pair(ev, target));

        if(!isCurrentlyActive) {
            isCurrentlyActive = true;

            while(eventQueue.size() > 0) {
                TQueueElem next = eventQueue.front();
                eventQueue.pop_front();
                dispatch(next.first, next.second);
            }

            isCurrentlyActive = false;
        }
    }


Events are unconditionally pushed into the queue along with their target. Then, wrapped in the reentry test (lines 7, 8 and 16), the event queue is processed as long as there are events in it.

The ``enqueue()`` method is not thread-safe, neither is the event queue itself; they do not need to be, because this implementation is only used in single-threaded testing environments. Also, a non-atomic test-and-set approach works for the reentry test because of the same reason.


Short-Circuiting Time Events
----------------------------

Unit tests are intended to be run often, both on the continuous integration system and the developer workstation. Thus, the tests must terminate within reasonable time. 

Suppose a state machine with very large timeouts needs to be tested. Clearly, delaying the test until the timeout fires is not an option.

This problem is solved by replacing the normal ``TimeEventScheduler`` with the ``TimeEvent- SchedulerMock``, which does not care about the actual time, but sends the time event back immediately. The test can then continue without delay. Since direct post-back of time events may not always be desired, it can be disabled by calling ``disable()``.

As the name suggests, the ``TimeEventSchedulerMock`` itself is also a mock. Thus, expectations can be configured on it, so it is possible to verify the timeout durations, regardless of the fact that the respective time event is sent back immediately.

Note, however, that a state machine that uses multiple timeouts in a nontrivial way cannot be seriously tested with the ``TimeEventSchedulerMock``. It is thus recommended to create a dedicated time event scheduler for the particular test requirements in such cases.



Using Synchronous Testing
-------------------------

In order to simplify the usage of the above helper classes, a custom test extension for *Google Test* has been created. It comes in the form of a base class for test cases and is called ``ReactRtSyncTest``. 

Creating a test case with that base class is trivial. The following listing shows that, again with the lamp example from above:



.. code-block:: c++
    :linenos:

    class LampTest : testing::ReactRtSyncTest {
    };

    TEST_F(LampTest, Test1) {
        ...
    }


The test itself is the same as before, but now the macro for tests with fixtures ``TEST_F`` is used (see [GoogleTest]_ for more information on test fixtures). This has the effect that a new ``LampTest`` is instantiated for each test. The constructor of ``ReactRtSyncTest`` makes sure that the ``SynchonousFakeThread`` and ``TimeEventSchedulerMock`` are instantiated and registered properly, so the testing code can concentrate on the actual testing tasks.

The ``ReactRtSyncTest`` offers another feature for concise and readable tests: Mock objects can be registered for incremental verification using the ``registerForIncremental- Verification()`` method. The tests can then set expectations and call ``verifyAndClear- IncrementalMockExpectations()``, which does exactly what it says. This feature is very handy for reactive classes, because it enables stepwise testing, so errors are easier to locate.

To summarize, the following listing shows a complete unit test from the RailCars example. The example application can be found in ``Sources/Examples/RailCars/`` and contains an extensive set of unit tests, where all of the concepts of this section are applied.


.. code-block:: c++
    :linenos:

    class TerminalTest : public ReactRtSyncTest
    {
    protected:
        virtual void SetUp();
        boost::shared_ptr<LoggerMock> logger;
    };

    void TerminalTest::SetUp()
    {
        logger.reset(new NiceMock<LoggerMock>());
    }

    TEST_F(TerminalTest, SingleCar)
    {
        TrackUserMock::Ptr car(new TrackUserMock());
        TrackObserverMock::Ptr observer(new TrackObserverMock());

        Terminal::Ptr terminal(new Terminal(1, logger));
        terminal->setTrackObserver(observer);
        terminal->start();

        EXPECT_CALL(* car, getName() ).Times(AnyNumber());
        EXPECT_CALL(* car, notifyNextSectionClear() );

        {
            InSequence;
            EXPECT_CALL(* observer, entered(Eq(car)));
            EXPECT_CALL(* observer, left(Eq(car)));
        }

        terminal->requestAccess(car);
        terminal->enter(car);
        terminal->leave(car);
    }


Asynchronous Testing
--------------------

As stated earlier, integration and system tests may require asynchronous testing. However, the problems described in section :ref:`synchronous-testing` still persist.

For asynchronous tests it makes sense to define a completion call on one of the mock objects, meaning that the test is considered complete as soon as this call occurs. This is exactly what can be achieved with the two methods ``signalCompletion()`` and ``waitForCompletion()``. They are defined in the class ``ReactRtAsyncTest``, which forms the asynchronous counterpart of ``ReactRtSyncTest``.

``WaitForCompletion()`` blocks the test thread until ``signalCompletion()`` is called, so the other threads have time to process events. 

The following listing shows a ``ThreadTest`` from the *Reactor* framework itself. It should make the usage of ``ReactRtAsyncTest`` clear. The complete test sources can be found in ``Sources/ Frameworks/Reactor/ReactorTest/``. 


.. code-block:: c++
    :linenos:

    TEST_F(ThreadTest, SingleReactiveInit) {
        Thread::Ptr aThread(new Thread(10));
        EventProcessorMock::Ptr reactive(new EventProcessorMock());

        Event::Ptr initEvent(new Event(Event::INITIALIZATION_SIGNAL));

        EXPECT_CALL(* reactive, init())
            .WillOnce(InvokeWithoutArgs(this, &ReactRtAsyncTest::signalCompletion));

        aThread->start();
        aThread->enqueue(initEvent, reactive);

        EXPECT_TRUE(waitForCompletion());
    }


The expectation on lines seven and eight is configured to invoke ``signalCompletion()`` when ``init()`` is called, so this is the completion call. The test thread starts the target thread, enqueues the initialization event, and then blocks on line 13. As soon as ``init()`` is called, *Google Mock* calls ``signalCompletion()``, the test thread exits the scope of the test, and the expectations are verified.

Note that the blocking timeout can be specified as constructor parameter of ``ReactRtAsyncTest``. By default it is five seconds.



Continuous Integration
**********************

Since model-driven software development strongly relies on tools, continuous integration (CI) can be difficult to achieve in such environments. This subsection discusses the possibilities to implement CI in the context of *ReactRT*.


Scriptability
=============

CI systems are normally triggered through check-in operations on the source control system. Then, the whole software is built, unit tests are run and their results checked for errors. It is thus fundamental that the whole integration can be automatized from source control checkout to unit test execution.

The console-based tools like compiler and linker can be called from a script through the command line interface. The critical components with regard to automation, however, are the graphical tools, because they are not necessarily scriptable.


.. _integration-steps:

.. figure:: /../images/IntegrationSteps.pdf
    :width: 100%

    Integration Steps

    Automatized integration in a typical ReactRT-based application. The critical step with regard to scripting is highlighted in blue.


The only graphical tool that may be a problem with regard to scripting in *ReactRT*-based projects is *MagicDraw*, the UML editor. It needs to participate in the integration because it is required to export the model to *Eclipse (EMF) UML* files, an intermediate model format that can be used with the code generator [Michel11]_. 

Figure :ref:`integration-steps` summarizes the steps that are required for a complete integration.

Fortunately, two elegant solutions exist to make *MagicDraw* export the model: Exporting is possible from an MWE workflow, or through a custom *MagicDraw* plugin.


Eclipse MWE Integration
-----------------------

*MagicDraw* models can be exported to EMF files directly from the MWE workflow by using the oAW integration plugin. This plugin contains a workflow component called ``ExportWorkflowComponent`` that takes the path to a *MagicDraw* model and a target directory. Thus, the export can be accomplished as workflow step [MdIntegrations]_. 


Custom *MagicDraw* Plugin
-------------------------

The workflow component may not be an appropriate solution if the same workflow file is to be used on the development machines as well as on the CI system. In that case, a custom plugin can be created, which contains a single line of code in the startup handler:

.. code-block:: java

    EmfUml2XmiPlugin.getInstance().exportXMI(project, destination);


This has the effect that the model is exported on startup. Thus, the CI server can just start *MagicDraw* as part of the integration [MdOpenApi]_, and continue as soon as the export files are available in the file system.





.. -------------------------------
.. FOOTNOTES


.. [#SynchronousUnitTests] Note that although concurrency is not desired in unit tests, there may be other automatized test scenarios where it is required. Automatized integration tests, for example, could test the multi-threaded behavior of a larger part of the system.

.. [#SmartPointerUsage] See chapter :ref:`generator-for-structural-models` for more information on smart pointer usage in *ReactRT* applications.

