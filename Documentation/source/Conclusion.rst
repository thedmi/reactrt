

Conclusion
##########

Project Achievements
********************

State Machine Generation and Execution
======================================

After an extensive refactoring and reorganization of the generator sources, the way was clear to implement the missing features for state machines and structural models. While experimenting with the generated code, it soon became apparent that *QP* and smart pointers were difficult to get together. A test application for event parameters finally made it clear: There is no sensible way to combine the two.

The decision to replace *QP* and build the *Reactor* framework was a shift in the project goals. Since designing and implementing the framework needed a considerable fraction of the overall project time, not all project deliverables could be realized as planned. The reactive infrastructure was left out completely, and the *Observatory* did not get event injection. Also, only the basics of a continuous integration solution could be worked out, so there is no showcase for the integration of *ReactRT* and *Hudson*\ [#CiTasks]_. 

Nonetheless, implementing a custom execution engine was definitely worth the effort: The *Reactor* framework is specifically built for the generator solution and is a perfect match for the targeted project types. Code generated for the *Reactor* integrates in a clean and concise way with user code.

The fact that the *Reactor* is completely shared pointer-based enables a novel approach towards event pooling: Since the reference counting algorithm effectively shares ownership, the framework does no longer need to enforce a certain kind of memory management. This means that the developer can follow any memory management strategy that seems appropriate. In a simulation environment on a desktop system, for example, ordinary heap allocation is fine, whereas in the target environment an event pool could be implemented using custom deleter functions. 


Class and Interface Generation
==============================

The generator for structural models implements all elements that are necessary to build models for real-world software. Modeling constructs that are not part of the UML are realized as a set of stereotypes and tagged values in the *ReactRT UML profile*. In contrast to proprietary solutions as the property system in *IBM Rational Rhapsody*, this approach is fully UML compatible and thus not tool-specific.

The open generator templates encourage further extension and customization for project-specific needs. This enables teams to concentrate on the interesting parts of a project, leaving repetitive tasks to the generator. The small set of language-specific stereotypes in the *ReactRT UML profile* provides a showcase for such scenarios.

This openness is in my opinion the biggest advantage compared to other MDSD solutions for embedded software. Extensions to proprietary solutions are either not possible or require some kind of quirk. Thus, an open approach broadens the possible usage area considerably, and ensures that upcoming project requirements can smoothly be integrated in the generator.


Monitoring Reactive Systems
===========================

One of the biggest achievement of this thesis is the *Observatory* monitoring solution. It enables the analysis of reactive systems, possibly running in time-critical environments, by visualizing state machine data. This is accomplished in a unified view that shows temporal relationships of reactions, state transitions and reaction details. 

This innovative approach to real-time data visualization greatly simplifies the debugging of reactive systems. And since debugging normally consumes a lot of time, the *Observatory* has the potential to speed up embedded application development and therefore save costs.

The *Observatory* uses the existing generator infrastructure, and as such acts as a showcase for generator extensions. The combination with the generator enables the code instrumentation to be turned on and off at request, so the monitoring code can be removed from release builds, for example. 


Testing Infrastructure
======================

Writing good tests for reactive classes is difficult, because reactive classes have an asynchronous interface. Test code becomes awkward, since manual constructs are required in every test to synchronize with the reactive.

The testing support library for *ReactRT* provides a set of helper classes for reactive tests. Using this library, it is possible to write concise and reliable unit tests for reactive classes. This is possible by eliminating concurrency in the test. 

Looking back, I think that the testing library has a great benefit from the flexibility and modularity of the *Reactor* framework. An equally easy to use testing infrastructure would not have been possible with *QP* as execution framework. The reason for this lies primarily in the fact that the *Reactor* framework is modular and allows the replacement of the important core services from client code.


Modularity and Reusablility
===========================

Another big advantage of *ReactRT* over other MDSD solutions is the fact that modularity was high on the priority list: A project may only use the generator for state machines, and build the rest manually. Other projects could use only the *Observatory*, without other *ReactRT* components. Still others may want to use a modern, shared pointer based execution framework, but refrain from using a code generator.

All these scenarios are possible and realistic. Needless to say, they work best when used together, because the connections between the various components are already implemented.

:math:`\pagebreak`


Outlook and Improvement Opportunities
*************************************

My intention is to make the whole *ReactRT* tool suite available as open source project. This will hopefully help to establish *ReactRT* as a well-known solution within the embedded software engineering community.


Incremental Generation
======================

*ReactRT* covers many software development disciplines that can benefit from the model-driven approach. It is, in my opinion, ready for production use, albeit not for very large projects. 

This is due to the fact that partial generation of models has not been investigated within this thesis. Partial generation is a necessity for large projects. Without it, regeneration always traverses the whole model and recreates all output files, even for small changes. Note that *Xpand* offers support for incremental generation. This feature has not been examined within this thesis, however, so no statement can be given whether using it with *ReactRT* makes sense or not. 


Supported Platforms
===================

As already pointed out earlier, the platform-specific part of the *Reactor* framework is currently only implemented for desktop Windows systems. Implementing a platform adaption for *Reactor* is straightforward, so this can be done on demand during a project. The platform abstraction layers could then be made public through the *ReactRT* website, so that over time a broader set of platforms would be supported. This is where I see benefits from an open source distribution model.


Other Generators
================

The generator itself has very good support for state machines and class models, but there are still countless extension possibilities. From configuration modeling over support for different target languages, everything is imaginable. 

The only point to keep in mind when developing extensions is to implement them as generator cartridge, and not integrate them directly into the core. This ensures that users can choose which extensions to use, and which to leave out. The generators for mock classes and the *Observatory* instrumentation are good examples on how to implement a cartridge. 


Observatory User Experience
===========================

The *Observatory* in its current state is a great help for debugging reactive systems. Nonetheless, the user experience could be further improved by implementing some "comfort" features. It is currently not possible to search for or filter certain reactions, for example. Also, zooming could increase usability, and the possibility to display only a subset of a recorded session would help visualizing large sessions.




.. ---------------------


.. [#CiTasks] The critical steps required to implement CI with *ReactRT* have been investigated, though. See chapter :ref:`testing-infrastructure`.

