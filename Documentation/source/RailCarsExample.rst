
.. _rail-cars-example:

An Exhaustive Example
*********************

The *Rail Cars* example was created out of the need for a reasonably complex application with many interacting state machines. The idea for the example is based on the application in [Harel97]_, where a rail car system is developed with *Rhapsody*. Note, however, the application developed for *ReactRT* is fundamentally different because it uses a distributed approach, whereas Harel's implementation uses a central controller.

The purpose of this example is to provide a showcase for the whole *ReactRT* system. As such, the *Rail Cars* example uses both the state machine and structural generator, contains a set of unit tests that use the *ReactRT* testing infrastructure, and can be monitored through the *Observatory*.


Problem Description
===================

The basic setup of the system is given in figure :ref:`rail-cars-problem`. Four terminals, each consisting of three parallel platforms, are connected by single tracks to form a circle. There are four automatic rail cars on the tracks, two travelling clockwise, two counter-clockwise. Obviously, the cars can only cross in the terminals; just one car per connecting track is allowed. The challenge is to navigate the rail cars around the course without producing a crash.


.. _rail-cars-problem:

.. figure:: /../images/RailCarsProblem.pdf
    :width: 50%

    Rail Cars Problem

    Terminals are shown in blue, connecting track sections in red, and rail cars in green.


Note that **no deadlocks** can occur with this setup **on a conceptual level**: If a terminal is full and another car waiting to enter, one of the cars in the terminal is guaranteed to leave in the direction of the free exit.

The rail cars problem is interesting for reactive systems, because the tracks and terminals can be modeled as shared resources, with the cars acquiring and releasing these shared resources while they travel around the course. When approaching this problem with classical multi-threaded techniques, it is difficult to eliminate deadlocks that result from shared resource management. With active objects and state machines, however, the problem can be solved in a clear and concise way.

In order to not get lost in the details, some simplifications are applied to the rail cars problem:

- Rail cars are either on one track or another. It is not possible that the front of the train is on a connecting track, while the end is still in a terminal.

- It is assumed that the switches always connect to the right platform. 

To get to a valid start configuration, rail cars are placed in a depot on application startup (not shown in the picture above). They then move out to the track connected to the depot. Note that depots may be attached to connecting tracks or to terminals, and they are of infinite size.


Design
======

The rail cars application has been divided into the three layers «Infrastructure», «Domain» and «Application». This might seem like over-engineering, but it is useful in the example to show how larger software can be built with *ReactRT*. Note that the application layer contains only assembling and UI code and is thus not modeled in *MagicDraw*. 

The infrastructure layer consists of the ``ILogger`` interface and a ``ConsoleLogger`` class that implements it. The domain layer contains all the classes and interfaces that model the rail cars problem.


.. _rail-car-track-types:

.. figure:: /../images/RailCarTrackTypes.pdf
    :width: 80%

    Track Types


Figure :ref:`rail-car-track-types` shows the ``Terminal`` and ``SimpleTrackSection`` classes, the latter representing the connecting tracks between the terminals. They implement ``ITrackSection`` and have a common abstract base class ``AbstractTrackSection`` that handles the setup and tear down of the ring topology.

The ``ITrackObserver`` interface is used from the application layer to visualize the tracks. It is not relevant for the actual problem.

.. _rail-car-dependencies:

.. figure:: /../images/RailCarDependencies.pdf
    :width: 70%

    Rail Car Class


Figure :ref:`rail-car-dependencies` shows the ``RailCar`` class and its dependencies. The ``currentSection`` relation models the current position of the rail car on the tracks. The ``slowness`` member is initialized in the constructor and expresses the time that the rail car needs to travel to the next track section.


When following the active object pattern, shared resources must be encapsulated, so that their interface is asynchronous. Thus, not only the rail cars are modeled as reactive classes, but also the terminals and connecting tracks. The idea is that a rail car requests access to the next track section, and receives permission to actually enter the track section when it is free. 


.. _rail-cars-sm:

.. figure:: /../images/RailCar.pdf
    :width: 100%

    Rail Car State Machine


Figure :ref:`rail-cars-sm` shows the state machine of the ``RailCar`` class. The states on the left represent the initialization steps, the ``Active`` state on the right encapsulates the operational loop. A rail car is travelling along a track section while in state ``Moving``. This state is left when a time event, which is initialized with the slowness of the rail car, is fired. The rail car then requests access to the next section and waits for permission in ``WaitingForSectionClearance``. As soon as access is granted, it enters the track section and transitions back to ``Moving``.


.. _terminal-sm:

.. figure:: /../images/Terminal.pdf
    :width: 90%

    Terminal State Machine


Figure :ref:`terminal-sm` shows the state machine of the ``Terminal`` class. Each ``Terminal`` holds a list of waiting cars. Cars requesting access are enqueued in that list and in turn given permission to enter while the terminal is not in state ``TerminalFull``.

The state machine of the ``SimpleTrackSection`` is similar to the one of the ``Terminal``, so it is not reproduced here.


Testing
=======

The *Rail Cars* example implements a set of unit and integration tests using the :ref:`ReactRT Testing Infrastructure <testing-infrastructure>`. The tests are implemented in the *Visual Studio* project ``DomainTest`` and use the generated mock classes from the respective ``testing`` subfolders.

The following listing shows a part of the ``RailCar`` unit test as an example:

:math:`\pagebreak`

.. code-block:: c++
    :linenos:

    TEST_F(RailCarTest, AutoPilotForward)
    {
        TimeEventSchedulerMock::Ptr scheduler = getTimeEventSchedulerMock();
        LoggerMock::Ptr logger(new NiceMock<LoggerMock>());

        TrackSectionMock::Ptr section1(new TrackSectionMock());
        TrackSectionMock::Ptr section2(new TrackSectionMock());
        TrackSectionMock::Ptr section3(new TrackSectionMock());
        TrackSectionMock::Ptr section4(new TrackSectionMock());

        registerForIncrementalVerification(scheduler);
        registerForIncrementalVerification(section1);
        registerForIncrementalVerification(section2);
        registerForIncrementalVerification(section3);
        registerForIncrementalVerification(section4);

        ON_CALL(* section1, getNext()).WillByDefault(Return(section2));
        ON_CALL(* section2, getNext()).WillByDefault(Return(section3));
        ON_CALL(* section3, getNext()).WillByDefault(Return(section4));

        RailCar::Ptr railCar(new RailCar(section1, "TestRailCar", 1000, logger));

        // Leave the depot and request access to next section

        EXPECT_CALL(* section1, getNext()).Times(AtLeast(1));
        {
            InSequence;
            EXPECT_CALL(* section1, requestAccess(_));
            EXPECT_CALL(* section1, enter(_));
            EXPECT_CALL(* scheduler, scheduleTimeout(_, _, _));
            EXPECT_CALL(* section2, requestAccess(_));
        }

        railCar->start();
        railCar->startAutoPilot(true);
        railCar->notifyNextSectionClear();
        verifyAndClearIncrementalMockExpectations();

        // Proceed to section 2

        EXPECT_CALL(* section1, getNext()).Times(AnyNumber());
        EXPECT_CALL(* section2, getNext()).Times(AtLeast(1)); 
        {
            InSequence;
            EXPECT_CALL(* section1, leave(_));
            EXPECT_CALL(* section2, enter(_));
            EXPECT_CALL(* section3, requestAccess(_));
        }

        railCar->notifyNextSectionClear();
        verifyAndClearIncrementalMockExpectations();

        // Proceed to section 3
        // ...

        railCar->notifyNextSectionClear();
        verifyAndClearIncrementalMockExpectations(); 
    }


The details of the tests have already been explained in section :ref:`testing-infrastructure`. But the listing above shows how a step by step test that proceeds through a state machine can be implemented. The incremental verification approach is used to achieve this.


User Interface
==============

The *Rail Cars* example contains a *Qt* user interface that visualizes the rail cars as they travel through the system. This UI is implemented in the project ``ApplicationUI`` and is the only component that has dependencies to the *Qt* library. It is implemented completely in code, since the generator would not bring a real benefit to GUI code.


.. _rail-cars-screenshot:

.. figure:: /../images/screenshots/RailCarsWindow.png
    :width: 100%

    Rail Cars Screenshot


Figure :ref:`rail-cars-screenshot` shows the running application. Each track section is visualized with a list containing the cars that are on the section, and a list containing the waiting cars (grey). In the screenshot, the red and green car are currently both in terminal 3, and the red car is waiting to enter section 2, which is occupied by the blue car.

