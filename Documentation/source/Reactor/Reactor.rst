

Reactor
*******

In order not to be forced to talk about "the custom execution engine" all the time, I will give the framework the name *Reactor*.

A requirements analysis needs not to be conducted again, because the general requirements of an execution engine for this project were already established in [Michel11]_ and did not change since. The additional requirements imposed through the problem analysis above are briefly summarized here:

#. Enable multiple reactives on one active object in a clear and concise manner (i.e. according to figure :ref:`intuitive-active-sharing`).

#. Use smart pointers throughout the framework.

#. Separate portable code from platform-specific implementations.



Architecture
============

The *Reactor* framework consists of three libraries and two test applications, as figure :ref:`reactor-components` indicates.


.. _reactor-components:

.. figure:: /../images/ReactorComponents.pdf
    :width: 80%

    Reactor Components

    User libraries are shown in yellow, test applications in green.


The productive code is organized into a library called *Reactor*, which contains the platform-independent part, and a library called *ReactorWin32*, which contains the implementation of the platform-specific part for desktop Windows. This is the only platform that is supported as part of this thesis, but the component architecture ensures that new platforms can be added easily. All classes and interfaces in these two libraries are contained in the namespace ``rrt``.

The third library is a testing support library called *ReactorTesting*. It contains classes that simplify testing of *Reactor*-based applications. This library is discussed in section :ref:`testing-infrastructure`.

Finally, there are two test applications for the framework itself. The unit test application *ReactorTest* contains a set of automatic tests that assess the correctness of the *Reactor* library. Google Test and Google Mock are used as unit test framework. The *ReactorManualTest* application is a simple interactive test application.

All these libraries and applications are contained in a single Visual Studio solution. It can be found in the folder ``Sources/Frameworks/Reactor``.


Active Object Design
====================

When designing an event processing system with thread sharing among multiple reactives, the first approach may be to give each reactive object an event queue. After all, the events are destined for a specific reactive, so storing events at their destination seems logical.

However, this design has a significant drawback: If the thread reads from multiple queues to dispatch events, then it must be able to block on multiple queues in case they are empty. As soon as one of the queue is filled, the thread must awake and dispatch the new event. Blocking on multiple synchronization primitives simultaneously is however not trivial; on many operating systems this must be implemented manually.

To improve this situation, the event queue is simply attached to the active object rather than the reactive, so the thread effectively processes just one queue. Obviously, the reactive which the event belongs to must be stored along with each event.


Interfaces
==========

The portable part of the framework consists primarily of interfaces for asynchronous event posting, synchronous event processing and threading. The ``Reactive`` class is also completely portable, because it does not contain any thread synchronization code. Figure :ref:`reactor-reactives` shows the mentioned interfaces and the ``Reactive`` class.


.. _reactor-reactives:

.. figure:: /../images/ReactorReactives.pdf
    :width: 100%

    Reactives



The ``IReactive`` interface defines methods that concern the asynchronous interaction with a state machine, the two important methods here are ``start()`` and ``post(Event ev)``. This is the interface that the users of the state machine see, although they will normally work with the concrete state machine class directly.

The ``IEventProcessor`` represents the other aspect of the ``Reactive``, namely the ability to synchronously initialize the state machine and process an event. This interface ought to be used by the associated active object only.

``IThread`` defines the interface for a thread object within the *Reactor*. The most important method here is ``enqueue``. The reactives use it to put an event into the event queue and it takes two parameters: The event and a weak pointer back to the reactive, so that the thread knows which reactive the event must be dispatched to.

The ``Reactive`` class itself is relatively simple. Apart from implementing the interfaces it provides two protected methods for arming and disarming timers. They can be used from the state machine classes that derive from ``Reactive`` to schedule time events.


Threads and Event Queues
========================

Since the thread and event queue implementations require synchronization services from the OS, they cannot be implemented in a truly portable way. Thus, they are realized in the library ReactorWin32.


.. _reactor-thread:

.. figure:: /../images/ReactorThread.pdf
    :width: 95%

    Threads and Event Queues


Figure :ref:`reactor-thread` shows the relationship between these classes.

The thread class repeatedly calls ``getNext()`` on the event queue. This call blocks on empty queues to have the thread waiting when there are no events to dispatch. Once the call returns, the event is dispatched to the associated ``IEventProcessor``.

The interface contract of the queue itself is asymmetric: While calls to ``getNext()`` ought to block on empty queues, calls to ``insert()`` **must not** block when the queue is full. If they would, other state machines could be blocked in the middle of a run to completion step, which is a violation of the RTC semantics.

There are basically three alternatives for a nonblocking ``insert()`` implementation:

- Use an unbounded queue, so that it never actually can be full. This approach is implemented for the desktop version as part of this thesis, but is probably the wrong choice for an embedded target because of heap nondeterminism.

- Restrict the size of the queue and allocate all required memory at startup. This is a good solution for embedded devices, because it avoids allocating more memory during operation. The application should assert that there is always enough preallocated space and thus the queue cannot be full on ``insert()``. Cases where a queue is full must then be considered a system fault.

- The third possibility is the one applied in actors-based systems, where messages are not guaranteed to reach the destination, so the events can just be discarded if the queue is full [Clinger81]_. This is a fundamental change to the message delivery semantics that results in weaker service guarantees of the framework. As a result, it is not suitable for many systems, especially not for safety-critical applications.


Events and Reactives
====================

Event Posting
-------------

Since ``Event`` is a general type that is not specific to any particular state machine, it is basically possible to send arbitrary events to state machines. This complicates the usage of reactive classes, because the compiler cannot enforce that only the right events are sent to a state machine.

This situation can be improved by committing to the :ref:`Post To Self Rule <def1>`: 

.. _def1:

.. topic:: Definition 1

    Post To Self Rule
        Never invoke ``post(Event)`` on other objects. Use methods to provide an interface for clients, and post the event from these methods to ``this``.
        
        :math:`\vspace{0pt}`
  

Adhering to this rule has another benefit: Dependencies can be modeled through dedicated domain-specific interfaces. This is more expressive than using ``IReactive`` as interface and better testable than using the concrete reactive class. 

Also, in most cases it is irrelevant whether a dependency is implemented as state machine or not. Using ``IReactive`` as dependency would clearly violate the interface contract in these situations.

Note that although ``post()`` should not be called by clients directly, it can neither be removed from the ``IReactive`` interface nor made private in the state machine class because the time event scheduling mechanism relies on it (see below).


Event Propagation
-----------------

Figure :ref:`reactor-event-posting` shows how events are triggered by a client and propagate through the *Reactor* framework.


.. _reactor-event-posting:

.. figure:: /../images/ReactorEventPosting.pdf
	:width: 70%

	Event Propagation

	The activation boxes have been omitted in this diagram, because they distract from the thread context.


The client calls a method ``foo()`` on the state machine object, where the respective event is posted according to the :ref:`Post To Self Rule <def1>`. The reactive forwards the event to the associated thread, which inserts it into the event queue. Then, the call to ``foo()`` returns, because processing on the client thread (black arrows) is finished.

The thread of the state machine (red arrows) is signalled by the insert operation on the queue and eventually gets processor time. It extracts the event from the queue and passes it to the state machine by calling ``processEvent()``. This method consumes the event and performs a run to completion step.



Reactive Initialization
-----------------------

Figure :ref:`reactor-thread-start` indicates how a thread is started and the associated reactive is initialized. The client thread is signified by black arrows, the new thread by red arrows.


.. _reactor-thread-start:

.. figure:: /../images/ReactorThreadStart.pdf
	:width: 100%

	Thread Startup and Reactive Initialization


Here, the client retrieves a thread object from the ``IReactorFactory`` (see below). This object is then passed to ``start()`` of a reactive object. The reactive makes sure the given thread is started by calling ``start()`` on the thread object. This method does nothing if it has already been started.

Just after the start, the reactive enqueues a special initialization event. When the thread starts dispatching and sees the initialization event, it calls ``init()`` on the reactive instead of dispatching the event to it. This procedure is necessary so that the ``init()`` method is executed on the appropriate thread.



Time Stamping and Event Scheduling
==================================

Support for time events is crucial within reactive systems. Since timing information is required in many places\ [#ObservatoryTimestampProvider]_, it makes sense to provide an interface for exactly that in the *Reactor*.


.. _reactor-time-event-scheduler:

.. figure:: /../images/ReactorTimeEventScheduler.pdf
    :width: 65%

    Time Event Scheduler and Timestamp Provider


Figure :ref:`reactor-time-event-scheduler` shows the two important interfaces with regard to time:

- ``ITimestampProvider`` defines methods for monotonic time values and a method for an absolute timestamp. The implementation of this interface need to be adapted to the specific platform, so that accurate time values can be delivered.

- The second interface, ``ITimeEventScheduler``, is used by reactive classes to schedule time events. The scheduler takes an event, the designated target, and a timeout value in milliseconds, and enqueues the event in a priority queue, in which the calculated expiration time is used as the key.

Note that the time event scheduler performs its task by sleeping until the next expiration time, so it requires a dedicated thread that cannot be reused for reactive classes. Normally, the main thread can be used for this after the initial state machines have been created.


Dependency Resolution
=====================

Reactive classes need access to an ``ITimeEventScheduler`` as soon as time events are involved. Since the user will want to use constructor-based dependency injection for the problem domain, it would be cumbersome to supply the ``ITimeEventScheduler`` through the constructor. Indeed, the user does not normally care how time events are scheduled at all.

Thus, a registry seems the right design pattern for this problem: All reactive classes (and other clients as well) can acquire their *Reactor* related dependencies through the ``ReactorRegistry``, a singleton that holds commonly used objects in the *Reactor* framework.

The registry holds instances of the following interfaces in its current implementation:

- ``ITimeEventScheduler``

- ``ITimestampProvider``

- ``IReactorFactory``


The first two of them have already been introduced, the third is new. An ``IReactorFactory`` is responsible for creating ``IThread`` instances when they are needed: Either the client code can use the factory to create a thread, or invoke ``start()`` on a reactive without a thread instance, in which case the reactive invokes ``getDefaultThread()`` on the factory.


Startup and Shutdown
====================

The only thing that is left do define is how the ``ReactorRegistry`` actually gets its instances. This is what the ``ReactorConfigurator`` is there for. It creates instances of the above interfaces that match the current platform and registers them at the registry. This means that the configurator itself is platform-specific.

The goal of the configurator class is to simplify framework usage for the client: The configurator implements the RAII idiom (*Resource Acquisition Is Initialization* [Meyers05]_). Thus, the only thing the client needs to do to initialize the *Reactor* framework is to instantiate the ``ReactorConfigurator``, preferably on the stack to automate cleanup on shutdown.

After the framework is initialized, the user can instantiate the reactives, assign threads to them, and perform other initialization tasks. Finally, the user invokes ``Reactor::run()`` to pass control to the framework.

To shut down the application, a call to ``Reactor::stop()`` from any thread is enough: The time event scheduler will be stopped and the framework returns control. Since the *Reactor* uses smart pointers consequently, all resources will be freed when they run out of scope.




.. FOOTNOTES
.. -----------------------------------


.. [#ObservatoryTimestampProvider] For example, the Observatory needs time stamps while recording state machine behavior. See chapter :ref:`observatory` for more information.



