
Motivation
**********

In [Michel11]_, an evaluation of state machine execution frameworks, also called execution engines, was conducted. Out of four candidate engines, the *Quantum Platform (QP)* conformed best to the requirements and was consequently chosen for the project.

But during this master thesis, more and more problems and design weaknesses emerged and had to be worked around. The impact of the obstacles introduced by *QP* led to a complete reconsideration of the situation and finally to the removal of *QP*. This section discusses the reasons and consequences of this change. 

Even though this happened in the second half of the project, it is presented early in the documentation, because the execution engine forms the bottom layer of the state machine generator system and is therefore the basis of the topics discussed afterwards.

Note that the replacement of the execution engine with a custom solution was not intended to be part of the project. Since it required considerable amount of work, there was not enough time left for other topics that were planned.


Quantum Platform 
****************

Active Objects
==============

The *Quantum Platform* is targeted at embedded and real-time systems and claims to be suitable for very small to very large systems [Samek08]_. However, there is one restriction that seriously questions the suitability for very large systems: *QP* allows a maximum of 63 active objects.

This would not be such a bad restriction, if there was a simple and intuitive way to combine several passive state machines, or reactive objects, in the same active object. While this is possible, it is not at all intuitive.


.. _intuitive-active-sharing:

.. figure:: /../images/IntuitiveActiveSharing.pdf
    :width: 70%

    Flexible Active Object Sharing


Consider figure :ref:`intuitive-active-sharing`. Each reactive object is assigned to one active object, and the active object can be shared. The user of a state machine interacts with the reactive object that contains the state machine\ [#ReactivesTerminology]_. Events from the client are forwarded by the reactive to the queue of the associated active object. The thread of the active object can then process the queue and dispatch the events to the appropriate reactive.

This approach is common in reactive systems, as it employs a clear separation of concerns and thus enables reuse and flexibility. The execution engine of *IBM Rational Rhapsody*, for example, uses this approach [RhapsodyDocs]_.


.. _qp-active-sharing:

.. figure:: /../images/QpActiveSharing.pdf
    :width: 60%

    Active Object Sharing in QP


*QP* follows a different approach, as figure :ref:`qp-active-sharing` illustrates. Here, a reactive is promoted to an active object, so active objects cannot be shared by multiple reactive objects. Instead, the state machine in the active object must delegate certain (or all) events to the associate reactives. This, however, is something that the developer must implement, and is not provided automatically by the framework.

As a result, reactives are tightly coupled to the active object. There is only one feasible solution for the developer, which is to promote **every** reactive to an active object. It is clear that this is not the best approach from a performance perspective, however.

In addition to this, the *Quantum Platform* imposes the following restrictions on active objects [Samek08]_:

- There may be at most 63 active objects in the system.

- The priority of the active object corresponds to the priority of the thread the active object runs on. Each priority value can only be used once.

By observing the active object sharing approach and these limitations, it can be concluded that *QP* applications are effectively limited to 63 state machine objects, given the developer chooses not to use the cumbersome event forwarding to reactives as described above.



Memory Pools and Event Ownership
================================

In *QP*, two kinds of events are distinguished: Static events, which do not carry parameters and are allocated statically at program startup, and dynamic events, which do carry parameter and are instantiated through the macro ``Q_NEW``. The purpose of ``Q_NEW`` is to create an instance of the given event type using memory from one of the *QP*-managed memory pools [Samek08]_.

It is this ``Q_NEW`` macro that does not quite behave as one might expect: Instead of instantiating the event type at hand using *placement new* in the memory pool, a pointer to the memory chunk from the pool is simply casted into a pointer to the event type. According to the C++ standard, accessing such an object results in undefined behavior:

	«To form a pointer to (or access the value of) a direct nonstatic member of an object ``obj``, the construction of ``obj`` shall have started and its destruction shall not have completed, otherwise the computation of the pointer value (or accessing the member value) results in undefined behavior.» 
	
	-- \  [ISO14882]_, clause 12.7.2


Undefined behavior in the case at hand means that members of the event type are not initialized properly. While this may work for primitive types and raw pointers, it certainly breaks when using ``std::strings`` and smart pointers. An uninitialized ``boost::shared_ptr``, for example, tries to decrement the reference count of the current object before being assigned a new target, which results in an access violation\ [#QpMemoryPoolBug]_.

Since this project treats smart pointers as a fundamental concept and makes extensive use of them, there is clearly a conflict here.

An approach that seemed to provide a workaround for this situation was to reimplement the memory pools and completely ignore *QPs* pooling capabilities. While it was possible to initialize the custom events correctly, the experiment failed at the point where the state machine had finished processing the events: *QP* defines events to be owned by the framework after processing, so there is no way to give the memory back to the pool. 

Working without any pools at all is not possible either, because the allocated memory would not be freed up by *QP* after processing.


Minor Shortcomings
==================

The previous two subsections described two major drawbacks of the *Quantum Platform* as it is used in this project. There are other, although smaller inconveniences that influences the decision process.

One of them concerns time events. *QP* time events are allocated per object and must be initialized with the appropriate signal in the initializer list of the constructor, because time events are not default-constructible. Since the constructor is not generated code but provided by the user, this initialization needs to be performed manually.

Also, *QP* measures time in system time ticks, and consequently, timeouts must be specified in ticks rather than milliseconds. This is unintuitive for the state machine designer.


Weighting Problems, Workarounds and Solutions
=============================================

Now that the problems are analyzed, different solutions can be considered:

#. Patch the *QP* framework.

#. Reevaluate execution engines to check if another one suits the needs of this project better.

#. Develop a new execution engine from scratch.


**Solution one** seems to be the least involved approach. However, it also has the potential to become *very* involved, namely when the patches need to be ported to newer *QP* versions often. Also, patching other code has never been among my favorite ways of solving problems, although this may not count as a scientifically justified reason.

However, it is not possible to "patch away" all of the problems above anyway, because some of them are by design. Patches for them would undoubtedly spread throughout the whole *QP* code base.

Consequently, patching *QP* feels more like an ineffectual workaround than a solution.

**Approach number two** seems not to bring any real benefit: I do not think that the evaluation conducted in [Michel11]_ was incomplete or based on wrong assumptions; especially the memory pool bug was hidden in the implementation details of *QP* and not easy to find.

**The third solution**, implementing a self-made execution engine, is clearly the most involved one. But apart from fixing the memory allocation bug, it has other benefits as well:

- The design of the execution engine could implement the active object approach as depicted in figure :ref:`intuitive-active-sharing`, effectively enabling multiple reactive objects to share the same active object, and also have active objects of equal priority.

- The whole execution engine can be built using smart pointers, making the end system more homogeneous and concise: Since the generated structural models will be using smart pointers, implementing the execution engines with smart pointers too, is one potential pitfall less for the user of the generator.

- Time events can be implemented in a way that suits the generator better, so they do not have to be initialized by the user in the constructor. Also, the time unit could be chosen to be milliseconds.

Of course, when abandoning *QP*, the major benefit of *QP* is lost, which is its very good portability. This means that the custom framework must take portability into account, so that the platform-specific parts are contained in a exchangeable component.

Keeping the design of the generator interface parts similar to *QP* would cause the generator to remain mostly compatible. This must be respected in the new execution engine's design, so that not the whole generator needs to be reinvented.

The solution of choice is thus to build an execution engine tailored at the specific needs of this project.



.. ---------------------------------

.. [#QpMemoryPoolBug] I filed a bug report describing this problem on the *Quantum Platform* bug tracking system. However, the bug report was promptly recategorized as feature request with a statement that such «high-end applications» are not currently supported. The bug report can be found on http://sourceforge.net/tracker/?func=detail&aid=3300384&group_id=190182&atid=932317 .


.. [#ReactivesTerminology] Note that the terms "reactive object" and "state machine" describe basically the same thing. Here, the figures indicate that the state machine is somehow contained in a reactive object. This is how state machines are perceived from a high-level point of view, but in reality, state machines and reactives are the same object.

