

.. _react-rt-uml-profile:

ReactRT UML Profile
*******************

The UML profile for *ReactRT*, which is discussed in this chapter, is implemented in a dedicated *MagicDraw* project. This profile project can be loaded into the other projects as module to make the profile available.

The profile project file can be found under ``Sources/ModelLibraries/MagicDraw/``.


Type Library
============

The C++ runtime system comes with a set of predefined types, some defined as primitive types in the language standard, some as part of the *Standard Template Library (STL)*. Almost every real-world application uses these types somewhere.

If the types are to be used as class members, for example, the developer must be able to specify them in the model. This is what the type library in the *ReactRT* profile is made for.

Apart from the standard types, the type library also contains classes that are defined within the *Reactor* framework.


Primitive Types and STL
-----------------------

The packages ``BasicTypes`` and ``StlContainer`` in the profile contain definitions for primitive types, standard strings, and STL container types. 

The primitive types are modeled as *uml::PrimitiveType*. Note that separate elements are necessary to model the ``unsigned`` variants, so the integral types exist in fact twice, once as signed and once as unsigned type. 

The standard string and the container types are modeled as *uml::DataType*, because they are obviously not primitive types. *DataType* is preferred over *Class* because it makes the elements different from ordinary types as modeled by the user.

All types that are defined in the *STL* are stereotyped with «needsInclude» and the appropriate header, so that the includes are generated as soon as one of these types is used.


Reactor Types
-------------

It may be necessary to explicitly use one of the types of the *Reactor* execution engine. For example, an event deferral mechanism could be implemented manually, so the ``Event`` type would be needed in the model.

Thus, the platform independent *Reactor* types are included in the profile, too. They can be found in the package ``Reactor``.



Domain-Specific Modeling
========================

The UML profiling mechanism is well suited for implementing domain-specific UML extensions. Within this project, a language-specific extension has been created to support the development of C++ software. This extension serves also as a showcase for domain-specific extensions, which could be implemented in exactly the same way.

The goal of the extension was to simplify repetitive tasks and to customize the generator behavior. 

Note that some language-specific stereotypes like «needsInclude» were already introduced above and will not be described here again.


Avoiding Repetitive Modeling Tasks
----------------------------------

Getters and Setters
~~~~~~~~~~~~~~~~~~~

Some languages, such as C# and Scala, have the ability to encapsulate fields by language design. In these languages, fields can be implemented in a way that enables the execution of code whenever the field is read or written to.

Languages that do not support this feature require the developer to make the field private and implement accessor and mutator functions, also called getters and setters, to achieve that encapsulation. C++ is one of those languages. Clearly, most accessors and mutators are boilerplate code; they do not add any information to the program. Consequently, it makes sense to generate them.

The *ReactRT* profile contains the two stereotypes «getter» and «setter» for this purpose. If they are set on a property, the generator produces additional getter and setter functions, respectively. Since only the header is generated, they are generated as inline functions.


Data Classes
~~~~~~~~~~~~

Some classes serve only as data structures, their only purpose is to hold data that belongs together. They are normally implemented as classes that have only fields, and these fields are accessible by getters, and if the field is mutable, also setters. Further, the data class usually provides a constructor that sets all fields during object initialization.



.. _data-class-example:

.. figure:: /../images/EventLogItem.pdf
    :width: 40%

    Data Class Example



However, stereotyping all properties in a class with «getter» and «setter», and creating a constructor that contains all the properties as parameter, is yet another repetitive task. Therefore, the stereotype  «dataClass» can be set on classes that should act as data structures. The constructor and the getters and setters are then generated automatically.

Figure :ref:`data-class-example` shows a class from the *Observatory Monitor* (see chapter :ref:`Observatory`) that is implemented using the «dataClass» stereotype. Note that all fields are marked as read only, so they can only be set in the constructor; no setters are generated for them. 


Customizing the Generator Behavior
----------------------------------

During the development of the code generator, many decisions had to be made on how to transform the model elements to code. Sometimes, even though the decision generally makes sense, the developer needs to deviate from the standard. 

A common example of such required customization concerns one-to-many relations. Which container type is the most appropriate one represent these relations? For the most cases, ``std::vector`` should do, so this has been appointed the default container. However, the user needs to select other containers when he needs, for example, one that sorts the elements.

This customization possibility is again implemented with the help of stereotypes: To customize a container, the developer just applies the «customContainer» stereotype. This stereotype contains a tag "container" of type *uml::Classifier*, which can be used to specify an alternate container from the type library.



Implementing Domain-Specific Languages
--------------------------------------

This section showed that custom extensions are easy to implement using the profiling mechanism of the UML. Clearly, the extensions developed here are rather simple and of a broad scope, but they should demonstrate how to build powerful domain-specific languages with this mechanism.

For this purpose, it is possible to extend the generator without changing the existing packages through the use of AOP, as indicated earlier. 


