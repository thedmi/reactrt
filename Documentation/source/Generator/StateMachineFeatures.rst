

State Machine Generator Extensions
**********************************

Before diving into the state machine generator itself, the next section introduces the generator reorganization that became necessary. The new state machine features are the topics of the following sections.


.. _generator-reorganization:

Generator Reorganization
========================


The code generator is considerably larger than in the previous thesis. With the generator templates growing and growing, the package structure used before became gradually less usable. Thus, a major refactoring was performed to improve structure and reusability. The most important reorganization is the division of selector extensions and code assembling extensions.

Selectors are extensions that facilitate model traversal. They complement the Eclipse UML implementation by providing concise model element accessors, queries and filters. As such, they are not specific to any generated code, they are merely used to keep the templates clean and readable. Because of that code-independent nature, it makes sense to completely separate them from the templates and extensions that are target code-specific.

Thus, the generator sources have been reorganized into three basic packages:

- ``selectors`` – Contains all selector extensions. They are further divided into packages as specified by the UML, such as ``classes``, ``commonBehaviors`` and ``stateMachines``. The extension files are organized by meta class, and all functions in a file either return instances of the respective meta class, or perform a query on instances of the respective meta class. These strict organization rules enables faster selector discovery. 
  
- ``extensions`` – Contains code-specific extensions. They are organized in a way that functions commonly used together are in the same file. Some of these extensions use the selector extensions to perform their task.

- ``templates`` – Contains the generator templates, which, naturally, are always code-specific. They are divided into templates for structural models and templates for state machines.

By separating the selector functions the generator becomes useful for other generator tasks than producing C++ code. For example, a developer could decide to build a report generator that dumps statistics about the model into an html file. The selectors will simplify that task considerably, because much of the model traversal capabilities of the selectors can be reused.

The ``selectors`` package is contained in its own *Eclipse* project called ``com.react-rt.gen. selectors``. This advocates reusability. The rest of the generator has been reorganized in the same manner: The core of the generator with the ``extensions`` and ``templates`` packages is now in the ``com.react-rt.gen.core`` project, and the project ``com.react-rt.gen.main`` contains only the workflow, it acts as a kind of configuration project that pulls in other projects as required.

Other generator extensions that are described in this thesis are also realized in dedicated projects.



Missing Features
================

As described in [Michel11]_, three important state machine features were still missing for the generator to be useful in real world projects:

- Internal transitions

- Arbitrary event parametrization

- Time events

In the following, the modifications made to the generator in order to support these features are discussed. The generator sources can be found in the main and selector projects, which are located in the folder ``Sources/Generator/``.


Internal Transitions
====================

Internal transitions are transitions contained in a state that do not trigger a state change. This enables the execution of actions without affecting the current state of the state machine. 


.. _internal-transition-example:

.. figure:: /../images/InternalTransitions.pdf
    :width: 50%

    Internal Transition Example


Figure :ref:`internal-transition-example` illustrates the concept. Here, ``action()`` will be called whenever an event ``evX`` arrives, without affecting ``InnerState1`` or ``InnerState2``. Note that this is different from an external transition from ``OuterState`` to itself, because such a transition would re-initialize the inner state of ``OuterState`` to ``InnerState1``.


Generator Templates
-------------------

Since the generator was already able to find relevant transitions for a specific state, implementing internal transitions is straight-forward. Note that the header file templates did not need to be modified, because action wrappers for internal transitions were already generated correctly in the header. Thus, only implementation file templates are discussed in this section. 

First, a pair of functions is needed to differentiate the two transition types. This is realized using the *Implicit Null Checking Idiom* [Michel11]_ in the file ``selectors/stateMachines/transitions/ GeneralTransitions.ext``:


.. code-block:: xtend
    :linenos:

    externalTransitionOnly(List[uml::Transition] this):
        if first().kind == uml::TransitionKind::external then this;

    internalTransitionOnly(List[uml::Transition] this):
        if first().kind == uml::TransitionKind::internal then this;


As can be seen in the listing above, the distinction can be made based on the ``kind`` attribute of the transitions.

The functions work on a list of transitions because external transitions may be compound. Internal transitions are never compound, but the functions need the same signature for clarity in the templates.

With the selector functions in place, the templates need a point of conditional expansion, which is located in the file 
``templates/stateMachines/implementation/Transitions.xpt``:


.. code-block:: xpand
    :linenos:

    «DEFINE CompoundTransition(uml::State affectedSimpleState) 
        FOR List[uml::Transition] -»
        «EXPAND ExternalTransitionActionSequence(affectedSimpleState) 
            FOR externalTransitionOnly() -»
        «EXPAND InternalTransitionActionSequence(affectedSimpleState) 
            FOR internalTransitionOnly() -»
    «ENDDEFINE»


The ``ExternalTransitionActionSequence`` corresponds to the existing template for extenal transitions and was left unchanged. The new template ``InternalTransitionActionSequence`` is given in the following listing:


.. code-block:: xpand
    :linenos:

    «DEFINE InternalTransitionActionSequence(uml::State affectedSimpleState) 
        FOR List[uml::Transition] -»
        «EXPAND InternalTransitionComment -»
        «EXPAND TransitionAction FOREACH this -»
    «ENDDEFINE»


Thus, only the transition action call is generated, but the state entry and exit action calls as well as the state pointer reassignment are omitted.


Arbitrary Event Parameters
==========================

When applying a pure asynchronous design, state machines interact only through events. This means that any information that needs to be stored in a state machine object must be encapsulated in an event to be sent to the state machine. Thus, events must be able to carry data of any type, including user-defined types. 

Note that in UML the event parameters are attached to the *Signal* meta class, and not the *SignalEvent* [OMG10]_.


Custom Event Type Definitions
-----------------------------

Regarding code generation, parametrized events differ in two ways from parameterless ones: Firstly, the event cannot be instantiated as class member, since the parameter requires each object to be unique. Secondly, a new subtype of ``Event`` must be created to accommodate the parameters.

This event type needs at least the following: 

- A constructor that initializes the base class with the appropriate signal

- An (empty) virtual destructor

- One member variable for each event parameter

The following listing shows a hypothetical event type which could be used to register an observer in a state machine:


.. code-block:: c++
    :linenos:

    class ObserverRegistrationEvent : public rrt::Event {
    public:
        ObserverRegistrationEvent() : rrt::Event(sigObserverRegistration) {}
        virtual ~ObserverRegistrationEvent() {}

        boost::shared_ptr<Observer> observer;

        typedef boost::shared_ptr<ObserverRegistrationEvent> Ptr;
        typedef boost::shared_ptr<const ObserverRegistrationEvent> ConstPtr;
    };


This event type has one parameter, which is a shared pointer to the observer. The two ``typedefs`` are used in the modified action wrappers, which will be explained shortly. Also, they can be used in the client code to improve readability.

Note that the class has no private members, especially the ``observer`` member is exposed directly, rather than through a getter and a setter. This makes sense, because events are only data containers, nothing more. Besides, the event processing code always gets a ``const`` pointer to the event, so the data can only be read there.

Custom event types are related to the state machine that processes them, so it makes sense to place the custom event definitions inside the respective state machine class as an inner class. The code example above would thus be encapsulated in the class definition of the respective state machine.


.. _action-and-guard-wrappers:

Action and Guard Wrappers
-------------------------

Consider figure :ref:`parametrized-event-example`, which shows an excerpt of a state machine that reacts to the event ``evButtonPressed`` containing an ``enum``-typed parameter ``buttonId``. 


.. _parametrized-event-example:

.. figure:: /../images/ParametrizedEvents.pdf
    :width: 70%

    Parametrized Event Example


The action and guard code that is shown in the diagram is generated into a wrapper method, which gets the event as ``const`` ``Event`` pointer\ [#WrapperMethods]_. However, this event type is not specific enough anymore, because the action and guard code is accessing the event parameter, which requires the event to be cast down to a ``const`` ``ButtonPressedEvent`` pointer. 

There are two possibilities to handle this situation:

1. Leave the responsibility to cast the event to the appropriate type to the user.

2. Perform the cast before the event is passed to the wrapper, and use the concrete event type in the formal parameter of the wrapper.

Obviously, the first variant is a very uncomfortable solution, because the code for the cast would show up in the state machine diagram and clutter it a lot. Fortunately, the concrete event type is known at the place where the wrappers are called, enabling the cast to be performed on the caller side. Thus, the cast can be generated in a way that is guaranteed to be safe.

The following implementation code fragments illustrate the approach:


.. code-block:: c++
    :linenos:

    void Example::transition_On_Idle_evButtonPressed(ButtonPressEvent::ConstPtr ev) {
        log(ev->buttonId);
    }

    void Example::transition_t2(ButtonPressEvent::ConstPtr ev) {
        handleButtonPress(ev->buttonId);
    }

    bool Example::guard_t1(ButtonPressEvent::ConstPtr ev) {
        return ev->buttonId == Buttons::Stop;
    }

    void Example::Sm::On::Idle(Context context, ConstEventPtr ev) {
        switch (ev->sig) {
            case sigButtonPressed:
                if(context->guard_t1(
                    boost::shared_polymorphic_downcast<const ButtonPressEvent>(ev))) {
                    
                    context->transition_On_Idle_evButtonPressed(
                        boost::shared_polymorphic_downcast<const ButtonPressEvent>(ev));
                    context->currentState = Sm::Off;

                } else {
                    context->transition_On_Idle_evButtonPressed(
                        boost::shared_polymorphic_downcast<const ButtonPressEvent>(ev));
                    context->transition_t2(
                        boost::shared_polymorphic_downcast<const ButtonPressEvent>(ev));
                    context->currentState = Sm::On::Idle;
                }
                break;
        }
    }


As can be seen, a ``boost::shared_polymorphic_downcast`` is required in the guard call as well as in each of the transition action calls. This template function performs a downcast on a shared pointer, using ``dynamic_cast`` in debug builds and ``static_cast`` in release builds behind the scene [BoostSmartPtr]_. 



Header File Generator Templates
-------------------------------

After the concepts for the generated code have been explained, this section provides information about the generator templates for the header, and the next section for the CPP file. 

The following listing shows the new templates for the header file of the state machine class:


.. code-block:: xpand 
    :linenos:
    
    «DEFINE CustomEventType FOR uml::Signal»
        class «eventTypeName()» : public rrt::Event {
        public:
            «EXPAND CustomEventConstructor»
            «EXPAND CustomEventDestructor»
            «EXPAND CustomEventAttribute FOREACH ownedAttribute»
            typedef boost::shared_ptr<«eventTypeName()»> Ptr;
            typedef boost::shared_ptr<const «eventTypeName()»> ConstPtr;
        };
    «ENDDEFINE»


    «DEFINE CustomEventConstructor FOR uml::Signal»
        «eventTypeName()»() : rrt::Event(«signalName()») {}
    «ENDDEFINE»


    «DEFINE CustomEventDestructor FOR uml::Signal»
        virtual ~«eventTypeName()»() {}
    «ENDDEFINE»


    «DEFINE CustomEventAttribute FOR uml::Property -»
        «EXPAND templates::classes::Associations::ToOneAssociation 
            FOR signalAttributeWithCustomTypeOnly() -»
        «EXPAND templates::classes::Attributes::Attribute 
            FOR signalAttributeWithBasicTypeOnly() -»
    «ENDDEFINE»


The ``CustomEventType`` template is expanded in the ``StateMachine`` template for each signal with attributes (not shown here). The templates called from ``CustomEventAttribute`` are implemented as part of the structural generator that is discussed in section :ref:`generator-for-structural-models`. 

Another modification that is necessary in the header is the wrapper method signature for the respective guards and transition action wrappers. There, the type of the event needs to be replaced by the generated custom type described above. This works by calling an extension function ``eventPointerTypeName()`` instead of unconditionally generating ``ConstEventPtr``. The function is given in the following listing:


.. code-block:: xtend
    :linenos:

    eventPointerTypeName(uml::Transition this):
        let e = unambiguousPrimary().event():
            if e != null && e.hasAttributes() then e.eventTypeName() + "::ConstPtr"
            else                                   "ConstEventPtr";

    private uml::Transition unambiguousPrimary(uml::Transition this):
        if      isPrimary()              then this
        else if source.incoming.size > 1 then null
        else    unambiguousPrimary(source.incoming.first());


:math:`\pagebreak`


The ``unambiguousPrimary()`` is a helper function that returns the primary transition\ [#PrimaryTransition]_ of a given (possibly nonprimary) transition, if and only if there is a single primary transition. If there is no such transition, it returns ``null``.


.. _unambiguous-primary-transitions:

.. figure:: /../images/UnambiguousPrimaryTransitions.pdf
    :width: 80%

    Unambiguous Primary Transitions

    An example illustrating a situation where the primary transition is ambiguous (left), and one where an unambiguous primary exists (right). The unabiguous primary is highlighted in red.


This distinction is important, because a guard may reside on a nonprimary transition, but the custom event signal is specified on the primary. In that case the guard expression potentially accesses custom event parameters, so the custom event type is required. On the other hand, it is not always possible to unambiguously determine the primary transition. The two cases are shown in figure :ref:`unambiguous-primary-transitions`.

Consequently, if ``unambiguousPrimary()`` returns ``null``, ``eventPointerTypeName()`` returns the basic event pointer, and guards and transition actions may not access event parameters. They cannot know which event triggered the transition anyway, as ``evX`` and ``evY`` in figure :ref:`unambiguous-primary-transitions` demonstrate.



Implementation File Generator Templates
---------------------------------------

Since the signature of the wrapper methods changed in the header, they must change in the CPP file accordingly. This is now trivial, because the very same function ``eventPointerTypeName()`` for determining the event type name can be used to construct the function signature.

What remains is the ``shared_polymorphic_downcast`` in the state handler functions. As described in section :ref:`action-and-guard-wrappers`, the casts are required for guard wrapper and transition action wrapper calls. Only the latter is discussed here because it has a simpler template, the guard wrapper was changed accordingly.

This listing shows the ``TransitionActionCall`` template from ``templates/stateMachines/ implementation/ActionWrappers.xpt`` after the required modification:


.. code-block:: xpand
    :linenos:

    «DEFINE TransitionActionCall(uml::Transition t) FOR uml::Behavior -»
        context->«t.transitionActionMethodName()»(«t.eventParameter()»);
    «ENDDEFINE»


Instead of just generating the event pointer name ``ev`` into the parameter list, an extension function ``eventParameter()`` is called, which returns a string. This function is implemented such that it returns just ``ev`` for parameterless events, or ``boost::shared_polymorphic_downcast<T>(ev)`` for the custom event type ``T`` if the event is parametrized. Also, the same reasoning about ambiguous and unambiguous primary transitions applies as described above.

The following listing shows the ``eventParameter()`` function from the extension file ``extensions/stateMachines/WrapperMethods.ext``:


.. code-block:: xtend
    :linenos:
    
    eventParameter(uml::Transition this):
        let e = unambiguousPrimary().event():
            if e != null && e.hasAttributes() then 
                "boost::shared_polymorphic_downcast<const " + e.eventTypeName() + ">(ev)"
            else
                "ev";



Time Events
===========

Time events, or timeouts, are events that are triggered after a previously specified time elapses. A typical application of a time event is for example in a protocol handler state machine, where a connection needs to be closed after some timeout if the peer does not respond.

The meta class for time events specified by the UML subclasses *Event* and is called *TimeEvent*. A *TimeEvent* contains a *TimeExpression* which is a subclass of *ValueSpecification* and expresses the actual timeout value [OMG10]_.


Generated Code
--------------

The *Reactor* framework has built-in support for time events through the ``armTimer(event, timeout)`` and ``disarmTimers(event)`` methods defined in the class ``Reactive``. Time events are ordinary instances of the ``Event`` class; there is no special type for time events. 


.. _time-events:

.. figure:: /../images/Timeouts.pdf
    :width: 50%

    Time Event Example


For each time event usage, an ``Event`` instance must be stored as a member in the state machine object. This instance can be passed to ``armTimer()`` and will then be sent back by the time event scheduler after the timeout has elapsed.

Note that the time events in the *Reactor* differ from UML time events in the sense that the former contain a signal, where in UML only signal events have a signal. This difference is necessary, because the event processor code needs to be able to distinguish different time events. Thus, for each time event that is used in a *Reactor* state machine, one additional signal needs to be specified in the signal enumeration.

To conform to UML, the respective time events must be armed whenever a state with outgoing time event transitions is entered, and disarmed whenever such a state is left. Figure :ref:`time-events` shows an example with two time events, one on a transition starting at ``S1``, one on a transition starting at ``S2``. Here, the first one must be armed whenever entering ``S1``, the second one whenever entering ``S2``. Disarming happens accordingly.

The following listing shows the ``init()`` method and state handler for state ``S2`` to clarify what code needs to be generated:


.. code-block:: c++
    :linenos:

    void Example::init(ConstEventPtr ev) {
        context->armTimer(context->evTimeout1, 1000);
        context->armTimer(context->evTimeout2, 100);
        currentState = Sm::S1::S2;
    }

    void Example::Sm::S1::S2(Context context, ConstEventPtr ev) {
        switch (ev->sig) {
            case sigX:
                context->disarmTimers(context->evTimeout2);
                context->currentState = Sm::S1::S3;
                break;
            case sigTimeout2:
                context->disarmTimers(context->evTimeout2);
                context->disarmTimers(context->evTimeout1);
                context->currentState = Sm::S4;
                break;
            case sigTimeout1:
                context->disarmTimers(context->evTimeout2);
                context->disarmTimers(context->evTimeout1);
                context->armTimer(context->evTimeout1, 1000);
                context->armTimer(context->evTimeout2, 100);
                currentState = Sm::S1::S2;
                break;
        }
    }


As can be seen in the ``init()`` method, the two timers need to be armed, because both ``S1`` and ``S2`` are entered on initialization. In the state handler, if a ``sigX`` arrives, one of the timers is disarmed. If a ``sigTimeout2`` arrives, which means that the timer of ``S2`` expired, both ``S1`` and ``S2`` are left, thus both timeouts need to be disabled. Consequently, if a ``sigTimeout1`` arrives, ``S2`` and ``S1`` are left and entered again, so the timers are rearmed accordingly.

Note that for the sake of simplicity in the generator, time events are always explicitly disarmed, even if they just fired and were thus disarmed automatically.


Generator Templates for Arming and Disarming
--------------------------------------------

The templates that generate the time event objects in the header file are trivial and thus not further explained here. What is more interesting are the arming and disarming calls in the state action sequences, as shown in the listing above. This is realized using the following two templates:


.. code-block:: xpand
    :linenos:

    «DEFINE ArmTimer(uml::StateMachine context) FOR uml::TimeEvent -»
        context->armTimer(context->«eventName(context)», «timeout()»);
    «ENDDEFINE»


    «DEFINE DisarmTimer(uml::StateMachine context) FOR uml::TimeEvent -»
        context->disarmTimers(context->«eventName(context)»);
    «ENDDEFINE»


The function ``eventName()`` simply keeps track of all time events in a state machine and assigns them a number, starting at one. The returned name is "evTimeout" followed by that number.

To activate these templates, the existing templates for entering and leaving a state are modified to call ``ArmTimer`` and ``DisarmTimer``, respectively:


.. code-block:: xpand
    :linenos:

    «DEFINE LeaveState FOR uml::State -»
        «EXPAND WrapperFunctions::ExitActionCall(this) FOR exit -»
        «EXPAND DisarmTimer(containingStateMachine()) FOREACH timeEvents() -»
    «ENDDEFINE»

    «DEFINE EnterState FOR uml::State -»
        «EXPAND ArmTimer(containingStateMachine()) FOREACH timeEvents() -»
        «EXPAND WrapperFunctions::EntryActionCall(this) FOR entry -»
    «ENDDEFINE»


The function ``timeEvents()`` returns all time events that are attached to transitions going out of the given state. It is implemented as follows:


.. code-block:: xpand
    :linenos:

    timeEvents(uml::State this):
        outgoing.trigger.event.typeSelect(uml::TimeEvent);





.. -------------------------------------------------------------------- 
.. FOOTNOTES

.. [#WrapperMethods] See [Michel11]_ for a discussion of the concept and why wrapper methods are a necessity.

.. [#PrimaryTransition] The term «primary transition» was introduced in [Michel11]_ and describes transitions that start at a state, rather than at a pseudo-state.









