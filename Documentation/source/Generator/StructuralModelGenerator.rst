

.. _generator-for-structural-models:

Generator for Structural Models
*******************************


In order to further improve the benefits of using a code generator, it must be able to generate code from structural elements such as classes, interfaces and the relations between them. 

Inherently, the same problem exists as with the state machine generator: The user code must somehow be connected to the generated code. Here, a very similar approach to the one described in section 5.8 of [Michel11]_ is chosen: Fully generate the header files, leave the CPP files to the user.

The templates for the structural generator are located in the ``templates/classes/`` package of the generator source folder. The required extensions reside in the package ``extensions/classes/``.

Note that template and extension names in those packages are chosen according to what they generate, and not which model element they process. This is important to keep in mind, because otherwise the generator sources can become confusing, for example when looking into a template like ``«DEFINE ClassDeclaration FOR uml::Classifier»``. Here, ``ClassDeclaration`` refers to a declaration of a C++ class, and C++ classes can be classes or interfaces.

This rule is of course not true for the ``selectors`` package, which is target language independent, and thus uses names according to the UML only\ [#GeneratorReorganization]_.


UML Profiling
=============

Since UML is a general-purpose modeling language that is not tied to any particular programming language, its building blocks differ from those of C++ in various ways. In the case of interfaces and classes as described above, this is not much of a problem. However, in cases where C++ constructs do not exist in UML, we have to find a way to model them somehow. 

A simple example of a C++ feature that is not present in the UML is the ``virtual`` keyword. There is just no ``IsVirtual`` property on UML operations, and it cannot be semantically derived from the model in the general case. Thus, we need to extend the UML.

One of the extension possibilities of UML is the use of profiles. Profiles provide a container for stereotypes and tagged values, which can later be applied to model elements in a concrete model [OMG10]_.

Consequently, in order to retrofit UML to support the required C++ constructs, a *ReactRT* profile has been created. Apart from the C++-specific extensions, other stereotypes have been created that simplify repetitive modeling tasks. 

Some of the stereotypes defined in the profile are introduced in the following sections, where they are used. The profile itself is the topic of chapter :ref:`react-rt-uml-profile`.


Pointers
========


Memory management in C++ is difficult to get right, which is of course due to the developers' responsibility to release memory after usage. 

A concept that can profoundly simplify memory management in C++ are smart pointers. Different kinds of smart pointers exist, but the most flexible ones are shared pointers, which implement a reference counting algorithm: They keep track of how many references to an object are currently in use, and if this value drops to zero, the shared pointer calls ``delete`` on the managed object automatically [CppStdDraft11]_.

Shared pointers will be included in the upcoming C++ standard. Until then, the smart pointers of the *Boost* library\ [#BoostSmartPtr]_ can be used.

In my opinion, modern software engineering projects should not fiddle with raw pointers anymore, because smart pointers offer an efficient alternative that is much more robust. Thus, pointers in ReactRT are generated as shared pointers, employing the *Boost* library.

This decision has some consequences, though:

- It must be possible to use weak pointers in selected cases, so the user must be able to stereotype a relation as «weak» in the model. The generator produces a ``weak_ptr`` instead of a ``shared_ptr`` in that case.

- Pointer type code generation must be organized in a way that it is possible to replace the pointers with raw pointers. Although this is not the default, raw pointers may be necessary for certain projects. These projects should not refrain from using ReactRT just because smart pointers are employed by default.

  However, since the *Reactor* framework is implemented using smart pointers, it is not possible to use the state machine generator with raw pointers, only the generator for structural models.

Note that if a user explicitly needs a raw pointer for a special case, it is always possible to create a ``typedef`` for the raw pointer and use that instead of the target type. Typedefs are discussed in more detail in section :ref:`structural-typedefs`.


Extensions for Pointer Types
----------------------------

In order to enable the replacement of shared pointers, a set of extension functions has been created that deals just with the creation of pointer code. All templates that need to produce pointers eventually call these functions. 

The functions are collected in the extension file ``PointerTypes.ext`` in the package ``extensions/ classes/member/``. All functions in this file except some simple boolean queries call either ``pointerTo()`` or ``weakPointerTo()``, depending on whether the «weak» stereotype from the ReactRT profile has been set.

The following listing shows the respective functions. They are overloaded for ease of use.


.. code-block:: xtend
    :linenos:

    pointerTo(uml::Type this, boolean asConst):
        if asConst then constPointerTo(name.toString())
        else            pointerTo(name.toString());
        
    weakPointerTo(uml::Type this, boolean asConst):
        if asConst then constWeakPointerTo(name.toString())
        else            weakPointerTo(name.toString());
        
    pointerTo(String typeName):
        "boost::shared_ptr<" + typeName + ">";
        
    weakPointerTo(String typeName):
        "boost::weak_ptr<" + typeName + ">";
        
    constPointerTo(String typeName):
        pointerTo("const " + typeName);
        
    constWeakPointerTo(String typeName):
        weakPointerTo("const " + typeName);


The meta class ``uml::Type`` is an abstract meta class and is the base of ``uml::Class`` and ``uml:: Interface``, among others. 

The function design as shown above brings the desired flexibility: By replacing the content of just ``pointerTo(String)`` and ``weakPointerTo(String)`` it is effectively possible to remove smart pointers completely\ [#AopFeatures]_.


Type Usage
==========

Types are used in C++ code in many different situations, for example as formal parameter type, as attribute type, or as type parameter for collections. Templates should not care whether a type needs pointer code or not. To ensure the type is always used correctly, type usage has been abstracted away in the sense of pointers above. The respective functions are contained in the extension file ``TypeUsage.ext``.

The following function shows an example of a type usage to create a formal parameter definition for a function:


.. code-block:: xtend
    :linenos:

    String parameterDeclaration(uml::Parameter this):
        useType() + " " + name;


Here, it does not matter if the type of the parameter is a primitive type or a custom class; ``useType()`` will always generate the appropriate code. The implementation of ``useType()`` is given in the following listing:


.. code-block:: xtend
    :linenos:

    String useType(uml::TypedElement this):
        if type.isPointerType()  then pointerType()
        else if type.isTypedef() then constKeyword() + " " + type.qualifiedTypedefName()
        else if type.isSignal()  then pointerTo(constKeyword() + " " + type.eventType())
        else                          useTypeByValue();


This function differentiates between four kinds of type usage: Pointer types, typedefs, signals and everything else. Pointers generate a shared pointer or weak pointer as described in the previous section. Typedefs need to be handled specially, as discussed below. Signals are used when a custom event is passed to a function, so they need to always produce a raw pointer. Finally, primitive types and other types are used by value. This is implemented as follows:

.. code-block:: xtend
    :linenos:

    private useTypeByValue(uml::TypedElement this):
        if isConst() then useTypeByConstValue()
        else              type.name;
        
    private useTypeByConstValue(uml::TypedElement this):
        if uml::PrimitiveType.isInstance(type) then "const " + type.name
        else                                        type.constRefTo();
    
    constRefTo(uml::Type this):
        "const " + name + "&";

The function ``useTypeByValue`` differentiates between ``const`` and non-``const`` usage. If it is a ``const`` usage, it is further checked if the type is a primitive type or not. This enables the generation of more efficient code, because complex types that are used by value can be passed as ``const`` reference. By-value usage is common for standard strings, for example.

In associations, the normal thing to generate is a pointer type. However, the user may explicitly choose to override the default behavior. For example, to use a type as embedded member it can be modeled it as attribute instead of association. The following two functions are called for exactly these cases, producing the desired effect:


.. code-block:: xtend
    :linenos:

    String useTypeInAttribute(uml::Property this):
        if type.isTypedef() then type.qualifiedTypedefName()
        else                     type.name;
        
    String useTypeInRelation(uml::Property this):
        pointerType();


.. _structural-typedefs:

Typedefs
========

Typedefs do not have a UML-equivalent, either. To find an appropriate modeling approach, we need to consider what a typedef consists of:


.. code-block:: text

      typedef   const int *   ConstIntPointer  ;
    |         |             |                 |
    | keyword | target type |    new name     |


To define this typedef in the model, some model element named "ConstIntPointer" must be created, which carries the target type as additional information. Also, the model element must be identifiable as a typedef. That would provide enough information for the code generator to produce the code above.

To achieve this, a stereotype «typedef» carrying a tagged value ``originalType`` has been created. This typedef can be attached to UML *DataType* elements. *DataType* is just another classifier meta type. It is preferred over *Class* because *DataType* is not normally used for other modelling purposes, so using it only for typedefs makes them more distinguished in the model.

Thus, to create the typedef above, the following steps are required:

#. Create a new *DataType* element and name it "ConstIntPointer".

#. Apply the «typedef» stereotype and set ``originalType`` to "const int \*".

#. If required by the target type, apply the «needsInclude» stereotype as described in section :ref:`structural-generator-includes`. Since ``int`` is a primitive type, no additional include is required for the example above.


Because typedefs are *DataType* instances, they can be used in the model in every place that types and classifiers can be used, for example to define a member attribute. 

As can be seen in the listing of ``useType()`` above, the code generator handles typedef usages differently. This is because typedefs are often used in multiple classes, so they need to be qualified with the class name, which is exactly what ``qualifiedTypedefName()`` does. This function, as well as other helper functions to work with typedefs, are located in the ``Typedefs.ext`` extension file in the package ``extensions/classes/member/``.


Specializations and Realizations
================================


Base Classifiers
----------------


Since interfaces and classes have the same syntax in C++, class specialization and interface realization is indistinguishable, too. In order to not get confused by the different meanings of "class" in the UML and C++ contexts, the following definition is introduced:


.. topic:: Definition 2

    Base classifiers
        The union of the set of generalizations and the set of realized interfaces as defined by the UML. This is equivalent to the set of base classes in C++. 
        
        :math:`\vspace{0pt}`


Base classifiers have an impact on a C++ header file at three places:

#. The respective header of the base classifier needs to be included

#. The base classifier must be listed in the derivation list after the class name

#. Overridden virtual and implemented abstract methods must be marked ``virtual``

This must be considered when implementing class specialization and interface realization in the generator.

Since the three points pertain to different constructs in the generated code, it makes sense to deduce the respective code fragments from a single function that returns the set of base classifiers. This function is given in the following listing:


.. code-block:: xtend
    :linenos:

    baseClassifiers(uml::Classifier this):
        generalizationClassifiers().union(realizedInterfaces());
    
    generalizationClassifiers(uml::Classifier this):
        generalization.general;
    
    realizedInterfaces(uml::BehavioredClassifier this):
        interfaceRealization.contract;

    realizedInterfaces(uml::Classifier this):
        {};


The second overload of ``realizedInterfaces()`` is just a polymorphic base case that returns an empty list. It is required to make ``realizedInterfaces()`` type compatible with the call in ``baseClassifiers()``.

The query ``generalization.general`` returns an *uml::Classifier*, whereas ``interface Realization.contract`` returns an *uml::Interface*. Forming the union set creates a set of the common base meta class, which is *uml::Classifier*.

Now that the set of base classifiers is ready, it can be used to generate code for the three points above; the generation of includes is described in section :ref:`structural-generator-includes`, information about the other two points follow here.

 
Derivation Lists
----------------

In ``templates/classes/Classes.xpt``, the following template can be found:

.. code-block:: xpand
    :linenos:

    «DEFINE ClassDeclaration FOR uml::Classifier»
        class «name»«inheritanceColon()»«derivationList()» {
            «EXPAND ClassBody»
        };
    «ENDDEFINE»


The ``inheritanceColon()`` function is rather simple: It returns a colon if the classifier has base classifiers, or an empty string otherwise. 

The function ``derivationList()`` concatenates all strings in the list returned by ``derivations()`` with a comma as delimiter:


.. code-block:: xtend
    :linenos:
    
    String derivationList(uml::Classifier this):
        derivations().toString(", ");

    private derivations(uml::Classifier this):
        let baseNames = visibilityPrefixedBaseClassifierNames():
            baseNames.addAll(enableSharedFromThisStereotype()) ->
            ( if containsStateMachine() then baseNames.add(reactiveBase()) ) ->
            ( if needsNoncopyableBase() then baseNames.add(noncopyableBase()) ) ->
            baseNames.toSet();

    private visibilityPrefixedBaseClassifierNames(uml::Classifier this):
        baseClassifiers().name.collect(e | "public " + e);
    
    private reactiveBase(uml::Classifier this):
        "public rrt::Reactive";
        
    private noncopyableBase(uml::Classifier this):
        "private boost::noncopyable";
    
    private enableSharedFromThisStereotype(reactRT::enableSharedFromThis this):
        { "public boost::enable_shared_from_this<" + name + ">" };
        
    private enableSharedFromThisStereotype(uml::Classifier this):
        {};


As can be seen, the ``derivations()`` function is a bit more complex. It starts with the list returned by ``visibilityPrefixedBaseClassifierNames()`` and assigns it to ``baseNames``. Then, the base class for state machines and the noncopyable base class are added if required. Finally, the ``baseNames`` are returned as set of strings, where each string contains the inheritance access modifier followed by the name of the base classifier.



Virtual Operations
------------------

An operation needs the ``virtual`` keyword in the following cases:

- The operation is abstract, as indicated in the model by the IsAbstract property 

- The operation is defined within an interface (and is thus implicitly abstract)

- The operation is forced ``virtual`` because the «virtual» stereotype is set

- The operation overrides a base class operation which is marked ``virtual``\ [#VirtualInBase]_


While the first three cases are trivial, determining if an operation is ``virtual`` in a base classifier requires recursively traversing the inheritance tree upwards and looking for operations that are override compatible. The code in the following listing does exactly that:


.. code-block:: xtend
    :linenos:

    virtualKeyword(uml::Operation this):
        if isStatic               then ""
        else if isAbstract        then "virtual"
        else if interface != null then "virtual"
        else if isVirtualInBase() then "virtual"
        else                           "";
            
    virtualKeyword(reactRT::virtual this):
        "virtual";

    private isVirtualInBase(uml::Operation this):
        if interface != null  then interface.allBaseClassifiers().exists(
                                   e | e.hasOverrideCompatibleOperation(this))
        else if class != null then class.allBaseClassifiers().exists(
                                   e | e.hasOverrideCompatibleOperation(this))
        else                       false;

    private boolean hasOverrideCompatibleOperation(uml::Classifier this, 
            uml::Operation op):
        operations().exists(e | e.isOverrideCompatible(op));

    private isOverrideCompatible(uml::Operation this, uml::Operation op):
        (name == op.name) && areParamsCompatible(parameters(), op.parameters());

    private boolean areParamsCompatible(List[uml::Parameter] this, 
            List[uml::Parameter] other) : 
        if isEmpty && other.isEmpty                then true
        else if isEmpty && !other.isEmpty          then false
        else if !isEmpty && other.isEmpty          then false
        else if first().type == other.first().type then
                withoutFirst().areParamsCompatible(other.withoutFirst())
        else                                            false;


The two overloads of ``virtualKeyword()`` perform the distinction of the cases described above. The function ``isVirtualInBase()``, which represents the non-trivial case, calls ``hasOverride CompatibleOperation()`` on every classifier that is a base, be it direct or through other classifiers. This function then in turn checks for each operation in the respective base classifier if it is override compatible to the operation in question.

Override compatibility is checked by simply comparing the operation name and the type names of the parameters. Note that this is safe, because C++ does not support contravariant parameters.


.. _structural-generator-includes:

Includes and Forward Declarations
=================================

There are various references for which an include or a forward declaration must be generated. This section gives an overview of the handled cases. The actual extension functions that collect the required information are straight-forward and thus not reproduced here. They can be found in the extension files ``Includes.ext`` and ``ForwardDeclarations.ext`` in the package ``extensions/classes/``.

Includes are generated for:


- The smart pointer templates ``<boost/smart_ptr.hpp>``

- The noncopyable base class template ``<boost/noncopyable.hpp>``

- The *Reactor* framework, if the respective class is a state machine ``<Reactive.h>``, ``<Event.h>``

- Each base classifier

- Each container that is used by a relation

- Each ``typedef`` that is not defined in the current class

- Each embedded member

- Each additional include as specified by a «needsInclude» stereotype


The «needsInclude» stereotype provides a mechanism to specify includes that would otherwise not be generated. This may be required when external classes are referenced in the header, for example when an STL file stream is stored as member.

Note that the function that returns the required includes returns a set, so that the same include is not generated twice.

Forward declarations are generated for:

- Each association

- Each pointer type that is used as formal parameter

- Each pointer type that is used as relation qualifier

- Each usage dependency

- Each custom signal attribute, if the respective class is a state machine


Consequently, the forward declarations are returned as set, too.



.. -------------------------------------------------------------------- 
.. FOOTNOTES


.. [#GeneratorReorganization] See section :ref:`generator-reorganization` for more information about the generator source organzation.

.. [#BoostSmartPtr] Documentation and downloads are available on http://www.boost.org.

.. [#AopFeatures] Replacing the content of an Xtend function can be done in an unintrusive way by using aspect-oriented programming. The AOP features of Xpand and Xtend are discussed in more detail in section :ref:`Observatory — State Machine Instrumentation <observatory-state-machine-instrumentation>`.

.. [#VirtualInBase] This is actually not required, but deriving class C from an already derived class B can lead to problems if an operation is not marked ``virtual`` in B. Thus, the approach taken here is "once virtual, always virtual".














