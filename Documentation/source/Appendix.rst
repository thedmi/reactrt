

Included Examples
#################

The three *ReactRT* examples can be found in folder ``Sources/Examples``.


Lamp
****

The *Lamp* example was originally created as part of the previous thesis, but has been adapted to the new generator. The example consists of a single, very simple state machine that implements a lamp. It can be turned on and off using keyboard commands.

This is the simplest possible example that demonstrates how the generator works. It makes sense to use this example to verify that a setup of *MagicDraw*, the generator and the *Reactor* framework works.

See [Michel11]_ for more information.


Candy Machine
*************

The *Candy Machine* has been developed within the previous thesis, too. It consists of two collaborating state machines that model a candy machine. Using the keyboard, coins can be fed into the candy machine, and as soon as the price for a candy is paid, it can be released.

See [Michel11]_ for more information.


Rail Cars
*********

The *Rail Cars* example is considerably larger than the other two and demonstrates all aspects of *ReactRT*. It is discussed in a dedicated chapter, see :ref:`rail-cars-example`.



CD-ROM Contents
###############

.. literalinclude:: CdContents.txt



Utilized Software Packages
##########################


- Boost Library 1.46

- Command Line Parser Library for C# 1.8

- Eclipse Helios
	
	- C/C++ Development Tools 7.0.1

	- Eclipse Modeling Tools 1.3.1

	- Graphical Modeling Framework SDK 2.3.0

	- MWE SDK 1.0.0

	- Xpand SDK 1.0.1

	- Apache Commons Logging Plugin

- Java SE SDK 1.6.0-20 

- .Net SDK 4.0

- NoMagic MagicDraw UML 16.5 Standard

- Python 2.7 (Generator Verifier)

- Visual Studio 2008 Professional (for C++ development)

- Visual Studio 2010 Ultimate (for C# development)





Project Proposal
################

(see attached document)



Non-Plagiarism Statement
########################


I hereby declare that all sources have been mentioned and are referenced according to common scientific rules and practice.



    |
    |
    |
    |
    | 
    
Daniel Michel

