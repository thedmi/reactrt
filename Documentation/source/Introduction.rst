
Introduction
************


Previous Work
=============

This master thesis builds upon my previous thesis [Michel11]_, where a generator for UML state machines was developed. 

Much of the base technology of the previous thesis is reused here, knowledge about it is assumed. Particularly the generator framework *Xpand*, the state machine execution framework *Quantum Platform*, and the details of the state machine UML models are not introduced here again. See [Michel11]_ for an introduction to these frameworks.


Project Overview
================

This section gives a rough overview of the major parts of *ReactRT* and shows how they relate to each other.


.. _high-level-overview:

.. figure:: /../images/HighLevelOverview.pdf
    :width: 100%

    ReactRT Overview


Figure :ref:`high-level-overview` shows the logical components of *ReactRT*. The green boxes designate components that have partially been developed during the previous thesis.

The **Code Generator** traverses a set of input models and produces the appropriate C++ code. The models can be enriched with UML extensions defined in the **ReactRT UML Profiles** to accommodate nonstandard constructs. 

The generated code is designed to run on the **Reactor** state machine execution engine. It implements the basis of the active object computing paradigm and drives applications produced with *ReactRT*.

The **Observatory** is a monitoring solution that enables the debugging and analysis of state machine-driven software. It collects information directly in the target and displays them in a desktop application for inspection.

Note that some parts of the previous thesis have been removed (cf. [Michel11]_). Particularly the *Quantum Platform* and the *QP Adaption Layer* are no longer part of *ReactRT* anymore, chapter :ref:`execution-engine-reconsidered` explains why.


