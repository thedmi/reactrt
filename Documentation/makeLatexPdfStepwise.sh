#!/bin/bash

pushd . > /dev/null
cd `dirname $0`

make latex

echo ""
read -p "Press enter to make PDF or Ctrl-C to abort"

make latexpdf

popd

