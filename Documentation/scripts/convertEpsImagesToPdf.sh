#!/bin/bash

pushd . > /dev/null
cd `dirname $0`
cd ../images

for file in *.eps; do
	echo "Converting $file ..."
	ps2pdf -dEPSCrop "$file"
done

popd
