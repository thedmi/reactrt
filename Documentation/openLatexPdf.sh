#!/bin/bash

pushd . > /dev/null
cd `dirname $0`

evince build/latex/ReactRtDocumentation.pdf &

popd
