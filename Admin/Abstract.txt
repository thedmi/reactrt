
Model-Driven Software Development (MDSD) technologies have a high reputation in embedded and real-time software engineering, mostly due to their ability to turn state machine diagrams into executable code automatically and enable software engineering on a higher level of abstraction. However, the conventional tools are not only expensive, but also often inflexible, because their generators are built as monolithic black boxes that offer only a limited amount of customization possibilities. 

The ReactRT tool suite eliminates these shortcomings through an open and extensible approach. This is achieved by implementing the ReactRT generator as an open set of generator templates and model traversing functions that promote reuse, extendability and customization.

Also, since testing is a very important aspect of modern software projects, ReactRT provides a testing infrastructure that eases unit and integration test development for reactive systems. With this infrastructure it is possible to implement fast, reliable and comprehensible tests, something that is not trivial to achieve with traditional MDSD solutions.

The ReactRT tool suite further contains a monitoring solution, which is able to track state machine behavior in a running application, either while connected to a live system, or from a recorded session. The state machine data can be visualized in a powerful analysis application that enables the inspection of state changes, inter-state machine behavior and single run-to-completion steps. The monitoring solution makes it possible to efficiently debug and analyze a reactive application on the appropriate level of abstraction. This greatly simplifies the investigation and diagnosis of bugs and has therefore the potential to save project costs.

ReactRT as a whole forms an effective development infrastructure for reactive application engineering. This thesis covers the development of the ReactRT tool suite, with an in-depth discussion of each component.



