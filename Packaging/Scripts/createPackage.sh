#!/bin/bash

pushd . > /dev/null
cd `dirname $0`
cd ..


DESTINATION=Deployment/reactRT_1.1/


rm -Rf Deployment
mkdir -p $DESTINATION


for DIR in Examples Frameworks Generator Integration ModelLibraries; do
	svn export https://svn.origo.ethz.ch/react-rt/trunk/Sources/$DIR/ $DESTINATION/$DIR
done

cd Deployment

zip -r reactRT_1.1.zip reactRT_1.1

popd

