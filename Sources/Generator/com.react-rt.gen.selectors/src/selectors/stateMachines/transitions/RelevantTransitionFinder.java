package selectors.stateMachines.transitions;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.SignalEvent;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;

public class RelevantTransitionFinder {

	private static final Log LOG = LogFactory.getLog(RelevantTransitionFinder.class);
	
	/**
	 * Finds all outgoing transitions that are relevant for a given simple state
	 * s.
	 * 
	 * Relevant are all transitions in the state hierarchy of s that are not
	 * masked by a more specific state's transition. Masking occurs when a
	 * transition of a state is triggered by the same signal as a transition of
	 * one of its sub-states.
	 * 
	 * @param s
	 *            The simple state to find transitions for.
	 * @return A collection made up of all relevant transitions for s.
	 */
	public static List<Transition> findRelevantSignalEventTransitions(State s) {

		List<Transition> allTransitions = accumulateTransitions(s);
		LinkedList<Signal> foundSignals = new LinkedList<Signal>();
		LinkedList<Transition> relevantTransitions = new LinkedList<Transition>();

		for (Transition t : allTransitions) {

			LOG.debug("Transition for state " + s.getName() + ": "
					+ t.getSource().getName() + " -> "
					+ t.getTarget().getName() + " [ " + t.getEffect() + " ]");

			Signal sig = extractSignal(t);
			if (sig != null) {
				if (!foundSignals.contains(sig)) {
					relevantTransitions.add(t);
					foundSignals.add(sig);
				}
			}
		}

		return relevantTransitions;
	}

	/**
	 * Recursively accumulates all outgoing transitions of the given state and
	 * its super-states. More specific transitions (i.e. belonging to a deeper
	 * state nesting level) are guaranteed to occur before less specific
	 * transitions in the list.
	 * 
	 * @param s
	 *            The state for which to accumulate outgoing transitions.
	 * @return A list of all outgoing transitions of s in a hierarchical sense.
	 *         There may be multiple transitions with the same trigger event.
	 */
	private static List<Transition> accumulateTransitions(State s) {

		LOG.debug("Accumulating transitions for state " + s.getName());

		// Copy the outgoing one-by-one into a normal list. If the
		List<Transition> outgoingTransitions = new LinkedList<Transition>();
		for (Transition t : s.getOutgoings()) {
			outgoingTransitions.add(t);
		}

		if (s.getContainer().getState() != null) {
			outgoingTransitions.addAll(accumulateTransitions(s.getContainer()
					.getState()));
		}
		return outgoingTransitions;
	}

	/**
	 * Extracts a signal from a given transition. Note that only the first
	 * trigger of the transition is considered. Of course, a signal can only be
	 * extracted if the trigger's event is of type SignalEvent.
	 * 
	 * @param t
	 *            The transition to extract from.
	 * @return The signal if there is one, null otherwise.
	 */
	private static Signal extractSignal(Transition t) {
		Trigger trig = t.getTriggers().get(0);
		if (trig != null && trig.getEvent() instanceof SignalEvent) {
			return ((SignalEvent) trig.getEvent()).getSignal();
		}
		return null;
	}

}