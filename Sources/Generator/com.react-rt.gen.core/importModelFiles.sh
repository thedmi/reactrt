#!/bin/bash

for file in /media/ZRHN1054/Data/MSE/SemProj/Sources/ReferenceImplementations/ReferenceModels/export/*.uml; do
	out=`basename "$file"`
	sed -e "s/uml2\/2\.0\.0/uml2\/3\.0\.0/g" "$file" > "model/$out"
done
