/*
 *	This file contains functions to determine names of the wrapper methods, such as action wrapper
 *	or guard wrapper.
 */

extension selectors::commonBehaviors::Events;
extension selectors::stateMachines::Transitions;
extension selectors::stateMachines::States;
 
extension extensions::stateMachines::SignalNaming;
extension extensions::stateMachines::EventNaming;

extension org::eclipse::xtend::util::stdlib::io;

 
		
entryActionMethodName(uml::State this):
	"enter_" + actionMethodFqn();
		
		
exitActionMethodName(uml::State this):
	"exit_" + actionMethodFqn();
		
		
initActionMethodName(uml::State this):
	"initialize_" + actionMethodFqn();
	
	
transitionActionMethodName(uml::Transition this):
	if isPrimary() then primaryTransitionActionMethodName()
	else				nonprimaryTransitionActionMethodName();
	
	
/*
 *	As with nonprimary transitions, we cannot assume that the source of the guarded 
 *	transition has a name. Thus, guards are also labeled with the transition id.
 */
guardName(uml::Transition this):
	"guard_t" + transitionId();
	

private guardCall(uml::Transition this, uml::Transition primary):
	"context->" + guardName() + "(" + primary.eventParameter() + ")"; 	

	
compoundGuardWrapperCalls(List[uml::Transition] this):
	select(e | e.guard != null).collect(e | e.guardCall(first())).toString(" && ");
	
	
compoundGuardExpression(List[uml::Transition] this):
	'(' + select(e | e.guard != null).guard.guardExpression().toString(') and (') + ')';
	
	
guardExpression(uml::Constraint this):
	specification.expression();
	
	
eventPointerTypeName(uml::Transition this):
	let e = unambiguousPrimary().event():
		if e != null && e.hasAttributes() then e.eventTypeName() + "::ConstPtr"
		else								   "ConstEventPtr";

/* 
 *	Returns either just "ev" or ev wrapped in a downcast, which casts the
 *	event to the appropriate custom event type.
 */
eventParameter(uml::Transition this):
	let e = unambiguousPrimary().event():
		if e != null && e.hasAttributes() then "boost::shared_polymorphic_downcast<const " + e.eventTypeName() + ">(ev)"
		else					  			   "ev";

/*
 *	Primary transitions are those that start at a state. They are named according
 *	to the source state and the signal that triggers the transition.
 */
private primaryTransitionActionMethodName(uml::Transition this):
	"transition_" + source.asState().actionMethodFqn() + "_" + eventName();
	
	
/*
 *	Nonprimary transitions are those that start at a pseudo-state. They cannot be named
 *	according to the source, because pseudo-states do not normally have names. Thus,
 *	they are labeled with a unique transition id (e.g. t3).
 */
private nonprimaryTransitionActionMethodName(uml::Transition this):
	"transition_t" + transitionId();
	
	
private String actionMethodFqn(uml::State this):
	if (container.stateMachine != null) then name
	else									 actionMethodFqn(container.state) + "_" + name;


private String transitionId(uml::Transition this):
	transitionId(containingStateMachine());

private String transitionId(uml::Transition this, uml::StateMachine ctx):
	JAVA extensions.stateMachines.IdTracker.transitionId(org.eclipse.uml2.uml.Transition, org.eclipse.uml2.uml.StateMachine);

/*
 *	This function follows a compound transition back, starting at the given transition. 
 *	If a primary transition is reached, the primary transition is returned. If a pseudostate
 *	is reached where multiple incoming transitions arrive, null is returned, because there
 * 	is not a single primary transition in that case.
 */
private uml::Transition unambiguousPrimary(uml::Transition this):
	if 		isPrimary() 			 then this
	else if source.incoming.size > 1 then null
	else								  unambiguousPrimary(source.incoming.first());
	
	
private String expression(uml::ValueSpecification this):
	throwError("Use OpaqueExpressions where ValueSpecifications are required.") -> "";
	
private String expression(uml::OpaqueExpression this):
	body.first();
	