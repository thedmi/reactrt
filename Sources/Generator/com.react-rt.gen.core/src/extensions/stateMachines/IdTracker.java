package extensions.stateMachines;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.TimeEvent;
import org.eclipse.uml2.uml.Transition;

public class IdTracker {

	private static Map<Element, Vector<Transition>> identifiedTransitions = new HashMap<Element, Vector<Transition>>();

	private static Map<Element, Vector<TimeEvent>> identifiedTimeEvents = new HashMap<Element, Vector<TimeEvent>>();

	/**
	 * This function assigns IDs to transitions. Calling the function multiple
	 * times with the same transition will always return the same ID.
	 * 
	 * @param t
	 *            The transition for which the ID should be returned.
	 * @return Returns a unique numerical ID which unambiguously can be used to
	 *         identify a transition.
	 */
	public static String transitionId(Transition t, StateMachine context) {
		return determineId(t, context, identifiedTransitions);
	}

	/**
	 * This function assigns IDs to time events. Calling the function multiple
	 * times with the same time event will always return the same ID.
	 * 
	 * @param e
	 *            The time event for which the ID should be returned.
	 * @return Returns a unique numerical ID which unambiguously can be used to
	 *         identify a time event.
	 */
	public static String timeEventId(TimeEvent e, StateMachine context) {
		return determineId(e, context, identifiedTimeEvents);
	}

	/**
	 * Tests if the element is already in the provided vector. If not, the
	 * element is inserted. A unique ID is returned for the element. Note that
	 * the IDs are managed for a specific context element. Thus, ID numbering
	 * restarts from 1 if a new context is provided.
	 * 
	 * @param <T>
	 *            The type of the elements.
	 * @param element
	 *            The element for which an ID should be returned.
	 * @param v
	 *            The collection holding the elements that already have an ID.
	 * @return The ID assigned to the element.
	 */
	private static <T> String determineId(T element, Element context,
			Map<Element, Vector<T>> contextStore) {

		if (!contextStore.containsKey(context)) {
			contextStore.put(context, new Vector<T>());
		}

		Vector<T> trackerStore = contextStore.get(context);

		if (!trackerStore.contains(element)) {
			trackerStore.add(element);
		}

		return String.valueOf(trackerStore.indexOf(element) + 1);
	}
}
