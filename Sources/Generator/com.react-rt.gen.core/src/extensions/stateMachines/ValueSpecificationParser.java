package extensions.stateMachines;

import org.eclipse.uml2.uml.ValueSpecification;

public class ValueSpecificationParser {

	/**
	 * This function tries to parse the string contained in the given value
	 * specification into an integer and indicates whether this was successful.
	 * 
	 * @param s
	 *            The string to parse.
	 * @return True if the string can be parsed into an int, false otherwise.
	 */
	public static boolean containsInteger(ValueSpecification s) {
		try {
			Integer.parseInt(s.stringValue());
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}