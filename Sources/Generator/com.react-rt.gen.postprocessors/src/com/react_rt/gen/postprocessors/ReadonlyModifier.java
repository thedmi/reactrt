package com.react_rt.gen.postprocessors;

import java.io.File;

import org.eclipse.xpand2.output.FileHandle;
import org.eclipse.xpand2.output.PostProcessor;

public class ReadonlyModifier implements PostProcessor {

	@Override
	public void beforeWriteAndClose(FileHandle handle) {
		// Intentionally do nothing
	}

	@Override
	public void afterClose(FileHandle handle) {
		File target = new File(handle.getAbsolutePath());
		target.setReadOnly();
	}

}
