package com.react_rt.gen.postprocessors;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.xpand2.output.FileHandle;
import org.eclipse.xpand2.output.PostProcessor;

import com.react_rt.gen.components.RegexDirectoryCleaner;

public class UncrustifyBeautifier implements PostProcessor {

	private static final Log LOG = LogFactory.getLog(RegexDirectoryCleaner.class);

	@Override
	public void afterClose(FileHandle handle) {

		LOG.info("Beautifying " + cannonicalPath(handle.getAbsolutePath()));

		try {
			File uncrustifyConfig = new File("../com.react-rt.gen.postprocessors/config/uncrustify.cfg");

			if (!uncrustifyConfig.exists()) {
				throw new RuntimeException("Uncrustify config file not found. Path is " + uncrustifyConfig);
			}

			delay(100);

			String cmd = "uncrustify -c " + uncrustifyConfig.getAbsolutePath() + " -l CPP --replace --no-backup "
					+ handle.getAbsolutePath();
			Process beautifier = Runtime.getRuntime().exec(cmd);

			BufferedReader pStderr = new BufferedReader(new InputStreamReader(beautifier.getErrorStream()));

			delay(200);
			readStderr(pStderr);

		} catch (IOException e) {
			LOG.error("IOException while communication with uncrustify process", e);
			e.printStackTrace();
		}
	}

	private void readStderr(BufferedReader pStderr) throws IOException {

		StringBuffer errorMessage = new StringBuffer();

		while (pStderr.ready()) {
			errorMessage.append(pStderr.readLine());
		}

		LOG.debug(errorMessage);
	}

	private void delay(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// Intentionally do nothing
		}
	}

	private String cannonicalPath(String p) {
		try {
			return new File(p).getCanonicalPath();
		} catch (IOException e) {
			// Intentionally fail
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public void beforeWriteAndClose(FileHandle handle) {
		// Intentionally do nothing
	}

}
