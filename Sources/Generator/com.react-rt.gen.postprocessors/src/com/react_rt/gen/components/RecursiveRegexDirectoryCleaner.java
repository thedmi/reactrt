package com.react_rt.gen.components;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.DirectoryWalker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

class RecursiveRegexDirectoryCleaner extends DirectoryWalker<File> {

	private static final Log LOG = LogFactory.getLog(RecursiveRegexDirectoryCleaner.class);

	private Pattern pattern;

	public List<File> deleteMatchingDirectories(File baseDir, String regex) throws IOException {
		List<File> result = new ArrayList<File>();
		pattern = Pattern.compile(regex);
		walk(baseDir, result);
		return result;
	}

	@Override
	protected boolean handleDirectory(File directory, int depth, Collection<File> results) throws IOException {

		LOG.debug("Matching " + directory.getCanonicalPath() + " against regex " + pattern);

		if (pattern.matcher(directory.getCanonicalPath()).matches()) {
			LOG.info("Cleaning directory  " + directory.getCanonicalPath());

			FileUtils.cleanDirectory(directory);

			results.add(directory);
			return false; // Don't process contents (there won't be any)
		}
		return true;
	}

}
