package com.react_rt.gen.components;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.emf.mwe.core.WorkflowContext;
import org.eclipse.emf.mwe.core.issues.Issues;
import org.eclipse.emf.mwe.core.lib.AbstractWorkflowComponent2;
import org.eclipse.emf.mwe.core.monitor.ProgressMonitor;

public class RegexDirectoryCleaner extends AbstractWorkflowComponent2 {

	private static final Log LOG = LogFactory.getLog(RegexDirectoryCleaner.class);

	private RecursiveRegexDirectoryCleaner cleaner;

	private String baseDirectory;

	private String includePattern;
	
	public RegexDirectoryCleaner() {
		cleaner = new RecursiveRegexDirectoryCleaner();
	}

	@Override
	protected void invokeInternal(WorkflowContext ctx, ProgressMonitor monitor, Issues issues) {
		LOG.debug("Starting recursive regex directory cleaner...");

		try {
			cleaner.deleteMatchingDirectories(new File(baseDirectory), includePattern);
		} catch (IOException e) {
			issues.addError(this, "Directory cleaner failed", this, e, new ArrayList<Object>());
		}

	}

	public void setBaseDirectory(String baseDirectory) {
		this.baseDirectory = baseDirectory;
	}

	public void setIncludePattern(String includePattern) {
		this.includePattern = includePattern;
	}

}
