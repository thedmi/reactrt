#include "EventQueue.h"

rrt::EventQueue::EventQueue()
{
	isActive = true;

	InitializeCriticalSection(&eventQueueCS);
	eventQueueNotEmptyAnymoreEvent = CreateEvent(NULL, false, false, NULL);
}

rrt::EventQueue::~EventQueue()
{
	CloseHandle(eventQueueNotEmptyAnymoreEvent);
	DeleteCriticalSection(&eventQueueCS);
}

rrt::IEventQueue::TElement rrt::EventQueue::getNext()
{
	TElement elem;

	EnterCriticalSection(&eventQueueCS);

	while (eventQueue.empty() && isActive) {
		LeaveCriticalSection(&eventQueueCS);
		WaitForSingleObject(eventQueueNotEmptyAnymoreEvent, INFINITE);
		EnterCriticalSection(&eventQueueCS);
	}
	if(isActive) {
		elem = eventQueue.front();
		eventQueue.pop_front();
	}

	LeaveCriticalSection(&eventQueueCS);

	return elem;
}

void rrt::EventQueue::insert( rrt::IEventQueue::TElement elem )
{
	EnterCriticalSection(&eventQueueCS);
	if(isActive) {
		if (eventQueue.empty()) {
			eventQueue.push_back(elem);
			SetEvent(eventQueueNotEmptyAnymoreEvent);
		} else {
			eventQueue.push_back(elem);
		}
	}
	LeaveCriticalSection(&eventQueueCS);
}

void rrt::EventQueue::deactivate()
{
	isActive = false;
	SetEvent(eventQueueNotEmptyAnymoreEvent);
}
