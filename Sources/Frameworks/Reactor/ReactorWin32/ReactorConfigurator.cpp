#include "ReactorConfigurator.h"

#include "ReactorRegistry.h"

#include "ReactorFactory.h"
#include "TimestampProvider.h"
#include "TimeEventScheduler.h"


rrt::ReactorConfigurator::ReactorConfigurator()
{
	ReactorFactory::Ptr reactorFactory(new ReactorFactory());
	TimestampProvider::Ptr timestampProvider(new TimestampProvider());
	TimeEventScheduler::Ptr timeEventScheduler(new TimeEventScheduler(timestampProvider));

	ReactorRegistry::Instance().setReactorFactory(reactorFactory);
	ReactorRegistry::Instance().setTimeEventScheduler(timeEventScheduler);
	ReactorRegistry::Instance().setTimestampProvider(timestampProvider);
}

rrt::ReactorConfigurator::~ReactorConfigurator()
{
	ReactorRegistry::Destroy();
}
