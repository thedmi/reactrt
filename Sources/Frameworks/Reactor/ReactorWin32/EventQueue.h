#pragma once

#include "IEventQueue.h"

#include <deque>
#include <Windows.h>

namespace rrt
{
	class EventQueue : public IEventQueue
	{
	public:
		EventQueue();
		virtual ~EventQueue();

		virtual TElement getNext();

		virtual void insert(TElement elem);

		virtual void deactivate();

		typedef boost::shared_ptr<EventQueue> Ptr;
		typedef boost::shared_ptr<const EventQueue> ConstPtr;

	private:

		std::deque<TElement> eventQueue;

		CRITICAL_SECTION eventQueueCS;
		HANDLE eventQueueNotEmptyAnymoreEvent;

		volatile bool isActive;

	};
}
