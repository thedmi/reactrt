#pragma once

/* This is a convenience header that includes all required header 
 * that are necessary to work with the ReactRT Reactor.
 */

#include <Reactor.h>
#include <ReactorRegistry.h>
#include <ReactorConfigurator.h>
#include <IReactorFactory.h>

#include <Event.h>
#include <IThread.h>