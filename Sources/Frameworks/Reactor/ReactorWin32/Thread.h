#pragma once

#include "IThread.h"

#include <Windows.h>
#include <string>


namespace rrt
{
	class IEventQueue;
	class IEventProcessor;
	class Event;


	class Thread : public IThread
	{
	public:
		Thread(IThread::Urgency urgency, std::string name = "");
		virtual ~Thread();

		virtual void start();

		virtual void enqueue(boost::shared_ptr<const Event> ev, boost::weak_ptr<IEventProcessor> target);

		virtual IThread::Urgency getUrgency() const;
		virtual std::string getName() const;

		typedef boost::shared_ptr<Thread> Ptr;
		typedef boost::shared_ptr<const Thread> ConstPtr;

	private:

		void run();
		void stop();

		void dispatch(boost::shared_ptr<const Event> ev, boost::shared_ptr<IEventProcessor> target);

		static DWORD WINAPI threadEntryPoint(LPVOID me);

		int win32ThreadPrio(IThread::Urgency urgency);

	private:

		boost::shared_ptr<IEventQueue> eventQueue;

		unsigned char urgency;

		std::string name;

		HANDLE threadHandle;

		volatile bool hasBeenStarted;
		volatile bool isRunning;
	};
}