#pragma once

namespace rrt
{
	/*
	 *	This class implements the RAII idiom and must thus be instantiated on the
	 *	stack. Instantiating it sets up the Reactor framework for use on Win32 systems.
	 *	Create an object before any other calls to Reactor. 
	 */
	class ReactorConfigurator
	{
	public:
		ReactorConfigurator();
		~ReactorConfigurator();

	};
	
}
