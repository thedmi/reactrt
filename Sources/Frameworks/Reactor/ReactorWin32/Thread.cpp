#include "Thread.h"

#include "ReactorRegistry.h"
#include "IReactorFactory.h"

#include "IEventQueue.h"
#include "IEventProcessor.h"
#include "Event.h"


rrt::Thread::Thread(rrt::IThread::Urgency urgency, std::string name) : urgency(urgency), name(name), isRunning(false), hasBeenStarted(false)
{
	eventQueue = ReactorRegistry::Instance().getReactorFactory()->createEventQueue(urgency, name);
}

rrt::Thread::~Thread()
{
	if(hasBeenStarted) {
		if(isRunning) {
			stop();
		}

		WaitForSingleObject(threadHandle, INFINITE);
		CloseHandle(threadHandle);
	}
}

void rrt::Thread::start()
{
	if(!hasBeenStarted) {
		hasBeenStarted = true;
		isRunning = true;

		threadHandle = CreateThread(NULL, 1024, &threadEntryPoint, this, NULL, NULL);
		SetThreadPriority(threadHandle, win32ThreadPrio(urgency)); 
	}
}

void rrt::Thread::stop()
{
	isRunning = false;
	eventQueue->deactivate();
}

void rrt::Thread::run()
{
	while(isRunning) {
		IEventQueue::TElement elem = eventQueue->getNext();
		if(isRunning && elem.first) {
			dispatch(elem.first, elem.second.lock());
		}
	}
}

void rrt::Thread::dispatch( boost::shared_ptr<const Event> ev, boost::shared_ptr<IEventProcessor> target )
{
	if (target) {
		if(ev->sig == Event::INITIALIZATION_SIGNAL) {
			target->init();
		} else {
			target->processEvent(ev);
		}
	}

}

void rrt::Thread::enqueue( boost::shared_ptr<const Event> ev, boost::weak_ptr<IEventProcessor> target )
{
	eventQueue->insert(std::make_pair(ev, target));
}

rrt::IThread::Urgency rrt::Thread::getUrgency() const
{
	return urgency;
}

std::string rrt::Thread::getName() const
{
	return name;
}

DWORD WINAPI rrt::Thread::threadEntryPoint( LPVOID me )
{
	((Thread *)(me))->run();
	return 0;
}

int rrt::Thread::win32ThreadPrio( rrt::IThread::Urgency urgency )
{
	if(urgency == 255)
		return THREAD_PRIORITY_TIME_CRITICAL;
	else if(urgency >= 200)
		return THREAD_PRIORITY_HIGHEST;
	else if(urgency >= 150)
		return THREAD_PRIORITY_ABOVE_NORMAL;
	else if(urgency >= 100)
		return THREAD_PRIORITY_NORMAL;
	else if(urgency >= 50)
		return THREAD_PRIORITY_BELOW_NORMAL;
	else if(urgency >= 1)
		return THREAD_PRIORITY_LOWEST;
	else 
		return THREAD_PRIORITY_IDLE;

}
