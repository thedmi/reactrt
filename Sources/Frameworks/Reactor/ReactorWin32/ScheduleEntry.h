#pragma once

#include <boost/smart_ptr.hpp>


namespace rrt 
{
	class Event;
	class IReactive;


	struct ScheduleEntry
	{
		boost::shared_ptr<const Event> ev;
		boost::weak_ptr<IReactive> target;
	};
}
