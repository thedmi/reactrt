#pragma once

#include "ITimestampProvider.h"


namespace rrt
{
	class TimestampProvider : public ITimestampProvider
	{
	public:
		virtual ~TimestampProvider() {}

		virtual unsigned long long monotonicMilliseconds();
		virtual unsigned long long monotonicMicroseconds();

		virtual long long unixTimestamp();

		typedef boost::shared_ptr<TimestampProvider> Ptr;
		typedef boost::shared_ptr<const TimestampProvider> ConstPtr;
	};
	
}
