#include "ReactorFactory.h"

#include "EventQueue.h"
#include "Thread.h"


boost::shared_ptr<rrt::IEventQueue> rrt::ReactorFactory::createEventQueue(IThread::Urgency threadUrgency, const std::string& threadName)
{
	EventQueue::Ptr queue(new EventQueue());
	assert(queue);
	return queue;
}

boost::shared_ptr<rrt::IThread> rrt::ReactorFactory::createThread( IThread::Urgency urgency, const std::string& name)
{
	Thread::Ptr thread(new Thread(urgency, name));
	assert(thread);
	return thread;
}

boost::shared_ptr<rrt::IThread> rrt::ReactorFactory::getDefaultThread()
{
	return createThread(10, "");
}
