#include "TimeEventScheduler.h"

#include "IReactive.h"
#include "ITimestampProvider.h"
#include "ScheduleEntry.h"
#include "Event.h"


rrt::TimeEventScheduler::TimeEventScheduler(boost::shared_ptr<ITimestampProvider> timestampProvider) 
	: timestampProvider(timestampProvider), isRunning(false), hasBeenStartedAsynchronously(false)
{
	InitializeCriticalSection(&timerMapCS);
	checkTimerExpirationEvent = CreateEvent(NULL, false, false, NULL);
}

rrt::TimeEventScheduler::~TimeEventScheduler()
{
	if(isRunning) {
		stop();
	}

	if(hasBeenStartedAsynchronously) {
		WaitForSingleObject(threadHandle, INFINITE);
		CloseHandle(threadHandle);
	}
	
	CloseHandle(checkTimerExpirationEvent);
	DeleteCriticalSection(&timerMapCS);
}

void rrt::TimeEventScheduler::scheduleTimeout( boost::shared_ptr<const Event> timeEvent, boost::weak_ptr<IReactive> target, unsigned long timeoutMilliseconds )
{
	unsigned long long postAt = timestampProvider->monotonicMilliseconds() + timeoutMilliseconds;

	ScheduleEntry entry = {timeEvent, target};

	EnterCriticalSection(&timerMapCS);
	armedTimers.insert(std::make_pair(postAt, entry));
	LeaveCriticalSection(&timerMapCS);

	SetEvent(checkTimerExpirationEvent);
}

void rrt::TimeEventScheduler::abortTimeouts( boost::shared_ptr<const Event> timeEvent )
{
	EnterCriticalSection(&timerMapCS);
	if(!armedTimers.empty()) {

		TTimerMap::iterator it = armedTimers.begin();

		while(it != armedTimers.end()) {
			if(it->second.ev == timeEvent) {
				armedTimers.erase(it++); // Side effect intentional and necessary
			} else {
				it++;
			}
		}
	}
	LeaveCriticalSection(&timerMapCS);
}

void rrt::TimeEventScheduler::start()
{
	hasBeenStartedAsynchronously = true;
	isRunning = true;
	threadHandle = CreateThread(NULL, 1024, threadEntryPoint, this, NULL, NULL);
}

void rrt::TimeEventScheduler::run()
{
	isRunning = true;
	processTimers();
}

void rrt::TimeEventScheduler::processTimers()
{
	while(isRunning) {
		checkTimerExpiration();
		WaitForSingleObject(checkTimerExpirationEvent, getSleepDuration());
	}
}

void rrt::TimeEventScheduler::stop()
{
	isRunning = false;
	SetEvent(checkTimerExpirationEvent);
}

void rrt::TimeEventScheduler::checkTimerExpiration()
{
	unsigned long long currentTime = timestampProvider->monotonicMilliseconds();

	EnterCriticalSection(&timerMapCS);
	if(!armedTimers.empty()) {
		TTimerMap::iterator it = armedTimers.begin();
		while(it != armedTimers.end() && it->first < currentTime) {
			postExpiredTimeoutEvent(it->second.ev, it->second.target);
			it++;
		}
		armedTimers.erase(armedTimers.begin(), it);
	}
	LeaveCriticalSection(&timerMapCS);
}

void rrt::TimeEventScheduler::postExpiredTimeoutEvent( boost::shared_ptr<const Event> timeEvent, boost::weak_ptr<IReactive> target )
{
	IReactive::Ptr lockedTarget = target.lock();
	if (lockedTarget) {
		lockedTarget->post(timeEvent);
	}
}

unsigned long rrt::TimeEventScheduler::getSleepDuration()
{
	bool hasAnotherArmedTimer = false;
	long sleepDuration = 0;

	EnterCriticalSection(&timerMapCS);
	if(!armedTimers.empty()) {
		hasAnotherArmedTimer = true;
		sleepDuration = static_cast<long>(armedTimers.begin()->first - timestampProvider->monotonicMilliseconds());
	}
	LeaveCriticalSection(&timerMapCS);

	sleepDuration = sleepDuration > 0 ? sleepDuration : 0;

	return hasAnotherArmedTimer ? sleepDuration : 100;
}

DWORD WINAPI rrt::TimeEventScheduler::threadEntryPoint( LPVOID me )
{
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
	((TimeEventScheduler*)me)->processTimers();
	return 0;
}
