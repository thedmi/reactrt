#pragma once

#include "ITimeEventScheduler.h"

#include <map>
#include <Windows.h>


namespace rrt
{
	class ITimestampProvider;

	struct ScheduleEntry;


	class TimeEventScheduler : public ITimeEventScheduler
	{
	public:

		TimeEventScheduler(boost::shared_ptr<ITimestampProvider> timestampProvider);
		virtual ~TimeEventScheduler();

		virtual void scheduleTimeout(boost::shared_ptr<const Event> timeEvent, boost::weak_ptr<IReactive> target, unsigned long timeoutMilliseconds);
		virtual void abortTimeouts(boost::shared_ptr<const Event> timeEvent);

		/*	The scheduler can be started synchronously by calling run(), or asynchronously by calling start().
		 *	Note that run() does not return until stop() is called from another thread.
		 */
		virtual void start();
		virtual void run();
		virtual void stop();

		typedef boost::shared_ptr<TimeEventScheduler> Ptr;
		typedef boost::shared_ptr<const TimeEventScheduler> ConstPtr;


	private:

		void processTimers();

		void checkTimerExpiration();

		unsigned long getSleepDuration();

		void postExpiredTimeoutEvent( boost::shared_ptr<const Event> timeEvent, boost::weak_ptr<IReactive> target );

		boost::shared_ptr<ITimestampProvider> timestampProvider;

		typedef std::multimap<unsigned long long, ScheduleEntry > TTimerMap;

		TTimerMap armedTimers;

		CRITICAL_SECTION timerMapCS;
		HANDLE checkTimerExpirationEvent;

		HANDLE threadHandle;

		volatile bool hasBeenStartedAsynchronously;
		volatile bool isRunning;


	public:

		static DWORD WINAPI threadEntryPoint(LPVOID arg);
	};

}
