#pragma once

#include "IReactorFactory.h"


namespace rrt
{
	class ReactorFactory : public rrt::IReactorFactory
	{
	public:
		virtual ~ReactorFactory() {}

		virtual boost::shared_ptr<IEventQueue> createEventQueue(IThread::Urgency threadUrgency, const std::string& threadName);

		virtual boost::shared_ptr<IThread> createThread(IThread::Urgency urgency, const std::string& name);
		virtual boost::shared_ptr<IThread> getDefaultThread();

		typedef boost::shared_ptr<ReactorFactory> Ptr;
		typedef boost::shared_ptr<const ReactorFactory> ConstPtr;
	};
	
}
