#include "TimestampProvider.h"

#include <Windows.h>


unsigned long long rrt::TimestampProvider::monotonicMicroseconds()
{
	FILETIME ft;
	unsigned long long now = 0;

	GetSystemTimeAsFileTime(&ft);

	now |= ft.dwHighDateTime;
	now <<= 32;
	now |= ft.dwLowDateTime;

	return now / 10;
}

unsigned long long rrt::TimestampProvider::monotonicMilliseconds()
{
	return monotonicMicroseconds() / 1000;
}

long long rrt::TimestampProvider::unixTimestamp()
{
	return static_cast<long long>(monotonicMicroseconds() / 1E6 - 11644473600i64);
}
