#include "ReactorRegistry.h"

#include <assert.h>


boost::scoped_ptr<rrt::ReactorRegistry> rrt::ReactorRegistry::instance;


rrt::ReactorRegistry::ReactorRegistry()
{
}

rrt::ReactorRegistry& rrt::ReactorRegistry::Instance()
{
	if(!instance) {
		instance.reset(new ReactorRegistry());
	}
	return *instance;
}

void rrt::ReactorRegistry::Destroy()
{
	assert(instance);
	instance.reset();
}

void rrt::ReactorRegistry::setReactorFactory( boost::shared_ptr<IReactorFactory> factory )
{
	reactorFactory = factory;
}


void rrt::ReactorRegistry::setTimeEventScheduler( boost::shared_ptr<rrt::ITimeEventScheduler> scheduler )
{
	timeEventScheduler = scheduler;
}

void rrt::ReactorRegistry::setTimestampProvider( boost::shared_ptr<rrt::ITimestampProvider> provider )
{
	timestampProvider = provider;
}

boost::shared_ptr<rrt::IReactorFactory> rrt::ReactorRegistry::getReactorFactory() const
{
	assert(reactorFactory);
	return reactorFactory;
}

boost::shared_ptr<rrt::ITimestampProvider> rrt::ReactorRegistry::getTimestampProvider() const
{
	assert(timestampProvider);
	return timestampProvider;
}

boost::shared_ptr<rrt::ITimeEventScheduler> rrt::ReactorRegistry::getTimeEventScheduler() const
{
	assert(timeEventScheduler);
	return timeEventScheduler;
}
