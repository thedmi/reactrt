#pragma once

#include <boost/smart_ptr.hpp>


namespace rrt
{
	class ITimestampProvider
	{
	public:
		virtual ~ITimestampProvider() {}
		
		virtual unsigned long long monotonicMilliseconds() = 0;
		virtual unsigned long long monotonicMicroseconds() = 0;

		virtual long long unixTimestamp() = 0;
	};
}
