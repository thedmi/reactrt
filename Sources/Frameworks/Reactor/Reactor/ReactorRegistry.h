#pragma once

#include <boost/smart_ptr.hpp>


namespace rrt
{
	class IReactorFactory;
	class ITimestampProvider;
	class ITimeEventScheduler;


	class ReactorRegistry
	{
	public:

		static ReactorRegistry& Instance();

		static void Destroy();

		void setReactorFactory(boost::shared_ptr<IReactorFactory> factory);
		void setTimestampProvider(boost::shared_ptr<ITimestampProvider> provider);
		void setTimeEventScheduler(boost::shared_ptr<ITimeEventScheduler> scheduler);

		boost::shared_ptr<IReactorFactory> getReactorFactory() const;
		boost::shared_ptr<ITimestampProvider> getTimestampProvider() const;
		boost::shared_ptr<ITimeEventScheduler> getTimeEventScheduler() const;

	private:

		ReactorRegistry();

		boost::shared_ptr<IReactorFactory> reactorFactory;

		boost::shared_ptr<ITimeEventScheduler> timeEventScheduler;
		boost::shared_ptr<ITimestampProvider> timestampProvider;

		static boost::scoped_ptr<ReactorRegistry> instance;
	};
}
