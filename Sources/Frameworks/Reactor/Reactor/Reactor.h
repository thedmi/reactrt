#pragma once


namespace rrt 
{
	/*	This class provides a control mechanism to start and stop the asynchronous services
	 *	of the Reactor framework, like the time event scheduler. Note that reactives run on 
	 *	their own thread, so these functions do not influence the reactives.
	 *	
	 *	It is possible to start the services asynchronously by calling start(), or 
	 *	synchronously by calling run(). In the latter case, the call will block until the
	 *	Reactor is stopped from another thread using stop().
	 */
	class Reactor 
	{
	public:

		static void start();
		static void run();
		static void stop();

	private:
		// This class cannot be instantiated
		Reactor() {};

	};

}