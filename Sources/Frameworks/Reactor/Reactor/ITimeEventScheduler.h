#pragma once

#include <boost/smart_ptr.hpp>


namespace rrt
{
	class Event;
	class IReactive;


	class ITimeEventScheduler
	{
	public:
		virtual ~ITimeEventScheduler() {}

		virtual void scheduleTimeout(boost::shared_ptr<const Event> timeEvent, boost::weak_ptr<IReactive> target, unsigned long timeoutMilliseconds) = 0;
		virtual void abortTimeouts(boost::shared_ptr<const Event> timeEvent) = 0;

		/*	Starts the scheduler on a dedicated thread and returns immediately. Use this 
		 *	if the main thread is required for something else, e.g. a GUI toolkit message
		 *	dispatcher.
		 */
		virtual void start() = 0;

		/*	Starts the scheduler synchronously. Thus, this method does not return until
		 *	stop() is called from another thread.
		 */
		virtual void run() = 0;

		/*	Signals the scheduler thread and terminates the scheduling loop.
		 */
		virtual void stop() = 0;
	};
}
