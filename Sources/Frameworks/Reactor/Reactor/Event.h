#pragma once

#include <boost/smart_ptr.hpp>


namespace rrt 
{
	class IReactive;


	class Event
	{
	public:
		Event(unsigned long signal);
		virtual ~Event();

		unsigned long sig;

		typedef boost::shared_ptr<Event> Ptr;
		typedef boost::shared_ptr<const Event> ConstPtr;

		static const unsigned long INITIALIZATION_SIGNAL;
	};
}

