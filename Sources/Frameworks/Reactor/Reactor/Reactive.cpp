#include "Reactive.h"

#include "ITimeEventScheduler.h"
#include "IThread.h"
#include "IReactorFactory.h"
#include "Event.h"

#include "ReactorRegistry.h"


rrt::Reactive::Reactive() : initializationEvent(new rrt::Event(rrt::Event::INITIALIZATION_SIGNAL))
{
}

rrt::Reactive::~Reactive()
{
}

void rrt::Reactive::start()
{
	start(ReactorRegistry::Instance().getReactorFactory()->getDefaultThread());
}

void rrt::Reactive::start(boost::shared_ptr<IThread> threadToRunOn)
{
	assignedThread = threadToRunOn;
	assignedThread->start();

	post(initializationEvent);
}

void rrt::Reactive::post( boost::shared_ptr<const rrt::Event> ev )
{
	assert(assignedThread);
	assignedThread->enqueue(ev, shared_from_this());
}

std::string rrt::Reactive::getName() const
{
	return "";
}

unsigned char rrt::Reactive::getUrgency() const
{
	return assignedThread->getUrgency();
}

void rrt::Reactive::armTimer( boost::shared_ptr<const Event> ev, unsigned long timeoutMilliseconds )
{
	ReactorRegistry::Instance().getTimeEventScheduler()->scheduleTimeout(ev, shared_from_this(), timeoutMilliseconds);
}

void rrt::Reactive::disarmTimers( boost::shared_ptr<const Event> ev )
{
	ReactorRegistry::Instance().getTimeEventScheduler()->abortTimeouts(ev);
}
