#pragma once

#include "IReactive.h"
#include "IEventProcessor.h"


namespace rrt {

	class IThread;
	class Event;


	class Reactive : public IReactive, public IEventProcessor, public boost::enable_shared_from_this<Reactive>
	{
	public:

		Reactive();
		virtual ~Reactive();

		virtual void start();
		virtual void start(boost::shared_ptr<IThread> threadToRunOn);

		virtual void post(boost::shared_ptr<const Event> ev);

		virtual std::string getName() const;
		virtual unsigned char getUrgency() const;

		typedef boost::shared_ptr<Reactive> Ptr;
		typedef boost::shared_ptr<const Reactive> ConstPtr;


	protected:

		virtual void armTimer(boost::shared_ptr<const Event> ev, unsigned long timeoutMilliseconds);
		virtual void disarmTimers(boost::shared_ptr<const Event> ev);

	private:

		boost::shared_ptr<IThread> assignedThread;

		const boost::shared_ptr<Event> initializationEvent;
	};
}

