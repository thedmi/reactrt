#pragma once

#include <boost/smart_ptr.hpp>

#include "IThread.h"


namespace rrt
{
	class IEventQueue;


	class IReactorFactory
	{
	public:
		virtual ~IReactorFactory() {}

		virtual boost::shared_ptr<IEventQueue> createEventQueue(IThread::Urgency threadUrgency, const std::string& threadName) = 0;

		virtual boost::shared_ptr<IThread> createThread(IThread::Urgency urgency, const std::string& name) = 0;
		virtual boost::shared_ptr<IThread> getDefaultThread() = 0;
	};
}
