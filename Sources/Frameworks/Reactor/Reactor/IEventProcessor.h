#pragma once

namespace rrt
{
	class Event;


	class IEventProcessor
	{
	public:
		virtual ~IEventProcessor() {}

		virtual void processEvent( boost::shared_ptr<const Event> ev ) = 0;
		virtual void init() = 0;

		typedef boost::shared_ptr<IEventProcessor> Ptr;
		typedef boost::shared_ptr<const IEventProcessor> ConstPtr;
	};
}
