#pragma once

#include <boost/smart_ptr.hpp>


namespace rrt
{
	class Event;
	class IThread;


	class IReactive
	{
	public:
		virtual ~IReactive() {}

		virtual void start(boost::shared_ptr<IThread> threadToRunOn) = 0;

		virtual void post(boost::shared_ptr<const Event> ev) = 0;

		virtual std::string getName() const = 0;
		virtual unsigned char getUrgency() const = 0;

		typedef boost::shared_ptr<IReactive> Ptr;
		typedef boost::shared_ptr<const IReactive> ConstPtr;
	};
}
