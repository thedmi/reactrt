
#include "Reactor.h"
#include "ReactorRegistry.h"
#include "ITimeEventScheduler.h"




void rrt::Reactor::start()
{
	ReactorRegistry::Instance().getTimeEventScheduler()->start();
}

void rrt::Reactor::run()
{
	ReactorRegistry::Instance().getTimeEventScheduler()->run();
}

void rrt::Reactor::stop()
{
	ReactorRegistry::Instance().getTimeEventScheduler()->stop();
}


