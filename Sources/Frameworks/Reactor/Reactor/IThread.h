#pragma once

#include <boost/smart_ptr.hpp>
#include <string>

namespace rrt
{
	class Event;
	class IEventProcessor;


	class IThread
	{
	public:
		typedef unsigned char Urgency;

	public:
		virtual ~IThread() {}

		virtual void start() = 0;

		virtual void enqueue(boost::shared_ptr<const Event> ev, boost::weak_ptr<IEventProcessor> target) = 0;

		virtual Urgency getUrgency() const = 0;
		virtual std::string getName() const = 0;

		typedef boost::shared_ptr<IThread> Ptr;
		typedef boost::shared_ptr<const IThread> ConstPtr;
	};
}
