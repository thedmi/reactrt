#pragma once

#include <boost/smart_ptr.hpp>


namespace rrt
{
	class Event;
	class IEventProcessor;

	class IEventQueue
	{
	public:
		virtual ~IEventQueue() {}

		typedef std::pair< boost::shared_ptr<const Event>, boost::weak_ptr<IEventProcessor> > TElement;

		virtual TElement getNext() = 0;

		virtual void insert(TElement elem) = 0;

		virtual void deactivate() = 0;

		typedef boost::shared_ptr<IEventQueue> Ptr;
		typedef boost::shared_ptr<const IEventQueue> ConstPtr;
	};
}
