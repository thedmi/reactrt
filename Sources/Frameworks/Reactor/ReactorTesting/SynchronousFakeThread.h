#pragma once

#include "IThread.h"

#include <deque>

namespace rrt 
{
	class Event;
	class IEventProcessor;
}

namespace testing 
{
	class SynchronousFakeThread : public rrt::IThread
	{
	public:

		SynchronousFakeThread();

		virtual void start();
		void enqueue(boost::shared_ptr<const rrt::Event> ev, boost::weak_ptr<rrt::IEventProcessor> target);

		virtual std::string getName() const;
		virtual Urgency getUrgency() const;

		typedef boost::shared_ptr<SynchronousFakeThread> Ptr;

	private:

		bool isCurrentlyActive;

		typedef std::pair<boost::shared_ptr<const rrt::Event>, boost::weak_ptr<rrt::IEventProcessor> > TQueueElem;
		std::deque<TQueueElem> eventQueue;

		void dispatch(boost::shared_ptr<const rrt::Event> ev, boost::weak_ptr<rrt::IEventProcessor> target);

	};
}
