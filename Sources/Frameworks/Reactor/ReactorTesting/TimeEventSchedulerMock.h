#pragma once

#include "ITimeEventScheduler.h"

#include "IReactive.h"

#include <gmock/gmock.h>
#include <boost/smart_ptr.hpp>


namespace testing 
{

	class TimeEventSchedulerMock : public rrt::ITimeEventScheduler
	{
	public:
		TimeEventSchedulerMock();

		MOCK_METHOD3(scheduleTimeout, void(boost::shared_ptr<const rrt::Event> timeEvent, boost::weak_ptr<rrt::IReactive> target, unsigned long timeoutMilliseconds) );
		MOCK_METHOD1(abortTimeouts, void(boost::shared_ptr<const rrt::Event> timeEvent) );

		MOCK_METHOD0(start, void() );
		MOCK_METHOD0(run, void() );
		MOCK_METHOD0(stop, void() );

		void postImmediately(boost::shared_ptr<const rrt::Event> timeEvent, boost::weak_ptr<rrt::IReactive> target);

		/*	By default, the time event scheduler mock is enabled, so it sends back time events immediately. 
		 *	If the test scenario requires that time events are temporarily disabled, the test can call 
		 *	disable(). Reactivation is possible by calling enable() thereafter.
		 */
		void enable();
		void disable();

		typedef boost::shared_ptr<TimeEventSchedulerMock> Ptr;

	private:
		bool isEnabled;
	};
}
