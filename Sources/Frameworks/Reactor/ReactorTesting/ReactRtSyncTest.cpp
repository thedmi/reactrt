#include "ReactRtSyncTest.h"

#include "ReactorRegistry.h"

#include "TestingReactorFactory.h"
#include "TimeEventSchedulerMock.h"

#include <boost/foreach.hpp>

testing::ReactRtSyncTest::ReactRtSyncTest()
{
	using namespace rrt;

	TestingReactorFactory::Ptr factory(new TestingReactorFactory());
	TimeEventSchedulerMock::Ptr scheduler(new NiceMock<TimeEventSchedulerMock>());

	ReactorRegistry::Instance().setReactorFactory(factory);
	ReactorRegistry::Instance().setTimeEventScheduler(scheduler);
}

testing::ReactRtSyncTest::~ReactRtSyncTest()
{
	rrt::ReactorRegistry::Destroy();
}

boost::shared_ptr<testing::TimeEventSchedulerMock> testing::ReactRtSyncTest::getTimeEventSchedulerMock() const
{
	return boost::shared_polymorphic_downcast<TimeEventSchedulerMock>(rrt::ReactorRegistry::Instance().getTimeEventScheduler());
}

void testing::ReactRtSyncTest::registerForIncrementalVerification( const boost::shared_ptr<void>& mock )
{
	incrementalVerificationMocks.push_back(mock.get());
}

void testing::ReactRtSyncTest::verifyAndClearIncrementalMockExpectations() const
{
	BOOST_FOREACH(void * mock, incrementalVerificationMocks) {
		Mock::VerifyAndClearExpectations(mock);
	}
}
