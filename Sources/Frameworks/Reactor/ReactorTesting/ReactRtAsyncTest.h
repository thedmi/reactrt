#pragma once

#include <gmock/gmock.h>
#include <boost/smart_ptr.hpp>


namespace rrt {
	class ReactorConfigurator;
}


namespace testing 
{
	class ReactRtAsyncTest : public Test
	{
	public:
		ReactRtAsyncTest(int timeout = 5000, bool startFramework = true);
		virtual ~ReactRtAsyncTest();

		bool waitForCompletion();

		void signalCompletion();

	private:
		boost::scoped_ptr<rrt::ReactorConfigurator>	configurator;

		void* completionBarrier;

		int timeout; 
		bool startFramework;

	};
}

