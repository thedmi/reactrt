#pragma once

#include <gmock/gmock.h>
#include <boost/smart_ptr.hpp>

#include <vector>


namespace testing 
{
	class TimeEventSchedulerMock;


	class ReactRtSyncTest : public Test
	{
	public:
		ReactRtSyncTest();
		virtual ~ReactRtSyncTest();

	protected:
		boost::shared_ptr<TimeEventSchedulerMock> getTimeEventSchedulerMock() const;

		void registerForIncrementalVerification(const boost::shared_ptr<void>& mock);
		void verifyAndClearIncrementalMockExpectations() const; 


	private:
		// This is a vector of void pointers, because GMock does not provide a common base for mocks
		// and takes the mock as void pointer itself.
		std::vector<void *> incrementalVerificationMocks;
	};
}
