#include "ReactRtAsyncTest.h"

#include "ReactorConfigurator.h"
#include "Reactor.h"

#include <Windows.h>


testing::ReactRtAsyncTest::ReactRtAsyncTest(int timeout, bool startFramework) : timeout(timeout), startFramework(startFramework)
{
	configurator.reset(new rrt::ReactorConfigurator());
	completionBarrier = CreateEvent(NULL, false, false, NULL);

	if(startFramework) {
		rrt::Reactor::start();
	}
}

testing::ReactRtAsyncTest::~ReactRtAsyncTest()
{
	if(startFramework) {
		rrt::Reactor::stop();
	}

	CloseHandle(completionBarrier);
	configurator.reset();
}

bool testing::ReactRtAsyncTest::waitForCompletion()
{
	DWORD waitResult = WaitForSingleObject(completionBarrier, timeout);
	Sleep(20);
	return waitResult == WAIT_OBJECT_0;
}

void testing::ReactRtAsyncTest::signalCompletion()
{
	SetEvent(completionBarrier);
}
