#include "TestingReactorFactory.h"

#include "SynchronousFakeThread.h"


using namespace rrt;


boost::shared_ptr<IThread> testing::TestingReactorFactory::getDefaultThread()
{
	return createThread(0, "");
}

boost::shared_ptr<IEventQueue> testing::TestingReactorFactory::createEventQueue(IThread::Urgency threadUrgency, const std::string& threadName)
{
	throw std::exception("There is currently no event queue implementation in the testing package.");
}

boost::shared_ptr<IThread> testing::TestingReactorFactory::createThread( IThread::Urgency urgency, const std::string& name )
{
	SynchronousFakeThread::Ptr t(new SynchronousFakeThread());
	return t;
}
