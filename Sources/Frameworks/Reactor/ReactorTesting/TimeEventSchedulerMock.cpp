#include "TimeEventSchedulerMock.h"


testing::TimeEventSchedulerMock::TimeEventSchedulerMock() : isEnabled(true)
{
	ON_CALL(*this, scheduleTimeout(_, _, _))
		.WillByDefault(
			WithArgs<0,1>(
				Invoke(this, &TimeEventSchedulerMock::postImmediately)
			)
		);
}


void testing::TimeEventSchedulerMock::postImmediately( boost::shared_ptr<const rrt::Event> timeEvent, boost::weak_ptr<rrt::IReactive> target )
{
	if (isEnabled) {
		rrt::IReactive::Ptr lockedTarget = target.lock();
		if (lockedTarget) {
			lockedTarget->post(timeEvent);
		}
	}
}

void testing::TimeEventSchedulerMock::enable()
{
	isEnabled = true;
}

void testing::TimeEventSchedulerMock::disable()
{
	isEnabled = false;
}
