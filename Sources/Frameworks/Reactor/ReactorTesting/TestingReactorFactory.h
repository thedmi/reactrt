#pragma once

#include "IReactorFactory.h"

namespace testing 
{
	class TestingReactorFactory : public rrt::IReactorFactory
	{
	public:
		virtual boost::shared_ptr<rrt::IThread> getDefaultThread();
		virtual boost::shared_ptr<rrt::IEventQueue> createEventQueue(rrt::IThread::Urgency threadUrgency, const std::string& threadName);
		virtual boost::shared_ptr<rrt::IThread> createThread( rrt::IThread::Urgency urgency, const std::string& name );

		typedef boost::shared_ptr<TestingReactorFactory> Ptr;
	};

}
