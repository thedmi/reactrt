#include "SynchronousFakeThread.h"


#include "Event.h"
#include "IEventProcessor.h"


testing::SynchronousFakeThread::SynchronousFakeThread() : isCurrentlyActive(false)
{
}

void testing::SynchronousFakeThread::enqueue( boost::shared_ptr<const rrt::Event> ev, boost::weak_ptr<rrt::IEventProcessor> target )
{
	eventQueue.push_back(std::make_pair(ev, target));

	if (!isCurrentlyActive) {
		isCurrentlyActive = true;

		while(eventQueue.size() > 0) {
			TQueueElem next = eventQueue.front();
			eventQueue.pop_front();

			dispatch(next.first, next.second);
		}

		isCurrentlyActive = false;
	}
}

void testing::SynchronousFakeThread::dispatch( boost::shared_ptr<const rrt::Event> ev, boost::weak_ptr<rrt::IEventProcessor> target )
{
	rrt::IEventProcessor::Ptr lockedTarget = target.lock();
	if (lockedTarget) {
		if (ev->sig == rrt::Event::INITIALIZATION_SIGNAL) {
			lockedTarget->init();
		} else {
			lockedTarget->processEvent(ev);
		}
	}
}

void testing::SynchronousFakeThread::start()
{
}

std::string testing::SynchronousFakeThread::getName() const
{
	return "(synchronous fake thread)";
}

rrt::IThread::Urgency testing::SynchronousFakeThread::getUrgency() const
{
	return 0;
}
