#include "StateMachine1.h"

#include <iostream>
#include <Reactor.h>
#include <Event.h>

using namespace std;
using namespace rrt;



StateMachine1::StateMachine1()
{
	steps = 0;
}

StateMachine1::~StateMachine1()
{
}

void StateMachine1::processEvent( boost::shared_ptr<const rrt::Event> ev )
{
	steps++;
	cout << "Dispatching, step " << steps << endl;

	if(steps < 10) {
		armTimer(evTimeout1, 200);
	} else {
		rrt::Reactor::stop();
	}
}

void StateMachine1::init()
{
	evTimeout1.reset(new Event(sigTimeout1));

	cout << "Inited..." << endl;
}
