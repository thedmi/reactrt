

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>



#define BOOST_SP_NO_SP_CONVERTIBLE 
#include <boost/smart_ptr.hpp>


#include <tchar.h>

#include "StateMachine1.h"

#include <Reactor.h>
#include <ReactorConfigurator.h>

#include <Event.h>

#include <Windows.h>


using namespace rrt;

int _tmain(int argc, _TCHAR* argv[])
{
	{
		ReactorConfigurator configurator;


		boost::shared_ptr<StateMachine1> sm1(new StateMachine1());
		sm1->start();

		Event::Ptr ev1(new Event(StateMachine1::sigHello));
		sm1->post(ev1);

		Sleep(100);


		Reactor::run();
	}

	_CrtDumpMemoryLeaks();
	return 0;
}

