#pragma once

#include <boost/enable_shared_from_this.hpp>

#include <Reactive.h>

class StateMachine1 : public rrt::Reactive
{

public:

	StateMachine1(void);
	
	virtual ~StateMachine1(void);

	enum Signals {
		sigHello,
		sigTimeout1
	};


	virtual void init();
	virtual void processEvent( boost::shared_ptr<const rrt::Event> ev );

private:

	boost::shared_ptr<const rrt::Event> evTimeout1;

	int steps;

};
