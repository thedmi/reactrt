#pragma once

#include <boost/smart_ptr.hpp>

#include <gmock/gmock.h>


#include "ITimestampProvider.h"


namespace rrt 
{
	class TimestampProviderMock : public ITimestampProvider
	{
	public:

		MOCK_METHOD0( monotonicMilliseconds, unsigned long long() );
		MOCK_METHOD0( monotonicMicroseconds, unsigned long long() );

		MOCK_METHOD0( unixTimestamp, long long() );


		typedef boost::shared_ptr<TimestampProviderMock> Ptr;
	};

}

