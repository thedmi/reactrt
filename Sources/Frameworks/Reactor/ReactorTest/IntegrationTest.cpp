#include "IntegrationTest.h"

#include <gmock/gmock.h>

#include "Reactive.h"
#include "Thread.h"
#include "Event.h"

using namespace rrt;
using namespace boost;
using namespace testing;



class ConcreteReactiveMock : public Reactive
{
public:
	MOCK_METHOD1( processEvent, void(boost::shared_ptr<const Event> ev) );
	MOCK_METHOD0( init, void() );

	static const shared_ptr<const Event> ev1;
	static const shared_ptr<const Event> ev2;

	typedef boost::shared_ptr<ConcreteReactiveMock> Ptr;
};

const shared_ptr<const Event> ConcreteReactiveMock::ev1 = shared_ptr<const Event>(new Event(1));
const shared_ptr<const Event> ConcreteReactiveMock::ev2 = shared_ptr<const Event>(new Event(2));


TEST_F(IntegrationTest, OneThreadManyReactives)
{
	const int numberOfReactives = 5;

	ConcreteReactiveMock::Ptr reactives[numberOfReactives];
	ConcreteReactiveMock::Ptr syncReactive(new ConcreteReactiveMock());

	for(int i = 0; i < numberOfReactives; i++) {

		reactives[i].reset(new ConcreteReactiveMock());

		{
			InSequence;
			EXPECT_CALL(*reactives[i], init());
			EXPECT_CALL(*reactives[i], processEvent(ConcreteReactiveMock::ev1)).RetiresOnSaturation();
			EXPECT_CALL(*reactives[i], processEvent(ConcreteReactiveMock::ev2)).RetiresOnSaturation();
			EXPECT_CALL(*reactives[i], processEvent(ConcreteReactiveMock::ev1)).RetiresOnSaturation();
		}
	}

	EXPECT_CALL(*syncReactive, init())
		.WillOnce(InvokeWithoutArgs(this, &ReactRtAsyncTest::signalCompletion));


	Thread::Ptr thread1(new Thread(10));

	for(int i = 0; i < numberOfReactives; i++) {
		reactives[i]->start(thread1);
		reactives[i]->post(ConcreteReactiveMock::ev1);
		reactives[i]->post(ConcreteReactiveMock::ev2);
		reactives[i]->post(ConcreteReactiveMock::ev1);
	}

	syncReactive->start(thread1);

	EXPECT_TRUE(waitForCompletion());

}
