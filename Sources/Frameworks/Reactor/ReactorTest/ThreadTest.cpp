#include "ThreadTest.h"

#include "Thread.h"
#include "Event.h"

#include "EventProcessorMock.h"

#include <Windows.h>


using namespace rrt;
using namespace testing;


boost::shared_ptr<rrt::Event> ThreadTest::createEvent( unsigned long signal )
{
	Event::Ptr ev(new Event(signal));
	return ev;
}


TEST_F(ThreadTest, NotStarted) 
{
	const int urgency = 10;
	
	Thread::Ptr aThread(new Thread(urgency));
	EXPECT_EQ(urgency, aThread->getUrgency());

	aThread.reset();
}

TEST_F(ThreadTest, SingleReactiveInit)
{
	Thread::Ptr aThread(new Thread(10));
	EventProcessorMock::Ptr reactive(new EventProcessorMock());

	EXPECT_CALL(*reactive, init())
		.WillOnce(InvokeWithoutArgs(this, &ReactRtAsyncTest::signalCompletion));

	aThread->start();

	aThread->enqueue(createEvent(Event::INITIALIZATION_SIGNAL), reactive);

	EXPECT_TRUE(waitForCompletion());
}

TEST_F(ThreadTest, MultiReactiveDispatch)
{
	Thread::Ptr aThread(new Thread(10));

	EventProcessorMock::Ptr reactive1(new EventProcessorMock());
	EventProcessorMock::Ptr reactive2(new EventProcessorMock());
	EventProcessorMock::Ptr reactive3(new EventProcessorMock());

	EventProcessorMock::Ptr completionReactive(new EventProcessorMock());

	{
		InSequence;
		EXPECT_CALL(*reactive1, init());
		EXPECT_CALL(*reactive1, processEvent(Pointee(Field(&Event::sig, 1))));
		EXPECT_CALL(*reactive1, processEvent(Pointee(Field(&Event::sig, 2))));
	}
	{
		InSequence;
		EXPECT_CALL(*reactive2, init());
		EXPECT_CALL(*reactive2, processEvent(Pointee(Field(&Event::sig, 3))));
		EXPECT_CALL(*reactive2, processEvent(Pointee(Field(&Event::sig, 1)))).Times(3);
		EXPECT_CALL(*reactive2, processEvent(Pointee(Field(&Event::sig, 2))));
	}
	{
		InSequence;
		EXPECT_CALL(*reactive3, init());
		EXPECT_CALL(*reactive3, processEvent(Pointee(Field(&Event::sig, 1))));
		EXPECT_CALL(*reactive3, processEvent(Pointee(Field(&Event::sig, 4))));
	}

	EXPECT_CALL(*completionReactive, processEvent(_))
		.WillOnce(InvokeWithoutArgs(this, &ReactRtAsyncTest::signalCompletion));

	aThread->start();

	aThread->enqueue(createEvent(Event::INITIALIZATION_SIGNAL), reactive1);
	aThread->enqueue(createEvent(1), reactive1);

	aThread->enqueue(createEvent(Event::INITIALIZATION_SIGNAL), reactive2);

	aThread->enqueue(createEvent(Event::INITIALIZATION_SIGNAL), reactive3);

	aThread->enqueue(createEvent(3), reactive2);
	aThread->enqueue(createEvent(1), reactive2);

	aThread->enqueue(createEvent(1), reactive3);
	aThread->enqueue(createEvent(4), reactive3);

	aThread->enqueue(createEvent(2), reactive1);

	aThread->enqueue(createEvent(1), reactive2);
	aThread->enqueue(createEvent(1), reactive2);
	aThread->enqueue(createEvent(2), reactive2);

	aThread->enqueue(createEvent(0), completionReactive);

	EXPECT_TRUE(waitForCompletion());
}