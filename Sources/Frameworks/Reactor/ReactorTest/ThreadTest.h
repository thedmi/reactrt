#pragma once

#include <boost/smart_ptr.hpp>

#include "ReactRtAsyncTest.h"


namespace rrt 
{
	class Event;
	class IReactive;
}


class ThreadTest : public testing::ReactRtAsyncTest
{
protected:
	boost::shared_ptr<rrt::Event> createEvent(unsigned long signal);

};
