#pragma once

#include <boost/smart_ptr.hpp>

#include <gmock/gmock.h>


#include "IReactive.h"
#include "IThread.h"
#include "Event.h"

#include <string>


namespace rrt 
{
	class ReactiveMock : public IReactive
	{
	public:

		MOCK_METHOD1( start, void(boost::shared_ptr<IThread> threadToRunOn) );
		MOCK_METHOD1( post, void(boost::shared_ptr<const Event> ev) );

		MOCK_CONST_METHOD0( getName, std::string() );
		MOCK_CONST_METHOD0( getUrgency, unsigned char() );

		typedef boost::shared_ptr<ReactiveMock> Ptr;
	};

}

