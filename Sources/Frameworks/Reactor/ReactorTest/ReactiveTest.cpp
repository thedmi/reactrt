#include "ReactiveTest.h"

#include <boost/smart_ptr.hpp>

#include <Reactive.h>
#include <Event.h>

#include "ThreadMock.h"

using namespace rrt;
using namespace boost;
using namespace testing;


class DummyReactive : public Reactive 
{
	void processEvent( boost::shared_ptr<const Event> ev ) {}
	void init() {}
};



MATCHER_P(WeakPointersEq, refWeakPointer, 
		  std::string(negation ? "points" : "doesn't point") + " to the reference object") 
{ 
	return arg.lock() == refWeakPointer.lock();
}


TEST_F(ReactiveTest, Start)
{
	// Scope required for clean shutdown
	{
		ThreadMock::Ptr aThread(new ThreadMock());

		boost::shared_ptr<DummyReactive> reactive(new DummyReactive());
		boost::weak_ptr<DummyReactive> weakReactive = reactive;

		{
			InSequence;

			EXPECT_CALL(*aThread, start());

			EXPECT_CALL(*aThread, enqueue(
				Pointee(Field(&Event::sig, Event::INITIALIZATION_SIGNAL)), 
				_
			));

			EXPECT_CALL(*aThread, enqueue(
				Pointee(Field(&Event::sig, 123)),
				WeakPointersEq(weakReactive)
			));
		}

		reactive->start(aThread);

		Event::Ptr ev(new Event(123));
		reactive->post(ev);
	}
}
