
#include <tchar.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>


int _tmain(int argc, _TCHAR* argv[])
{
	int res = 0;

	{
		testing::InitGoogleMock(&argc, argv);
		res = RUN_ALL_TESTS(); 

	}

	std::getchar();
	return res;
}
