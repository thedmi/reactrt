#pragma once

#include <boost/smart_ptr.hpp>

#include <gmock/gmock.h>


#include "IReactive.h"
#include "IThread.h"
#include "Event.h"

#include <string>

namespace rrt 
{
	class ThreadMock : public IThread
	{
	public:

		MOCK_METHOD0( start, void() );
		MOCK_METHOD2( enqueue, void(boost::shared_ptr<const Event> ev, boost::weak_ptr<IEventProcessor> target) );

		MOCK_CONST_METHOD0( getUrgency, IThread::Urgency() );
		MOCK_CONST_METHOD0( getName, std::string() );


		typedef boost::shared_ptr<ThreadMock> Ptr;
	};
}



