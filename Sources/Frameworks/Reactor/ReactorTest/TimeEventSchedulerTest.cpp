#include "TimeEventSchedulerTest.h"

#include <gtest/gtest.h>
#include <boost/smart_ptr.hpp>

#include <TimeEventScheduler.h>

#include "TimestampProviderMock.h"
#include "ReactiveMock.h"



using namespace rrt;
using namespace testing;



TEST_F(TimeEventSchedulerTest, SimpleTimeout) 
{
	TimestampProviderMock::Ptr timestampProvider(new TimestampProviderMock());
	ReactiveMock::Ptr reactive(new ReactiveMock());

	TimeEventScheduler::Ptr scheduler(new TimeEventScheduler(timestampProvider));

	Event::Ptr ev(new Event(42));

	{
		InSequence;

		EXPECT_CALL(*timestampProvider, monotonicMilliseconds())
			.Times(Between(3, 5))
			.WillOnce(Return(10))
			.WillOnce(Return(20))
			.WillOnce(Return(30))
			.WillOnce(Return(40))
			.WillOnce(Return(50));

		EXPECT_CALL(*reactive, post( Pointee(Field(&Event::sig, 42)) ))
			.WillOnce(InvokeWithoutArgs(scheduler.get(), &TimeEventScheduler::stop));
	}

	scheduler->scheduleTimeout(ev, reactive, 20);

	scheduler->run();
}


TEST_F(TimeEventSchedulerTest, OverlappingTimeouts) 
{

	TimestampProviderMock::Ptr timestampProvider(new TimestampProviderMock());
	ReactiveMock::Ptr reactive1(new ReactiveMock());
	ReactiveMock::Ptr reactive2(new ReactiveMock());
	ReactiveMock::Ptr reactive3(new ReactiveMock());

	TimeEventScheduler::Ptr scheduler(new TimeEventScheduler(timestampProvider));

	Event::Ptr ev1(new Event(42));
	Event::Ptr ev2(new Event(43));
	Event::Ptr ev3(new Event(44));
	Event::Ptr ev4(new Event(45));


	EXPECT_CALL(*timestampProvider, monotonicMilliseconds())
		.Times(Between(8, 20))
		.WillOnce(Return(10))
		.WillOnce(Return(20))
		.WillOnce(Return(30))
		.WillOnce(Return(40))
		.WillOnce(Return(50))
		.WillOnce(Return(60))
		.WillOnce(Return(70))
		.WillOnce(Return(80))
		.WillOnce(Return(90))
		.WillOnce(Return(100))
		.WillOnce(Return(110))
		.WillOnce(Return(120))
		.WillOnce(Return(130))
		.WillOnce(Return(140))
		.WillOnce(Return(150))
		.WillOnce(Return(160))
		.WillOnce(Return(170))
		.WillOnce(Return(180))
		.WillOnce(Return(190))
		.WillOnce(Return(200));

	{
		InSequence;

		EXPECT_CALL(*reactive1, post( Pointee(Field(&Event::sig, 42)) ));
		EXPECT_CALL(*reactive2, post( Pointee(Field(&Event::sig, 43)) ));
		EXPECT_CALL(*reactive3, post( Pointee(Field(&Event::sig, 44)) )).Times(2);
		EXPECT_CALL(*reactive1, post( Pointee(Field(&Event::sig, 45)) ))
			.WillOnce(InvokeWithoutArgs(scheduler.get(), &TimeEventScheduler::stop));
	}

	scheduler->scheduleTimeout(ev1, reactive1, 30);
	scheduler->scheduleTimeout(ev2, reactive2, 20);
	scheduler->scheduleTimeout(ev3, reactive3, 90);
	scheduler->scheduleTimeout(ev3, reactive3, 100);
	scheduler->scheduleTimeout(ev4, reactive1, 110);

	scheduler->run();
}



TEST_F(TimeEventSchedulerTest, AbortTimeouts) 
{
	TimestampProviderMock::Ptr timestampProvider(new TimestampProviderMock());
	ReactiveMock::Ptr reactive(new ReactiveMock());

	TimeEventScheduler::Ptr scheduler(new TimeEventScheduler(timestampProvider));

	Event::Ptr ev1(new Event(1));
	Event::Ptr ev2(new Event(2));
	Event::Ptr ev3(new Event(3));
	Event::Ptr ev4(new Event(4));

	{
		InSequence;

		EXPECT_CALL(*timestampProvider, monotonicMilliseconds())
			.Times(Between(1, 20))
			.WillOnce(Return(10))
			.WillOnce(Return(20))
			.WillOnce(Return(30))
			.WillOnce(Return(40))
			.WillOnce(Return(50))
			.WillOnce(Return(60))
			.WillOnce(Return(70))
			.WillOnce(Return(80))
			.WillOnce(Return(90))
			.WillOnce(Return(100))
			.WillOnce(Return(110))
			.WillOnce(Return(120))
			.WillOnce(Return(130))
			.WillOnce(Return(140))
			.WillOnce(Return(150))
			.WillOnce(Return(160))
			.WillOnce(Return(170))
			.WillOnce(Return(180))
			.WillOnce(Return(190))
			.WillOnce(Return(200));


		EXPECT_CALL(*reactive, post(Eq(ev2))).RetiresOnSaturation();
		EXPECT_CALL(*reactive, post(Eq(ev2))).RetiresOnSaturation();
		EXPECT_CALL(*reactive, post(Eq(ev3))).RetiresOnSaturation();
		EXPECT_CALL(*reactive, post(Eq(ev2))).RetiresOnSaturation();

		EXPECT_CALL(*reactive, post(Eq(ev4)))
			.WillOnce(InvokeWithoutArgs(scheduler.get(), &TimeEventScheduler::stop));
	}

	scheduler->scheduleTimeout(ev2, reactive, 10);
	scheduler->scheduleTimeout(ev1, reactive, 10);
	scheduler->scheduleTimeout(ev2, reactive, 10);
	scheduler->scheduleTimeout(ev3, reactive, 10);
	scheduler->scheduleTimeout(ev1, reactive, 10);
	scheduler->scheduleTimeout(ev1, reactive, 10);
	scheduler->scheduleTimeout(ev2, reactive, 10);
	scheduler->scheduleTimeout(ev1, reactive, 10);
	scheduler->scheduleTimeout(ev4, reactive, 10);
	scheduler->abortTimeouts(ev1);

	scheduler->run();

}



