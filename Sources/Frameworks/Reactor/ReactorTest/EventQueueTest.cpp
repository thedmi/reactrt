#include "EventQueueTest.h"

#include "EventQueue.h"
#include "Event.h"
#include "IEventProcessor.h"

#include <Windows.h>

#include <vector>


using namespace rrt;
using namespace boost;


TEST_F(EventQueueTest, NonblockingInsertAndGet)
{
	EventQueue::Ptr queue(new EventQueue());

	Event::Ptr ev1(new Event(0));
	Event::Ptr ev2(new Event(1));
	Event::Ptr ev3(new Event(2));

	IEventProcessor::Ptr d;

	queue->insert(std::make_pair(ev1, d));
	queue->insert(std::make_pair(ev2, d));
	queue->insert(std::make_pair(ev3, d));
	queue->insert(std::make_pair(ev1, d));

	ASSERT_EQ(queue->getNext().first->sig, 0);
	ASSERT_EQ(queue->getNext().first->sig, 1);
	ASSERT_EQ(queue->getNext().first->sig, 2);
	ASSERT_EQ(queue->getNext().first->sig, 0);
}



class TaggedEvent : public Event {
public: 
	TaggedEvent(int threadId, int sig) : Event(sig), threadId(threadId) {}

	int threadId;
};

struct ThrashingSpec {
	int threadId;
	shared_ptr<EventQueue> queue;
};

DWORD WINAPI queueThrashingThreadEntry( LPVOID tp )
{
	ThrashingSpec* spec = ((ThrashingSpec *)tp);

	IEventProcessor::Ptr d;

	for(int i = 0; i < 50; i++) {
		shared_ptr<TaggedEvent> ev(new TaggedEvent(spec->threadId, i));
		spec->queue->insert(std::make_pair(ev, d));
	}
	return 0;
}

TEST_F(EventQueueTest, QueueThrashing)
{
	const int numberOfThreads = 50;
	const int numberOfEventsPerThread = 50;

	HANDLE threadHandles[numberOfThreads];

	ThrashingSpec specs[numberOfThreads];

	EventQueue::Ptr queue(new EventQueue());


	for(int i = 0; i < numberOfThreads; i++) {
		specs[i].threadId = i;
		specs[i].queue = queue;

		threadHandles[i] = CreateThread(NULL, 1024, queueThrashingThreadEntry, &(specs[i]), NULL, NULL);
	}


	std::vector<std::pair<int, unsigned long> > receiveOrderSet;
	std::vector<unsigned long> controlSet[numberOfThreads];

	Sleep(200);

	for(int i = 0; i < numberOfThreads * numberOfEventsPerThread; i++) {
		shared_ptr<const TaggedEvent> ev = shared_polymorphic_downcast<const TaggedEvent>(queue->getNext().first);
		
		controlSet[ev->threadId].push_back(ev->sig);
		receiveOrderSet.push_back(std::make_pair(ev->threadId, ev->sig));
	}

	for(int i = 0; i < numberOfThreads; i++) {
		for(int j = 0; j < numberOfEventsPerThread; j++) {
			EXPECT_EQ(j, controlSet[i].at(j));
		}
	}

}