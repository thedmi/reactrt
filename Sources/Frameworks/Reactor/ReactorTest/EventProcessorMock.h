#pragma once

#include <boost/smart_ptr.hpp>

#include <gmock/gmock.h>


#include "IEventProcessor.h"
#include "IThread.h"
#include "Event.h"

#include <string>


namespace rrt 
{
	class EventProcessorMock : public IEventProcessor
	{
	public:

		MOCK_METHOD1( processEvent, void(boost::shared_ptr<const Event> ev) );
		MOCK_METHOD0( init, void() );

		typedef boost::shared_ptr<EventProcessorMock> Ptr;
	};

}

