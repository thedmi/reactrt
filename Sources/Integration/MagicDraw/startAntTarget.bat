@echo off

REM This is a redirection script that changes to the directory of 
REM the first parameter. MagicDraw can be configured to pass the 
REM project path with "$f" in the extenal tools dialog, which is 
REM the normal way of using this script.
REM 
REM The ant target to be called can be passed to this script as
REM second command line parameter.
REM

cd %~dp1
cd scripts

REM Start ant in a new console window and keep it open
start cmd /c "ant %2 & pause"
