
import subprocess
import shutil
import os


codeVerificationDir = 'CodeVerification/SystemVerification/gen/'
generatorDir = '../Code/SystemVerification/gen/'




def verify():
	__copySourceFiles()
	__verifyOutput()
	__verifySources()
	
	
def __copySourceFiles():
	for aFile in os.listdir(codeVerificationDir):
		filePath = os.path.join(codeVerificationDir, aFile)
		if os.path.isfile(filePath):
			os.unlink(filePath)
			
	for aFile in os.listdir(generatorDir):
		filePath = os.path.join(generatorDir, aFile)
		if os.path.isfile(filePath) and aFile.count('uncrustify') == 0:
			shutil.copyfile(filePath, os.path.join(codeVerificationDir, aFile))


def __verifyOutput():
	p = subprocess.Popen(['../Code/Debug/SystemVerification.exe'], stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True)

	(stdout, stderr) = p.communicate()

	referenceFile = open('referenceOutput.txt', 'r')
	reference = referenceFile.read()

	outputFile = open('actualOutput.txt', 'w')
	outputFile.write(stdout)

	if (stdout == reference):
		print "Verification executable output test: Passed"
	else:
		print "Verification executable output test: FAILED!"
		print ""
		print stdout
		
		
def __verifySources():
	p = subprocess.Popen(['svn', 'diff', codeVerificationDir], stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True)

	(stdout, stderr) = p.communicate()
	
	if (stdout.strip() == ""):
		print "Source diff test: Passed"
	else:
		print "Source diff test: FAILED!"
		

if __name__ == "__main__":
	verify()

