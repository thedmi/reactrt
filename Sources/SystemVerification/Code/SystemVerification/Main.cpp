#include <tchar.h>
#include <iostream>

#include <Reactor.h>
#include <ReactorConfigurator.h>

#include "gen/TestRunner.h"

#include <boost/smart_ptr.hpp>


int main(int argc, _TCHAR* argv[]) {

	using namespace std;
	using namespace rrt;
	
	ReactorConfigurator configurator;

	cout << "ReactRT System Verification" << endl;
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;


	boost::shared_ptr<TestRunner> testRunner(new TestRunner());
	testRunner->start();

	Reactor::run();
	
}

