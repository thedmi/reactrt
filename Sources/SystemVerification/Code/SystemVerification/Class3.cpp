
#include "gen/Class3.h"
#include "gen/Interface2.h"

Class3::Class3()
{

}

Class3::~Class3()
{

}

void Class3::baseOp1()
{

}

void Class3::baseOp2()
{

}

void Class3::baseOp3()
{

}

void Class3::op3( int param1 )
{

}

void Class3::op4() const
{

}

void Class3::abstractOp() const
{

}

const Interface2::Color Class3::HIGHLIGHT_COLOR = Interface2::Blue;
