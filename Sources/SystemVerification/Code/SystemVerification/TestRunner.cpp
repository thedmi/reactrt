
#include "gen/TestRunner.h"

#include "gen/StateNestingTest.h"
#include "gen/EventTypesTest.h"
#include "gen/GuardsTest.h"

#include "gen/Class1.h"
#include "gen/Class3.h"
#include "gen/Class4.h"
#include "gen/Class5.h"

#include "gen/ParameterTest1.h"
#include "gen/ParameterTest2.h"

#include "gen/GetterSetterTest.h"
#include "gen/DataClassTest.h"

#include <iostream>

#include <boost/smart_ptr.hpp>

#include <Reactor.h>

#include <Windows.h>


using namespace std;
using namespace boost;


TestRunner::TestRunner()
{
	initialTimeout = 500;
}

TestRunner::~TestRunner()
{

}

void TestRunner::terminate()
{

	rrt::Reactor::stop();
}

void TestRunner::startStateNesting()
{
	cout << endl << "-- StateNestingTest --" << endl;

	stateNestingTest.reset(new StateNestingTest());

	stateNestingTest->start();

	stateNestingTest->post(StateNestingTest::evA);
	stateNestingTest->post(StateNestingTest::evA);
	stateNestingTest->post(StateNestingTest::evB);
	stateNestingTest->post(StateNestingTest::evA);
	stateNestingTest->post(StateNestingTest::evB);
}

void TestRunner::startEventTypes()
{
	cout << endl << "-- EventTypesTest --" << endl;

	eventTypesTest.reset(new EventTypesTest());

	eventTypesTest->start();

	EventTypesTest::AEvent::Ptr aEvent(new EventTypesTest::AEvent());
	aEvent->param1 = "I am param one";
	aEvent->param2 = "I am param two";

	eventTypesTest->post(aEvent);
}

void TestRunner::startGuards()
{
	cout << endl << "-- GuardsTest --" << endl;

	guardsTest.reset(new GuardsTest());

	guardsTest->start();

	GuardsTest::BEvent::Ptr bEvent1(new GuardsTest::BEvent());
	GuardsTest::BEvent::Ptr bEvent2(new GuardsTest::BEvent());
	bEvent1->param = 100;
	bEvent2->param = 0;

	guardsTest->post(GuardsTest::evA);
	guardsTest->post(GuardsTest::evA);
	guardsTest->post(bEvent1);
	guardsTest->post(bEvent2);

	Sleep(100);

	guardsTest->post(bEvent1);
}

void TestRunner::stopStateNesting()
{
	stateNestingTest->post(StateNestingTest::evTerminate);
}

void TestRunner::stopEventTypes()
{
	eventTypesTest->post(EventTypesTest::evTerminate);
}

void TestRunner::stopGuards()
{
	guardsTest->post(GuardsTest::evTerminate);
}

void TestRunner::testStructuralElements()
{
	shared_ptr<Class1> c1(new Class1());
	shared_ptr<Class3> c3(new Class3());
	shared_ptr<Class4> c4(new Class4());

	Interface2::Color x = Interface2::DEFAULT_COLOR;

	ParameterTest1::Ptr pt1(new ParameterTest1());

	ParameterTest1::ParameterTestVector vec;

	ParameterTest2::Ptr pt2(new ParameterTest2(vec));
	
	ParameterTest2::getVec();

	GetterSetterTest::Ptr getSetTest(new GetterSetterTest());
	getSetTest->setAttribute1("hello");

	double theAnswer = getSetTest->getAttribute2();

}
