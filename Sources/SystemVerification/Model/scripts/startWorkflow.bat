@echo off

REM cd to batch script directory...
pushd %~dp0

REM Load base dir configuration from environment variable
set baseDir=%ReactRT_Base%

REM Note that we need to use forward slashes so that the WorkflowRunner 
REM (a Java program) finds the path. However, it does not matter if 
REM some slashes in between are backslashes.

set modelDir=file://%~dp0/../export
set modelFile=SystemVerification.uml
set outletPath=%~dp0/../../Code

echo Using model directory %modelDir%
echo Using model file %modelFile%
echo Using outlet path %outletPath%

start cmd /c "%baseDir%\Generator\com.react-rt.gen.main\scripts\runWorkflow.bat -pmodelDir=%modelDir% -pmodelFile=%modelFile% -poutletPath=%outletPath% -paddObservatoryInstrumentation=false" 

popd