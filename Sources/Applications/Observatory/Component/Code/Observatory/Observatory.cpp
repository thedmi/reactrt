
#include "gen/Observatory.h"

#include "gen/ObservatoryRegistry.h"
#include "gen/EventLogCollector.h"
#include "gen/EventLogUdpBroadcaster.h"
#include "gen/EventLogFileWriter.h"

#include <ReactorRegistry.h>
#include <IReactorFactory.h>
#include <IThread.h>


Observatory::Observatory(boost::shared_ptr<rrt::IThread> observatoryThread)
{
	init(observatoryThread);
}

Observatory::Observatory()
{
	// Create a thread with lowest urgency for use with the Observatory
	init(rrt::ReactorRegistry::Instance().getReactorFactory()->createThread(0, "observatory"));
}

Observatory::~Observatory()
{
	ObservatoryRegistry::Destroy();
}

void Observatory::init( boost::shared_ptr<rrt::IThread> observatoryThread )
{	
	EventLogCollector::Ptr collector(new EventLogCollector());
	collector->start(observatoryThread);

	//EventLogFileWriter::Ptr fileWriter(new EventLogFileWriter("logOutput.txt"));
	//collector->addForwarder(fileWriter);

	EventLogUdpBroadcaster::Ptr udpBroadcaster(new EventLogUdpBroadcaster("127.0.0.1", 1984));
	collector->addForwarder(udpBroadcaster);

	ObservatoryRegistry::Instance().setObserver(collector);
}
