#include "gen/EventLogFileWriter.h"

#include "gen/EventLogItem.h"

#include <iostream>


using namespace std;


EventLogFileWriter::EventLogFileWriter( const std::string& outputFilePath )
{
	stream.open(outputFilePath.c_str(), ios::trunc);
}

EventLogFileWriter::~EventLogFileWriter()
{
	stream.close();
}

void EventLogFileWriter::forward( boost::shared_ptr<const EventLogItem> logItem )
{
	stream << logItem->toString() << std::endl;
}
