
#include "gen/EventLogUdpBroadcaster.h"

#include "gen/EventLogItem.h"

#include "UdpSocket.h"


EventLogUdpBroadcaster::EventLogUdpBroadcaster( const char * targetIpAdress, const unsigned short targetPort )
{
	socket.reset(new UdpSocket(targetIpAdress, targetPort));
}

EventLogUdpBroadcaster::~EventLogUdpBroadcaster()
{
	socket.reset();
}

void EventLogUdpBroadcaster::forward( boost::shared_ptr<const EventLogItem> logItem )
{
	socket->send(logItem->toString() + "\r\n");
}