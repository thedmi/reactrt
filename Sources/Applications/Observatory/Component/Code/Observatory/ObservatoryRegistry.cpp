
#include "gen/ObservatoryRegistry.h"
#include <assert.h>



ObservatoryRegistry::ScopedPtr ObservatoryRegistry::instance;

ObservatoryRegistry::ObservatoryRegistry()
{

}

ObservatoryRegistry::~ObservatoryRegistry()
{

}

void ObservatoryRegistry::Destroy()
{
	instance.reset();
}

ObservatoryRegistry::Ref ObservatoryRegistry::Instance()
{
	if(!instance) {
		instance.reset(new ObservatoryRegistry());
	}
	return *instance;
}

boost::shared_ptr<StateMachineObserver> ObservatoryRegistry::getStateMachineObserver() const
{
	return stateMachineObserver;
}

void ObservatoryRegistry::setObserver( boost::shared_ptr<StateMachineObserver> observer )
{
	stateMachineObserver = observer;
}
