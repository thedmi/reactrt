#include "UdpSocket.h"

UdpSocket::UdpSocket(const char * destAddr, unsigned short destPort)
{
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	const bool sockOptVal = true;
	setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (const char*)&sockOptVal, sizeof(sockOptVal));

	sockaddr_in src;
	src.sin_family = AF_INET;
	src.sin_addr.s_addr = INADDR_ANY;
	src.sin_port = htons(15293);

	dest.sin_family = AF_INET;
	dest.sin_addr.s_addr = inet_addr(destAddr);
	dest.sin_port = htons(destPort);

	bind(sock, (sockaddr *)&src, sizeof(src));

}

UdpSocket::~UdpSocket()
{
	closesocket(sock);
	WSACleanup();
}

void UdpSocket::send( std::string msg )
{
	int retVal = sendto(sock, msg.c_str(), msg.length(), 0, (sockaddr *)&dest, sizeof(dest));
	int error = WSAGetLastError();
}
