#pragma once

#include <WinSock2.h>

#include <string>

class UdpSocket
{
public:

	UdpSocket(const char * destAddr, unsigned short destPort);

	~UdpSocket();

	void send(std::string msg);

private:

	WSAData wsaData;

	SOCKET sock;

	sockaddr_in dest;
};
