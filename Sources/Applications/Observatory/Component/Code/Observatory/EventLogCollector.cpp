
#include "gen/EventLogCollector.h"

#include "gen/EventLogItem.h"
#include "gen/EventLogForwarder.h"

#include <boost/foreach.hpp>

#include <ReactorRegistry.h>
#include <ITimestampProvider.h>

#include <sstream>


EventLogCollector::EventLogCollector()
{

}

EventLogCollector::~EventLogCollector()
{

}

void EventLogCollector::dispatchStarted( const std::string&  stateMachine, rrt::Event::ConstPtr event, const char * signalName )
{
	LogTextualDataEvent::Ptr ev(new LogTextualDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "dispatchStarted";
	ev->textualEventData = signalName;

	post(ev);
}

void EventLogCollector::dispatchCompleted( const std::string& stateMachine )
{
	LogTextualDataEvent::Ptr ev(new LogTextualDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "dispatchCompleted";
	ev->textualEventData = "";

	post(ev);
}

void EventLogCollector::stateChanged( const std::string& stateMachine, const char * newState )
{
	LogTextualDataEvent::Ptr ev(new LogTextualDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "stateChanged";
	ev->textualEventData = newState;

	post(ev);
}

void EventLogCollector::timerArmed( const std::string& stateMachine, const char * timeoutExpression )
{
	LogTextualDataEvent::Ptr ev(new LogTextualDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "timerArmed";
	ev->textualEventData = timeoutExpression;

	post(ev);
}

void EventLogCollector::timerDisarmed( const std::string& stateMachine, const char * timeoutExpression )
{
	LogTextualDataEvent::Ptr ev(new LogTextualDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "timerDisarmed";
	ev->textualEventData = timeoutExpression;

	post(ev);
}

void EventLogCollector::initialized( const std::string& stateMachine, const char * initialState )
{
	LogTextualDataEvent::Ptr ev(new LogTextualDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "initialized";
	ev->textualEventData = initialState;

	post(ev);
}

void EventLogCollector::instantiated( const std::string& stateMachine )
{
	LogTextualDataEvent::Ptr ev(new LogTextualDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "instantiated";
	ev->textualEventData = "";

	post(ev);
}

void EventLogCollector::destroyed( const std::string& stateMachine )
{
	LogTextualDataEvent::Ptr ev(new LogTextualDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "destroyed";
	ev->textualEventData = "";

	post(ev);
}

void EventLogCollector::priorityAnnounce( const std::string& stateMachine, const short priority )
{
	LogNumericDataEvent::Ptr ev(new LogNumericDataEvent());

	ev->stateMachine = stateMachine;
	ev->timestamp = rrt::ReactorRegistry::Instance().getTimestampProvider()->monotonicMicroseconds();
	ev->event = "priorityAnnounce";
	ev->numericEventData = priority;

	post(ev);
}

void EventLogCollector::addForwarder( boost::shared_ptr<EventLogForwarder> forwarder )
{
	forwarders.push_back(forwarder);
}

void EventLogCollector::forward( boost::shared_ptr<const EventLogItem> logItem )
{
	BOOST_FOREACH(boost::weak_ptr<EventLogForwarder> weakForwarder, forwarders) {
		EventLogForwarder::Ptr forwarder = weakForwarder.lock();
		if(forwarder) {
			forwarder->forward(logItem);
		}
	}
}

void EventLogCollector::forwardNumeric( boost::shared_ptr<const LogNumericDataEvent> ev )
{
	std::string eventData;

	std::stringstream s;
	s << ev->numericEventData;
	eventData = s.str();

	EventLogItem::Ptr logItem(new EventLogItem(ev->stateMachine, ev->timestamp, ev->event, eventData));
	forward(logItem);
}

void EventLogCollector::forwardTextual( boost::shared_ptr<const LogTextualDataEvent> ev )
{
	EventLogItem::Ptr logItem(new EventLogItem(ev->stateMachine, ev->timestamp, ev->event, ev->textualEventData));
	forward(logItem);
}
