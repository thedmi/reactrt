
#include "gen/EventLogItem.h"
#include <sstream>

std::string EventLogItem::toString() const
{
	std::stringstream s;
	s << timestamp << " " << stateMachine << ": " << event << " [ " << eventData << " ]";
	return s.str();
}
