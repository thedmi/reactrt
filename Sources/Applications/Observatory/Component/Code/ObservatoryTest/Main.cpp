
#include <tchar.h>

#include <RrtReactor.h>

#include "gen/Observatory.h"
#include "gen/StateMachine1.h"

#include <boost/smart_ptr.hpp>


int _tmain(int argc, _TCHAR* argv[])
{
	using namespace rrt;

	ReactorConfigurator configurator;

	IThread::Ptr observatoryThread = ReactorRegistry::Instance().getReactorFactory()->createThread(0, "observatory");

	Observatory observatory(observatoryThread);

	boost::shared_ptr<StateMachine1> testStateMachine(new StateMachine1());
	testStateMachine->start();
	

	Reactor::run();


	return 0;
	
}

