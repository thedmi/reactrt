﻿using ReactRT.Observatory.Monitor.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ReactRT.Observatory.MonitorTests.Domain {


    /// <summary>
    ///This is a test class for ReactionBuilderTest and is intended
    ///to contain all ReactionBuilderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReactionBuilderTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void ReactionBuilderOutputTest() {

            int seqNumber = 0;

            Reaction builderOutput = null;

            ReactionBuilder target = new ReactionBuilder();

            target.ReactionCompleted += (object sender, ReactionBuilder.ReactionCompletedEventArgs e) => {
                builderOutput = e.Reaction;
            };

            LogItem item0 = new LogItem(new DateTime(129454547282987710), "StateMachine1", 0, "", LogEventType.Initialized, "Sm::Inited", seqNumber++);
            target.AddLogItem(item0);

            Assert.IsNotNull(builderOutput);
            Assert.IsTrue(builderOutput.LogItems.Count == 1);
            Assert.IsTrue(builderOutput.LogItems[0].EventType == LogEventType.Initialized);

            builderOutput = null;

            LogItem item1 = new LogItem(new DateTime(129454547282987710), "StateMachine1", 0, "", LogEventType.DispatchStarted, "sigTimeout1", seqNumber++);
            target.AddLogItem(item1);

            LogItem item2 = new LogItem(new DateTime(129454547282987720), "StateMachine1", 0, "", LogEventType.TimerArmed, "200", seqNumber++);
            target.AddLogItem(item2);

            LogItem item3 = new LogItem(new DateTime(129454547282987730), "StateMachine1", 0, "", LogEventType.StateChanged, "Sm::Active::State42", seqNumber++);
            target.AddLogItem(item3);

            Assert.IsNull(builderOutput);

            LogItem item4 = new LogItem(new DateTime(129454547282987740), "StateMachine1", 0, "", LogEventType.DispatchCompleted, "", seqNumber++);
            target.AddLogItem(item4);

            Assert.IsNotNull(builderOutput);
            Assert.IsTrue(builderOutput.LogItems.Count == 4);

            Assert.IsTrue(builderOutput.LogItems[0].EventType == LogEventType.DispatchStarted);
            Assert.IsTrue(builderOutput.LogItems[1].EventType == LogEventType.TimerArmed);
            Assert.IsTrue(builderOutput.LogItems[2].EventType == LogEventType.StateChanged);
            Assert.IsTrue(builderOutput.LogItems[3].EventType == LogEventType.DispatchCompleted);
        }
    }
}
