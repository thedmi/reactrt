﻿using ReactRT.Observatory.Monitor.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ReactRT.Observatory.MonitorTests.Domain {

    /// <summary>
    ///This is a test class for LogItemParserTest and is intended
    ///to contain all LogItemParserTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LogItemParserTest {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void ParseRawEventLogTest1() {
            LogItemParser target = new LogItemParser();

            string rawEventLog = "12945454728298776 StateMachine1 ( 38c140 '' ): dispatchStarted [ sigTimeout1 ]";

            LogItem expected = new LogItem(new DateTime(129454547282987760), "StateMachine1", 0x38c140, "", LogEventType.DispatchStarted, "sigTimeout1", 0);

            LogItem actual = target.ParseRawEventLog(rawEventLog);

            Assert.AreEqual(expected.Timestamp, actual.Timestamp);
            Assert.AreEqual(expected.StateMachine, actual.StateMachine);
            Assert.AreEqual(expected.InstanceId, actual.InstanceId);
            Assert.AreEqual(expected.InstanceName, actual.InstanceName);
            Assert.AreEqual(expected.EventType, actual.EventType);
            Assert.AreEqual(expected.EventDescription, actual.EventDescription);
            Assert.AreEqual(expected.SequenceNumber, actual.SequenceNumber);
        }


        [TestMethod()]
        public void ParseRawEventLogTest2() {
            LogItemParser target = new LogItemParser();

            string rawEventLog = "12945454728298776 StateMachine2 ( 38c140 'my object' ): priorityAnnounce [ 61 ]";

            LogItem expected = new LogItem(new DateTime(129454547282987760), "StateMachine2", 0x38c140, "my object", LogEventType.PriorityAnnounce, "61", 0);

            LogItem actual = target.ParseRawEventLog(rawEventLog);

            Assert.AreEqual(expected.Timestamp, actual.Timestamp);
            Assert.AreEqual(expected.StateMachine, actual.StateMachine);
            Assert.AreEqual(expected.InstanceId, actual.InstanceId);
            Assert.AreEqual(expected.InstanceName, actual.InstanceName);
            Assert.AreEqual(expected.EventType, actual.EventType);
            Assert.AreEqual(expected.EventDescription, actual.EventDescription);
            Assert.AreEqual(expected.SequenceNumber, actual.SequenceNumber);
        }
    }
}
