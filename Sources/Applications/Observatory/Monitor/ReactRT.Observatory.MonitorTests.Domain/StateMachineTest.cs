﻿using ReactRT.Observatory.Monitor.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace ReactRT.Observatory.MonitorTests.Domain {

    /// <summary>
    ///This is a test class for StateMachineTest and is intended
    ///to contain all StateMachineTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StateMachineTest {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }


        #region Additional test attributes

        StateMachine stateMachine;

        [TestInitialize()]
        public void MyTestInitialize() {

            stateMachine = new StateMachine("StateMachine1", "Package::Subpackage", new SortedSet<IState>());
            stateMachine.ProcessLogItem(new LogItem(new DateTime(100), "StateMachine1", 0, "", LogEventType.DispatchCompleted, "", 1));
        }

        #endregion


        [TestMethod()]
        public void ThreadPrioTest() {

            Assert.AreEqual(0, stateMachine.Instances[0].ThreadPrio);

            stateMachine.ProcessLogItem(new LogItem(new DateTime(100), "StateMachine1", 0, "", LogEventType.PriorityAnnounce, "42", 2));

            Assert.AreEqual(42, stateMachine.Instances[0].ThreadPrio);
        }

        [TestMethod()]
        public void CurrentStateNameTest() {

            Assert.AreEqual("", stateMachine.Instances[0].CurrentStateName);

            stateMachine.ProcessLogItem(new LogItem(new DateTime(100), "StateMachine1", 0, "", LogEventType.DispatchStarted, "sigX", 2));
            stateMachine.ProcessLogItem(new LogItem(new DateTime(100), "StateMachine1", 0, "", LogEventType.StateChanged, "Sm::State1::State2::State3", 3));
            stateMachine.ProcessLogItem(new LogItem(new DateTime(100), "StateMachine1", 0, "", LogEventType.DispatchCompleted, "", 4));

            Assert.AreEqual("State3", stateMachine.Instances[0].CurrentStateName);
        }
    }
}
