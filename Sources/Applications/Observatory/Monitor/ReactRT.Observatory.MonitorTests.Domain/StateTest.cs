﻿using ReactRT.Observatory.Monitor.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace ReactRT.Observatory.MonitorTests.Domain {

    /// <summary>
    ///This is a test class for StateTest and is intended
    ///to contain all StateTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StateTest {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }


        #region Additional test attributes

        private State s1;
        private State s11;
        private State s12;
        private State s123;

        [TestInitialize()]
        public void MyTestInitialize() {
            s1 = new State(null, "S1");
            s11 = new State(s1, "S1");
            s12 = new State(s1, "S2");
            s123 = new State(s12, "S3");
        }

        #endregion


        [TestMethod()]
        public void FullyQualifiedNameTest() {
         
            Assert.AreEqual("Sm::S1", s1.FullyQualifiedName);
            Assert.AreEqual("Sm::S1::S1", s11.FullyQualifiedName);
            Assert.AreEqual("Sm::S1::S2", s12.FullyQualifiedName);
            Assert.AreEqual("Sm::S1::S2::S3", s123.FullyQualifiedName);
        }

        [TestMethod()]
        public void SubstatesTest() {

            Assert.AreEqual(2, s1.Substates.Count);
            Assert.AreSame(s11, new List<IState>(s1.Substates)[0]);
            Assert.AreSame(s12, new List<IState>(s1.Substates)[1]);

            Assert.AreEqual(1, s12.Substates.Count);
            Assert.AreSame(s123, new List<IState>(s12.Substates)[0]);

            Assert.AreEqual(0, s11.Substates.Count);

            Assert.AreEqual(0, s123.Substates.Count);
        }
    }
}
