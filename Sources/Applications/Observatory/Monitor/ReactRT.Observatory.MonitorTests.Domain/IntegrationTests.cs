﻿using ReactRT.Observatory.Monitor.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using System.Collections.Generic;

namespace ReactRT.Observatory.MonitorTests.Domain
{
    
    
    /// <summary>
    ///This is a test class for SessionTest and is intended
    ///to contain all SessionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IntegrationTests : IRawEventLogProvider
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        #region IRawEventLogProvider members

        public event EventHandler<RawEventLogEventArgs> RawEventLogReady;

        public event EventHandler<BulkDataProgressEventArgs> BulkDataProgress = delegate { };

        public event EventHandler BulkDataCompleted = delegate { };

        public bool IsBulkDataProvider
        {
            get { throw new NotImplementedException(); }
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Stop() {
            throw new NotImplementedException();
        }

        #endregion


        /// <summary>
        ///A test for Session Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ReactRT.Observatory.Monitor.Domain.dll")]
        public void IntegrationTest1()
        {
            IRawEventLogProvider rawEventLogProvider = this;

            Session_Accessor target = new Session_Accessor(rawEventLogProvider, "");

            StateMachine sm1 = new StateMachine("StateMachine1", "Domain::Lib1", new SortedSet<IState>());
            StateMachine sm2 = new StateMachine("StateMachine2", "Domain::Lib2", new SortedSet<IState>());

            target.AddStateMachine(sm1);
            target.AddStateMachine(sm2);

            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298007 StateMachine1 (0 ''): priorityAnnounce [ 42 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298006 StateMachine1 (0 ''): initialized [ Sm::Initial ]"));

            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298017 StateMachine2 (0 ''): priorityAnnounce [ 43 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298016 StateMachine2 (0 ''): initialized [ Sm::Initial ]"));

            Assert.IsTrue(sm1.Instances[0].ThreadPrio == 42);
            Assert.IsTrue(sm2.Instances[0].ThreadPrio == 43);

            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298776 StateMachine1 (0 ''): dispatchStarted [ sigTimeout1 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298777 StateMachine1 (0 ''): timerDisarmed [ 200 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298778 StateMachine1 (0 ''): stateChanged [ Sm::State42 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298779 StateMachine1 (0 ''): dispatchCompleted [  ]"));

            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298876 StateMachine2 (0 ''): dispatchStarted [ sigTimeout1 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298878 StateMachine2 (0 ''): stateChanged [ Sm::State43 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298879 StateMachine2 (0 ''): dispatchCompleted [  ]"));

            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298976 StateMachine1 (0 ''): dispatchStarted [ sigX ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728298979 StateMachine1 (0 ''): dispatchCompleted [  ]"));

            RawEventLogReady(this, new RawEventLogEventArgs("12945454728299076 StateMachine2 (0 ''): dispatchStarted [ sigY ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728299078 StateMachine2 (0 ''): timerArmed [ 500 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728299078 StateMachine2 (0 ''): timerArmed [ 1500 ]"));
            RawEventLogReady(this, new RawEventLogEventArgs("12945454728299078 StateMachine2 (0 ''): timerArmed [ 2500 ]"));

            Assert.IsTrue(sm1.Instances[0].Reactions.Count == 3);
            Assert.IsTrue(sm2.Instances[0].Reactions.Count == 2);

            RawEventLogReady(this, new RawEventLogEventArgs("12945454728299079 StateMachine2 (0 ''): dispatchCompleted [  ]"));

            Assert.IsTrue(sm1.Instances[0].Reactions.Count == 3);
            Assert.IsTrue(sm2.Instances[0].Reactions.Count == 3);

            Assert.IsTrue(sm1.Instances[0].Reactions[0].LogItems.Count == 2);
            Assert.IsTrue(sm1.Instances[0].Reactions[1].LogItems.Count == 4);
            Assert.IsTrue(sm1.Instances[0].Reactions[2].LogItems.Count == 2);

            Assert.IsTrue(sm2.Instances[0].Reactions[0].LogItems.Count == 2);
            Assert.IsTrue(sm2.Instances[0].Reactions[1].LogItems.Count == 3);
            Assert.IsTrue(sm2.Instances[0].Reactions[2].LogItems.Count == 5);
        }


    }
}
