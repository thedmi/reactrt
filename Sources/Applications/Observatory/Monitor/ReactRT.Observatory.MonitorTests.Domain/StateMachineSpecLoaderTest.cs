﻿using ReactRT.Observatory.Monitor.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml;
using System.Collections.Generic;
using System.Xml.Linq;

namespace ReactRT.Observatory.MonitorTests.Domain
{
    
    
    /// <summary>
    ///This is a test class for StateMachineSpecLoaderTest and is intended
    ///to contain all StateMachineSpecLoaderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StateMachineSpecLoaderTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        [DeploymentItem("ReactRT.Observatory.Monitor.Domain.dll")]
        public void ParseStateMachineSpecTest()
        {
            StateMachineSpecLoader_Accessor target = new StateMachineSpecLoader_Accessor();
            
            XmlDocument spec = new XmlDocument();
            spec.LoadXml(@"
                <reactRtStateMachines> 
                    <stateMachine name=""StateMachine1"" namespace=""Test::Space"">
                        <state name=""StateA"" />
                        <state name=""StateB"">
                            <state name=""StateB1"" />
                            <state name=""StateB2"" />
                        </state>
                    </stateMachine>
	                <stateMachine name=""StateMachine2"" namespace=""Test::Space"" />
                </reactRtStateMachines>");
            
            List<StateMachine> expected = new List<StateMachine>();

            State stateA = new State(null, "StateA");
            State stateB = new State(null, "StateB");
            State stateB1 = new State(stateB, "StateB1");
            State stateB2 = new State(stateB, "StateB2");

            HashSet<IState> statesOfSm1 = new HashSet<IState>();
            statesOfSm1.Add(stateA);
            statesOfSm1.Add(stateB);

            StateMachine sm1 = new StateMachine("StateMachine1", "Test::Space", statesOfSm1);
            StateMachine sm2 = new StateMachine("StateMachine2", "Test::Space", new SortedSet<IState>());

            expected.Add(sm1);
            expected.Add(sm2);

            List<StateMachine> actual = target.ParseStateMachineSpec(spec);

            Assert.AreEqual(expected[0].Name, actual[0].Name);
            Assert.AreEqual(expected[1].Name, actual[1].Name);

            Assert.IsTrue(AreSetsEqual(expected[0].StateHierarchy, actual[0].StateHierarchy));
        }

        private bool AreSetsEqual(ISet<IState> first, ISet<IState> second) {
            IEnumerator<IState> a = first.GetEnumerator();
            IEnumerator<IState> b = second.GetEnumerator();

            bool success = true;

            while (a.MoveNext()) {
                success &= b.MoveNext();

                success &= a.Current.Name == b.Current.Name;
                success &= AreSetsEqual(a.Current.Substates, b.Current.Substates);
            }

            return success;
        }

    }
}
