﻿using ReactRT.Observatory.Monitor.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ReactRT.Observatory.MonitorTests.Domain {

    /// <summary>
    ///This is a test class for ReactionTest and is intended
    ///to contain all ReactionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReactionTest {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void ReactionPropertiesTest() {

            int reactionSeqNumber = 0;
            int logItemSeqNumber = 0;

            Reaction firstReaction = new Reaction(reactionSeqNumber++);

            firstReaction.AddLogItem(new LogItem(new DateTime(100), "StateMachine1", 0, "", LogEventType.Initialized, "Sm::State1", logItemSeqNumber++));

            Assert.AreEqual("", firstReaction.StartState);
            Assert.AreEqual("Sm::State1", firstReaction.EndState);
            Assert.AreEqual("Sm::State1", firstReaction.TransitionTarget);
            Assert.IsTrue(firstReaction.IsInitialization);
            Assert.IsFalse(firstReaction.PerformsTransition);


            Reaction secondReaction = new Reaction(reactionSeqNumber++);

            secondReaction.AddLogItem(new LogItem(new DateTime(400), "StateMachine1", 0, "", LogEventType.DispatchStarted, "sigX", logItemSeqNumber++));
            secondReaction.AddLogItem(new LogItem(new DateTime(600), "StateMachine1", 0, "", LogEventType.DispatchCompleted, "", logItemSeqNumber++));

            secondReaction.SetPredecessorAndSetCompleted(firstReaction);

            AssertThrows<InvalidOperationException>(() => 
                secondReaction.AddLogItem(new LogItem(new DateTime(), "", 0, "", LogEventType.DispatchStarted, "", logItemSeqNumber++))
            );

            Assert.AreEqual(new DateTime(400), secondReaction.Start);
            Assert.AreEqual(new DateTime(600), secondReaction.End);
            Assert.AreEqual(TimeSpan.FromTicks(200), secondReaction.Duration);

            Assert.AreEqual("Sm::State1", secondReaction.StartState);
            Assert.AreEqual("Sm::State1", secondReaction.EndState);
            Assert.AreEqual("", secondReaction.TransitionTarget);
            Assert.IsFalse(secondReaction.IsInitialization);
            Assert.IsFalse(secondReaction.PerformsTransition);


            Reaction thirdReaction = new Reaction(reactionSeqNumber++);

            thirdReaction.AddLogItem(new LogItem(new DateTime(650), "StateMachine1", 0, "", LogEventType.DispatchStarted, "sigX", logItemSeqNumber++));
            thirdReaction.AddLogItem(new LogItem(new DateTime(650), "StateMachine1", 0, "", LogEventType.StateChanged, "Sm::State2", logItemSeqNumber++));
            thirdReaction.AddLogItem(new LogItem(new DateTime(650), "StateMachine1", 0, "", LogEventType.DispatchCompleted, "", logItemSeqNumber++));

            thirdReaction.SetPredecessorAndSetCompleted(secondReaction);

            Assert.AreEqual("Sm::State1", thirdReaction.StartState);
            Assert.AreEqual("Sm::State2", thirdReaction.EndState);
            Assert.AreEqual("Sm::State2", thirdReaction.TransitionTarget);
            Assert.IsFalse(thirdReaction.IsInitialization);
            Assert.IsTrue(thirdReaction.PerformsTransition);


            Reaction fourthReaction = new Reaction(reactionSeqNumber++);

            fourthReaction.AddLogItem(new LogItem(new DateTime(700), "StateMachine1", 0, "", LogEventType.DispatchStarted, "sigX", logItemSeqNumber++));
            fourthReaction.AddLogItem(new LogItem(new DateTime(700), "StateMachine1", 0, "", LogEventType.DispatchCompleted, "", logItemSeqNumber++));

            fourthReaction.SetPredecessorAndSetCompleted(thirdReaction);

            Assert.AreEqual("Sm::State2", fourthReaction.StartState);
            Assert.AreEqual("Sm::State2", fourthReaction.EndState);
            Assert.AreEqual("", fourthReaction.TransitionTarget);
            Assert.IsFalse(secondReaction.IsInitialization);
            Assert.IsFalse(secondReaction.PerformsTransition);

        }

        private void AssertThrows<T>(Action action) where T : System.Exception {
            try {
                action();
                throw new AssertFailedException("The action did not throw the specified exception");
            } catch (T) {
                // Intentionally ignore the exception, because it was expected to be thrown
            }
        }
    }
}
