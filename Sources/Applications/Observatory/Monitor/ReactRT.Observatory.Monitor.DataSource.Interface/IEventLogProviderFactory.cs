﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.DataSource.Interface {

    public interface IEventLogProviderFactory {

        IRawEventLogProvider CreateFileEventLogProvider(string eventLogPath);

        IRawEventLogProvider CreateUdpEventLogProvider(int port);
    }
}
