﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.DataSource.Interface {

    public class BulkDataProgressEventArgs : EventArgs {

        public double PercentageCompleted { get; private set; }

        public BulkDataProgressEventArgs(double percentage) {
            PercentageCompleted = percentage;
        }
    }

    public class RawEventLogEventArgs : EventArgs {

        public string Message { get; private set; }

        public RawEventLogEventArgs(string message) {
            Message = message;
        }
    }

    public interface IRawEventLogProvider {

        event EventHandler<RawEventLogEventArgs> RawEventLogReady;

        event EventHandler<BulkDataProgressEventArgs> BulkDataProgress;

        event EventHandler BulkDataCompleted;

        void Start();

        void Stop();

        bool IsBulkDataProvider { get; }
    }
}
