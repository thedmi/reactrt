﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using System.Net.Sockets;
using System.Net;

namespace ReactRT.Observatory.Monitor.DataSource {

    public class UdpEventLogReceiver : IRawEventLogProvider {

        private readonly UdpClient _udpClient;

        private IPEndPoint _ipEndPoint;

        private volatile bool _isActive = false;

        public UdpEventLogReceiver(int port) {
            _udpClient = new UdpClient(port);
            _ipEndPoint = new IPEndPoint(IPAddress.Any, 0);
        }

        public event EventHandler<RawEventLogEventArgs> RawEventLogReady = delegate { };

        // These events are unused, because this is not a bulk data provider
        public event EventHandler BulkDataCompleted = delegate { };
        public event EventHandler<BulkDataProgressEventArgs> BulkDataProgress = delegate { };

        public bool IsBulkDataProvider { get { return false; } }

        public void Start() {
            _isActive = true;
            ReceiveNext();
        }

        public void Stop() {
            _isActive = false;
            _udpClient.Close();
        }

        private void DatagramReceived(IAsyncResult ar) {

            try {
                Byte[] receivedBytes = _udpClient.EndReceive(ar, ref _ipEndPoint);
                string receivedString = Encoding.ASCII.GetString(receivedBytes).Trim();

                if (receivedString.Length > 0) {
                    RawEventLogReady(this, new RawEventLogEventArgs(receivedString));
                }

                if (_isActive) {
                    ReceiveNext();
                }

            } catch (Exception) {
                // ObjectDisposedException or SocketException may occur.

                // This exception occurs when calling EndReceive on a closed socket. However, EndReceive must
                // always be closed to complete the asynchronous operation. 
                // See http://msdn.microsoft.com/en-us/library/system.net.sockets.udpclient.beginreceive.aspx
                // and http://social.msdn.microsoft.com/forums/en-US/netfxnetcom/thread/c4d0d9c1-64cc-4881-900c-10b14d990c38/

                // Intentionally do nothing
            }
        }

        private void ReceiveNext() {
            _udpClient.BeginReceive(new AsyncCallback(DatagramReceived), null);
        }
    }
}
