﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.DataSource.Interface;

namespace ReactRT.Observatory.Monitor.DataSource {

    public class DefaultEventLogProviderFactory : IEventLogProviderFactory {

        public IRawEventLogProvider CreateFileEventLogProvider(string eventLogPath) {
            return new FileEventLogReader(eventLogPath);
        }

        public IRawEventLogProvider CreateUdpEventLogProvider(int port) {
            return new UdpEventLogReceiver(port);
        }
    }
}
