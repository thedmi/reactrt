﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using System.IO;
using System.Threading.Tasks;

namespace ReactRT.Observatory.Monitor.DataSource
{
    public class FileEventLogReader : IRawEventLogProvider
    {
        private readonly string _path;


        public event EventHandler<RawEventLogEventArgs> RawEventLogReady = delegate { };

        public event EventHandler BulkDataCompleted = delegate { };

        public event EventHandler<BulkDataProgressEventArgs> BulkDataProgress = delegate { };

        public bool IsBulkDataProvider { get { return true; } }


        public FileEventLogReader(string eventLogPath)
        {
            _path = eventLogPath;

            if (!File.Exists(_path))
            {
                throw new ArgumentException("Specified event log file does not exist");
            }
        }

        public void Start()
        {
            Task.Factory.StartNew(ReadWholeFile);
        }


        public void Stop() {
            throw new InvalidOperationException("Stopping the file reader is not supported");
        }

        private void ReadWholeFile()
        {

            using (StreamReader sr = new StreamReader(_path))
            {
                FileInfo info = new FileInfo(_path);

                long size = info.Length;
                int position = 0;

                int lineCount = 0;

                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    RawEventLogReady(this, new RawEventLogEventArgs(line));

                    position += line.Length + 1;
                    lineCount++;

                    if (lineCount % 10 == 0)
                    {
                        BulkDataProgress(this, new BulkDataProgressEventArgs((double)position / size));
                    }
                }
            }

            BulkDataCompleted(this, new EventArgs());
        }
    }
}
