﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace ReactRT.Observatory.Monitor.Launcher {

    class Options {
        
        [Option("s", "spec", HelpText = "Read the static state machine specification from the given file.")]
        public string StateMachineSpecPath = "";

        [Option("r", "replay", HelpText = "Start a replay session with the given file as source.")]
        public string ReplayFilePath = "";

        

        [HelpOption(HelpText = "Display this help text.")]
        public string GetUsage() {

            HelpText help = new HelpText("ReactRT Observatory");
            help.Copyright = new CopyrightInfo("Daniel Michel", 2011);
            help.AddOptions(this);
            return help;

        }

    }
}
