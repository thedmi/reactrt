﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using ReactRT.Observatory.Monitor.Domain;
using ReactRT.Observatory.Monitor.Presentation;
using ReactRT.Observatory.Monitor.DataSource;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using CommandLine;
using System.Runtime.InteropServices;


namespace ReactRT.Observatory.Monitor.Launcher {

    public partial class App : Application {

        private MainViewModel mainViewModel;

        protected override void OnStartup(StartupEventArgs e) {
            base.OnStartup(e);

            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(@"../../Log4Net.xml"));

            
            Options options = new Options();
            ICommandLineParser parser = new CommandLineParser();

            if (parser.ParseArguments(e.Args, options)) {
                if (options.ReplayFilePath != "" && options.StateMachineSpecPath != "") {
                    mainViewModel = new MainViewModel(new DefaultEventLogProviderFactory(), options.StateMachineSpecPath, options.ReplayFilePath);
                } else {
                    mainViewModel = new MainViewModel(new DefaultEventLogProviderFactory(), options.StateMachineSpecPath);
                }

                MainWindow w = new MainWindow();
                w.DataContext = mainViewModel;

                w.Show();

            } else {
                printToConsole(options.GetUsage());
            }
        }

        private void printToConsole(string message) {
            AttachConsole(-1);
            Console.WriteLine(message);
        }


        [DllImport("Kernel32.dll")]
        public static extern bool AttachConsole(int processId);
    }
}
