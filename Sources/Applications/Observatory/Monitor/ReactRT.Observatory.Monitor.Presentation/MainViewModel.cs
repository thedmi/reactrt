﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.TimelineControl;
using ReactRT.Observatory.Monitor.Domain;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using System.Windows;

namespace ReactRT.Observatory.Monitor.Presentation {

    public class MainViewModel : ViewModelBase {

        private readonly IEventLogProviderFactory _eventLogProviderFactory;

        private readonly bool _performAutoStart = false;

        private SessionSelectorViewModel _sessionSelector;
        
        public MainViewModel(IEventLogProviderFactory providerFactory, string defaultStateMachineSpecPath = "") {
            _eventLogProviderFactory = providerFactory;

            Timeline = new TimelineViewModel();
            _sessionSelector = new SessionSelectorViewModel();
            _sessionSelector.StateMachineSpecPath = defaultStateMachineSpecPath;
        }

        public MainViewModel(IEventLogProviderFactory providerFactory, string stateMachineSpecPath, string replayFilePath) 
                : this(providerFactory, stateMachineSpecPath) {

            _sessionSelector.IsReplayMode = true;
            _sessionSelector.StateMachineSpecPath = stateMachineSpecPath;
            _sessionSelector.ReplayFilePath = replayFilePath;

            _performAutoStart = true;

            
        }

        public TimelineViewModel Timeline { get; private set; }

        public RelayCommand NewSessionCommand {
            get {
                return new RelayCommand(() => ShowSessionSelector());
            }
        }

        public void OnLoaded() {
            if (_performAutoStart) {
                PrepareStartSession();
            } else {
                ShowSessionSelector();
            }
        }

        private void StartSession(IRawEventLogProvider rawEventLogProvider, string stateMachineSpecPath) {
                Session s = new Session(rawEventLogProvider, stateMachineSpecPath);
                Timeline.ActiveSession = s;

                rawEventLogProvider.Start();
        }

        private bool CanCreateNewSession() {
            if (Timeline.ActiveSession != null) {
                string message = "There is already an active session. Creating a new session will close the current one. Proceed?";
                MessageBoxResult result = MessageBox.Show(message, "Active Session", MessageBoxButton.YesNo, MessageBoxImage.Warning);

                return result == MessageBoxResult.Yes;
            } 
            return true;
        }

        private void PrepareStartSession() {
            if (CanCreateNewSession()) {

                Timeline.StopLiveTracing();

                if (_sessionSelector.IsReplayMode) {
                    StartSession(_eventLogProviderFactory.CreateFileEventLogProvider(_sessionSelector.ReplayFilePath), _sessionSelector.StateMachineSpecPath);
                } else {
                    StartSession(_eventLogProviderFactory.CreateUdpEventLogProvider(_sessionSelector.UdpPort), _sessionSelector.StateMachineSpecPath);
                }
            }
        }

        private void ShowSessionSelector() {

            SessionSelector s = new SessionSelector();
            s.Owner = Application.Current.MainWindow;
            s.DataContext = _sessionSelector;
            bool? startSession = s.ShowDialog();

            if (startSession == true) {
                PrepareStartSession();
            }
        }

    }
}
