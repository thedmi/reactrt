﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using System.Windows.Controls;

namespace ReactRT.Observatory.Monitor.Presentation {

    public class SessionSelectorViewModel : ViewModelBase {

        public SessionSelectorViewModel() {
            UdpPort = 1984;
            _relayCommand = new RelayCommand(() => { }, IsValidConfiguration);
        }

        public string StateMachineSpecPath { get; set; }

        public string ReplayFilePath { get; set; }

        public int UdpPort { get; set; }

        private bool _isReplayMode = true;
        private RelayCommand _relayCommand;

        public bool IsReplayMode {
            get { return _isReplayMode; }
            set {
                _isReplayMode = value;
                RaisePropertyChanged("IsReplayMode");
                StartSessionCommand.RaiseCanExecuteChanged();
            }
        }


        public RelayCommand BrowseStateMachineSpec {
            get {
                return new RelayCommand(() => {
                    OpenFileDialog d = new OpenFileDialog();
                    d.Filter = "XML Files |*.xml";
                    bool? result = d.ShowDialog();

                    if (result == true) {
                        StateMachineSpecPath = d.FileName;
                        RaisePropertyChanged("StateMachineSpecPath");
                        StartSessionCommand.RaiseCanExecuteChanged();
                    }
                });
            }
        }

        public RelayCommand BrowseReplayFile {
            get {
                return new RelayCommand(() => {
                    OpenFileDialog d = new OpenFileDialog();
                    bool? result = d.ShowDialog();

                    if (result == true) {
                        ReplayFilePath = d.FileName;
                        RaisePropertyChanged("ReplayFilePath");
                        StartSessionCommand.RaiseCanExecuteChanged();
                    }
                });
            }
        }

        public RelayCommand StartSessionCommand {
            get
            {
                return _relayCommand;
            }
        }

        private bool IsValidConfiguration() {
            if (IsReplayMode) {
                return File.Exists(StateMachineSpecPath) && File.Exists(ReplayFilePath);
            } else {
                return File.Exists(StateMachineSpecPath) && (UdpPort > 0 && UdpPort < 0xFFFF);
            }
        }
    }
}
