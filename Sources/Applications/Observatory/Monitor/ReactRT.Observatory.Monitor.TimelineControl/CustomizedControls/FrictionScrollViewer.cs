﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Input;

namespace ReactRT.Observatory.Monitor.TimelineControl.CustomizedControls
{
    /*
     * FrictionScrollViewer inspired by Sacha Barbers blog post
     * "Creating A Scrollable Control Surface In WPF", located at
     * http://sachabarber.net/?p=225 
     */
    class FrictionScrollViewer : ScrollViewer
    {
        #region Data

        private Point _scrollTarget;
        private Point _scrollStartPoint;
        private Point _scrollStartOffset;
        private Point _previousPoint;
        private Vector _velocity;
        private double _friction;
        private DispatcherTimer _animationTimer = new DispatcherTimer();

        #endregion

        #region Initializer

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            _friction = 0.95;

            _animationTimer.Interval = new TimeSpan(0, 0, 0, 0, 20);
            _animationTimer.Tick += new EventHandler(HandleWorldTimerTick);
            _animationTimer.Start();
        }

        #endregion


        #region Horizontal scroll offset dependency property

        public static readonly DependencyProperty DynamicHorizontalOffsetProperty =
            DependencyProperty.Register("DynamicHorizontalOffset", typeof(double), typeof(FrictionScrollViewer),
                new FrameworkPropertyMetadata((double)0.0, new PropertyChangedCallback(OnDynamicHorizontalOffsetChanged)));

        public double DynamicHorizontalOffset
        {
            get { return (double)GetValue(DynamicHorizontalOffsetProperty); }
            set { SetValue(DynamicHorizontalOffsetProperty, value); }
        }

        private static void OnDynamicHorizontalOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var viewer = (FrictionScrollViewer)d;

            double newOffset = (double)e.NewValue - viewer.ViewportWidth / 2;
            viewer.ScrollToHorizontalOffset(newOffset);
            viewer._scrollTarget.X = newOffset;
        }

        protected override void OnScrollChanged(ScrollChangedEventArgs e)
        {
            base.OnScrollChanged(e);

            DynamicHorizontalOffset = e.HorizontalOffset + ViewportWidth / 2;
        }

        #endregion


        #region Friction handling

        private void HandleWorldTimerTick(object sender, EventArgs e)
        {
            if (IsMouseCaptured)
            {
                Point currentPoint = Mouse.GetPosition(this);
                _velocity = _previousPoint - currentPoint;
                _previousPoint = currentPoint;
            }
            else
            {
                if (_velocity.Length > 1)
                {
                    ScrollToHorizontalOffset(_scrollTarget.X);
                    ScrollToVerticalOffset(_scrollTarget.Y);
                    _scrollTarget.X += _velocity.X;
                    _scrollTarget.Y += _velocity.Y;
                    _velocity *= _friction;
                }
            }
        }

        #endregion


        #region Mouse events

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (IsMouseOver)
            {
                // Save starting point, used later when determining how much to scroll.
                _scrollStartPoint = e.GetPosition(this);
                _scrollStartOffset.X = HorizontalOffset;
                _scrollStartOffset.Y = VerticalOffset;

                // Update the cursor if can scroll or not.
                this.Cursor = (ExtentWidth > ViewportWidth) ||
                    (ExtentHeight > ViewportHeight) ?
                    Cursors.SizeWE : Cursors.Arrow;

                this.CaptureMouse();
            }

            base.OnMouseDown(e);
        }


        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (this.IsMouseCaptured)
            {
                Point currentPoint = e.GetPosition(this);

                // Determine the new amount to scroll.
                Point delta = new Point(_scrollStartPoint.X - currentPoint.X, _scrollStartPoint.Y - currentPoint.Y);

                _scrollTarget.X = _scrollStartOffset.X + delta.X;
                _scrollTarget.Y = _scrollStartOffset.Y + delta.Y;

                // Scroll to the new position.
                ScrollToHorizontalOffset(_scrollTarget.X);
                ScrollToVerticalOffset(_scrollTarget.Y);
            }

            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (this.IsMouseCaptured)
            {
                this.Cursor = Cursors.Arrow;
                this.ReleaseMouseCapture();
            }

            base.OnMouseUp(e);
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e) {
            // Intentionally left empty

            // Overriding this method WITHOUT calling the base implementation causes
            // mouse wheel events to not be handled at all. This has the desired 
            // effect that the event will be passed up the tree, so that a parent
            // scroll viewer can handle the event.
        }

        #endregion
    }
}
