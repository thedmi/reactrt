﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace ReactRT.Observatory.Monitor.TimelineControl
{
    /// <summary>
    /// This is a helper class that provides an attached property <code>UpdateSourceOnEnter</code> for
    /// TextBox objects. When set to true, the respective TextBox updates its source as soon as the user
    /// presses enter.
    /// </summary>
    class TextBoxProperties : DependencyObject
    {
        public static readonly DependencyProperty UpdateSourceOnEnterProperty = DependencyProperty.RegisterAttached(
            "UpdateSourceOnEnter",
            typeof(Boolean),
            typeof(TextBoxProperties),
            new FrameworkPropertyMetadata(OnUpdateSourceOnEnterChanged)
            );

        public static void SetUpdateSourceOnEnter(TextBox element, Boolean value)
        {
            element.SetValue(UpdateSourceOnEnterProperty, value);
        }

        public static Boolean GetUpdateSourceOnEnter(TextBox element)
        {
            return (Boolean)element.GetValue(UpdateSourceOnEnterProperty);
        }

        private static void OnUpdateSourceOnEnterChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var element = (TextBox)obj;

            if ((bool)args.NewValue == true)
            {
                element.KeyUp += (object sender, KeyEventArgs e) =>
                {
                    if (e.Key == Key.Enter)
                    {
                        element.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                        element.SelectAll();
                    }
                };

            }
        }

    }
}
