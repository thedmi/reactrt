﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;

namespace ReactRT.Observatory.Monitor.TimelineControl {

    public class AxisMarkerViewModel : ViewModelBase {

        private TimeSpan _markerOffset;

        public TimeSpan Time { get; private set; }

        public string LargeScaleIndex {
            get {
                if (Time.Milliseconds == 0) {
                    return Time.ToString("hh\\:mm\\:ss");
                }
                return string.Empty;
            }
        }

        public string SmallScaleIndex {
            get {
                return Time.Milliseconds.ToString("000");
            }
        }

        public bool IsAlternative {
            get { 
                return (Time.Milliseconds % 100) == 0; 
            }
        }

        public AxisMarkerViewModel(TimeSpan markerOffset) {
            _markerOffset = markerOffset;
            Time = markerOffset;
        }

        public void UpdateTime(TimeSpan newTime) {
            Time = newTime + _markerOffset;
            RaisePropertyChanged("Time");
            RaisePropertyChanged("LargeScaleIndex");
            RaisePropertyChanged("SmallScaleIndex");
            RaisePropertyChanged("IsAlternative");
        }
    }
}
