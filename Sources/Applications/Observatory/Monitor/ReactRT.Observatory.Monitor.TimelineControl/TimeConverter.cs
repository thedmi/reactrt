﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.TimelineControl {
    /// <summary>
    /// This helper class converts <code>TimeSpan</code> objects to pixel values and back.
    /// </summary>
    public class TimeConverter {
        private bool _isInitialized = false;

        private DateTime _startReference;

        public void Reset() {
            _isInitialized = false;
        }

        public double PixelsPerMillisecond { get; set; }

        public double ToAbsolutePixels(DateTime d) {

            if (_isInitialized) {
                return ToPixels(d - _startReference);
            } else {
                return 0;
            }
        }

        public double ToPixels(TimeSpan duration) {
            return duration.TotalMilliseconds * PixelsPerMillisecond;
        }

        public TimeSpan ToRelativeTime(DateTime absoluteTime) {
            if (_isInitialized) {
                return absoluteTime - _startReference;
            } else {
                return TimeSpan.FromMilliseconds(0);
            }
        }

        public void SetStartReference(DateTime startReference) {
            _startReference = startReference;
            _isInitialized = true;
        }

        public TimeSpan ToTimeSpan(double value) {
            return TimeSpan.FromMilliseconds(value / PixelsPerMillisecond);
        }
    }
}
