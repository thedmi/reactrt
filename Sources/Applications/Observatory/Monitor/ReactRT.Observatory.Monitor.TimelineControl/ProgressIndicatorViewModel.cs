﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using System.Windows;
using GalaSoft.MvvmLight;
using ReactRT.Observatory.Monitor.Domain;

namespace ReactRT.Observatory.Monitor.TimelineControl {

    public class ProgressIndicatorViewModel : ViewModelBase {

        private bool _progressIndicatorActive = false;

        public double FractionCompleted { get; private set; }

        public Visibility LoadedDataVisibility {
            get {
                return _progressIndicatorActive ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public Visibility ProgressBarVisibility {
            get {
                return _progressIndicatorActive ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public string Message { get; private set; }

        public void SetSession(Session session) {
            if (session.IsReplaySession) {
                _progressIndicatorActive = true;

                session.RawEventLogProvider.BulkDataCompleted += SafeHandleBulkDataCompleted;
                session.RawEventLogProvider.BulkDataProgress += SafeHandleBulkDataProgress;

                Message = "Loading reaction data...";

                RaisePropertyChanged("FractionCompleted");
                RaisePropertyChanged("Message");
            } else {
                _progressIndicatorActive = false;
            }

            RaisePropertyChanged("LoadedDataVisibility");
            RaisePropertyChanged("ProgressBarVisibility");
        }


        private void SafeHandleBulkDataCompleted(object sender, EventArgs e) {
            Application.Current.Dispatcher.Invoke(new HandleBulkDataCompletedDelegate(HandleBulkDataCompleted));
        }

        private delegate void HandleBulkDataCompletedDelegate();

        private void HandleBulkDataCompleted() {
            _progressIndicatorActive = false;

            RaisePropertyChanged("LoadedDataVisibility");
            RaisePropertyChanged("ProgressBarVisibility");
        }


        private void SafeHandleBulkDataProgress(object sender, BulkDataProgressEventArgs e) {
            Application.Current.Dispatcher.Invoke(new HandleBulkDataProgressDelegate(HandleBulkDataProgress), new object[] { e.PercentageCompleted });
        }

        private delegate void HandleBulkDataProgressDelegate(double fractionCompleted);

        private void HandleBulkDataProgress(double fractionCompleted) {
            FractionCompleted = fractionCompleted;
            RaisePropertyChanged("FractionCompleted");

            if (fractionCompleted > 0.95) {
                Message = "Preparing data for view...";
                RaisePropertyChanged("Message");
            }
        }
    }

}
