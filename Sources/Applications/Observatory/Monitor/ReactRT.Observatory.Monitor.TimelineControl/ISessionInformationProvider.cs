﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.TimelineControl
{

    public class SessionDurationChangedEventArgs : EventArgs
    {
        public SessionDurationChangedEventArgs(TimeSpan newDuration)
        {
            NewDuration = newDuration;
        }

        public TimeSpan NewDuration { get; private set; }
    }

    public interface ISessionInformationProvider
    {
        event EventHandler<SessionDurationChangedEventArgs> SessionDurationChanged;

        TimeSpan SessionDuration { get; }
    }
}
