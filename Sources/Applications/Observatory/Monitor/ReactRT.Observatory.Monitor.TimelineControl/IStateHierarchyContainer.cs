﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.TimelineControl {

    public interface IStateHierarchyContainer {
        List<StateViewModel> StateHierarchyList { get; }
    }
}
