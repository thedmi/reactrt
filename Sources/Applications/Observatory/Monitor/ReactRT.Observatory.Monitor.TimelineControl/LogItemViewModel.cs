﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.Domain;

namespace ReactRT.Observatory.Monitor.TimelineControl {

    public class LogItemViewModel {

        private readonly LogItem _logItem;

        private readonly TimeConverter _timeConverter;

        public LogItemViewModel(LogItem item, TimeConverter timeConverter, int index) {
            _logItem = item;
            _timeConverter = timeConverter;

            Index = index;
        }

        public TimeSpan Timestamp {
            get { return _timeConverter.ToRelativeTime(_logItem.Timestamp); }
        }

        public string EventType {
            get { return DeCamelCase(_logItem.EventType.ToString()); }
        }

        public int SequenceNumber {
            get { return _logItem.SequenceNumber; }
        }

        public string EventDescription {
            get {
                switch (_logItem.EventType) {
                    case LogEventType.DispatchStarted:
                        return "Trigger: " + _logItem.EventDescription;
                    case LogEventType.Initialized:
                        return "Initial state: " + _logItem.EventDescription;
                    case LogEventType.PriorityAnnounce:
                        return "Priority: " + _logItem.EventDescription;
                    case LogEventType.StateChanged:
                        return "To " + _logItem.EventDescription;
                    case LogEventType.TimerArmed:
                    case LogEventType.TimerDisarmed:
                        return "Timeout: " + _logItem.EventDescription + " ms";
                }
                return _logItem.EventDescription;
            }
        }

        public int Index { get; private set; }

        private string DeCamelCase(string s) {
            StringBuilder output = new StringBuilder();

            foreach (char c in s) {
                if (char.IsUpper(c)) {
                    output.Append(' ');
                }
                output.Append(c);
            }

            return output.ToString().Trim();
        }

    }
}
