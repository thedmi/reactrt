﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.Domain;
using System.Text.RegularExpressions;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Windows;
using GalaSoft.MvvmLight;

namespace ReactRT.Observatory.Monitor.TimelineControl {

    public class ReactionViewModel : ViewModelBase, IComparable<ReactionViewModel> {

        #region Private fields

        private readonly Reaction _reaction;

        private readonly ISessionInformationProvider _sessionInformationProvider;

        private readonly StateHierarchyHelper _stateHierarchyHelper;

        private readonly TimeConverter _timeConverter;

        private readonly Regex _transitionTargetRegex = new Regex(@"(.*)(\w+)$", RegexOptions.RightToLeft);

        private ReactionViewModel _predecessor = null;
        private ReactionViewModel _successor = null;

        private Window _detailsWindow;

        private double _fractionalOffset = 0;

        #endregion


        public ReactionViewModel(Reaction r, ReactionViewModel predecessor, ISessionInformationProvider sessionInformationProvider,
                StateHierarchyHelper stateHierarchyHelper, TimeConverter timeConverter) {

            _reaction = r;
            _predecessor = predecessor;
            _sessionInformationProvider = sessionInformationProvider;
            _stateHierarchyHelper = stateHierarchyHelper;
            _timeConverter = timeConverter;

            if (_predecessor != null) {
                _predecessor.SetSuccessor(this);
            }

            _sessionInformationProvider.SessionDurationChanged += HandleSessionDurationChanged;

            if (r.LogItems.Count < 1) {
                throw new ArgumentException("When creating a ReactionViewModel from a Reaction, that Reaction must have at least one event log item");
            }
        }


        #region Properties

        public List<LogItemViewModel> LogItems {
            get {
                return _reaction.LogItems.ConvertAll(item => new LogItemViewModel(item, _timeConverter, _reaction.LogItems.IndexOf(item) + 1));
            }
        }

        public int SequenceNumber {
            get { return _reaction.SequenceNumber; }
        }

        public TimeSpan StartTime {
            get { return _timeConverter.ToRelativeTime(AbsoluteStartTime); }
        }

        public TimeSpan EndTime {
            get { return _timeConverter.ToRelativeTime(AbsoluteEndTime); }
        }

        public DateTime AbsoluteStartTime {
            get { return _reaction.Start; }
        }

        public DateTime AbsoluteEndTime {
            get { return _reaction.End; }
        }

        public TimeSpan Duration {
            get { return _reaction.Duration; }
        }

        public double Offset {
            get { 
                return _timeConverter.ToAbsolutePixels(AbsoluteStartTime) + GetOverlapStackLevel() * 4.0; 
            }
        }

        public double FractionalOffset {
            get { return _fractionalOffset; }
        }

        public double Length {
            get { return _timeConverter.ToAbsolutePixels(AbsoluteEndTime) - Offset; }
        }

        public bool PerformsTransition {
            get { return _reaction.PerformsTransition; }
        }

        public bool IsInitialization {
            get { return _reaction.IsInitialization; }
        }

        public string TransitionTargetName {
            get { return _transitionTargetRegex.Match(_reaction.TransitionTarget).Groups[2].Value; }
        }

        public string TransitionTargetPath {
            get {
                if (!_reaction.PerformsTransition && !_reaction.IsInitialization) {
                    return "(internal)";
                }
                return _transitionTargetRegex.Match(_reaction.TransitionTarget).Groups[1].Value;
            }
        }

        public string Trigger {
            get { return _reaction.Trigger; }
        }

        public string TriggerDescription {
            get {
                if (IsInitialization) {
                    return string.Empty;
                } else {
                    return "On " + _reaction.Trigger;
                }
            }
        }

        public string StateDescription {
            get {
                if (IsInitialization) {
                    return "To " + _reaction.TransitionTarget;
                } else if (PerformsTransition) {
                    return "From " + _reaction.StartState + " to " + _reaction.TransitionTarget;
                } else {
                    return "In " + _reaction.StartState;
                }
            }
        }

        public string FormattedDuration {
            get {
                if (_reaction.Duration.TotalSeconds >= 1) {
                    return _reaction.Duration.ToString(@"h\:mm\:ss\.ffffff");
                } else {
                    return _reaction.Duration.Milliseconds.ToString("000") + "'" +
                        (_reaction.Duration.Ticks / 10 % 1000).ToString("000") + " \u03BCs";
                }
            }
        }

        public double LengthToNextReaction {
            get {
                if (_successor != null) {
                    return _successor.Offset - Offset;
                }
                // If this is the last reaction of the instance, draw the line as 
                // long as the reaction, but at least 30px.
                return Math.Max(Length, 30);
            }
        }

        public bool IsStateIndexValid {
            get {
                return StateIndex >= 0;
            }
        }

        public double StateIndex {
            get {
                if (PerformsTransition || IsInitialization) {
                    return _stateHierarchyHelper.StateIndex(_reaction.TransitionTarget);
                } else {
                    return _stateHierarchyHelper.StateIndex(_reaction.StartState);
                }
            }
        }

        public double StateStepOffset {
            get {
                if (_predecessor != null) {
                    if (_predecessor.StateIndex < StateIndex) {
                        return _predecessor.StateIndex + 0.5;
                    } else {
                        return StateIndex + 0.5;
                    }
                }
                return 0;
            }
        }

        public double StateStepDifference {
            get {
                if (_predecessor != null) {
                    return Math.Abs(_predecessor.StateIndex - StateIndex);
                }
                return 0;
            }
        }

        public Visibility StateStepVisibility {
            get {
                if (PerformsTransition && StateStepDifference > 0) {
                    return Visibility.Visible;
                } else {
                    return Visibility.Collapsed;
                }
            }
        }

        public ICommand ShowDetailsCommand {
            get {
                return new RelayCommand<Window>((Window callingWindow) => {
                    // If the window is null or has been closed, a new one must be instantiated
                    if (_detailsWindow == null || !_detailsWindow.IsLoaded) {
                        _detailsWindow = new ReactionDetail();
                        _detailsWindow.DataContext = this;
                        _detailsWindow.Owner = callingWindow;
                    }
                    _detailsWindow.Show();
                    _detailsWindow.Focus();
                });
            }
        }

        #endregion


        public int CompareTo(ReactionViewModel other) {
            int timeCompare = AbsoluteStartTime.CompareTo(other.AbsoluteStartTime);
            if (timeCompare == 0) {
                return _reaction.LogItems.First().SequenceNumber.CompareTo(other._reaction.LogItems.First().SequenceNumber);
            } else {
                return timeCompare;
            }
        }

        public void SetSuccessor(ReactionViewModel successor) {
            _successor = successor;
            RaisePropertyChanged("LengthToNextReaction");
        }

        private void HandleSessionDurationChanged(object sender, SessionDurationChangedEventArgs e) {
            if (e.NewDuration.Ticks == 0) {
                _fractionalOffset = 0;
            } else {
                _fractionalOffset = (double)StartTime.Ticks / (e.NewDuration.Ticks);
            }
            RaisePropertyChanged("FractionalOffset");
        }

        private int GetOverlapStackLevel() {

            if (_predecessor != null) {

                double horizontalOffsetDifference =
                    Math.Abs(_timeConverter.ToAbsolutePixels(_predecessor.AbsoluteStartTime) - _timeConverter.ToAbsolutePixels(AbsoluteStartTime));

                if (horizontalOffsetDifference < 1.0) {
                    return _predecessor.GetOverlapStackLevel() + 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
    }
}
