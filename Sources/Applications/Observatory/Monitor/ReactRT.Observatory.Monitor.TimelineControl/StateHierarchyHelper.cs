﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.Domain;

namespace ReactRT.Observatory.Monitor.TimelineControl
{
    /// <summary>
    /// This helper class reads the state hierarchy from the associated state machine
    /// and caches it in a view-friendly format for quick rendering.
    /// </summary>
    public class StateHierarchyHelper
    {
        public List<StateViewModel> HierarchyList { get; private set; }


        public StateHierarchyHelper(StateMachine stateMachine)
        {
            HierarchyList = new List<StateViewModel>();

            BuildHierarchyList(stateMachine);
        }

        public int StateIndex(string fullyQualifiedStateName)
        {
            return HierarchyList.FindIndex(svm => svm.FullyQualifiedName == fullyQualifiedStateName);
        }


        private void BuildHierarchyList(StateMachine stateMachine)
        {
            foreach (IState s in stateMachine.StateHierarchy)
            {
                HierarchyList.AddRange(GetSubstateList(s, 0));
            }
        }

        private List<StateViewModel> GetSubstateList(IState s, int depth)
        {
            List<StateViewModel> l = new List<StateViewModel>();

            l.Add(new StateViewModel(s, depth));

            foreach (IState substate in s.Substates)
            {
                l.AddRange(GetSubstateList(substate, depth + 1));
            }
            return l;
        }
    }
}
