﻿using System.Windows.Controls;
using System.Windows;

namespace ReactRT.Observatory.Monitor.TimelineControl
{
    public class CaptionedUserControl : UserControl
    {
        public static readonly DependencyProperty IsCaptionVisibleProperty =
            DependencyProperty.Register("IsCaptionVisible", typeof(bool), typeof(CaptionedUserControl),
                new FrameworkPropertyMetadata(true));

        public bool IsCaptionVisible
        {
            get { return (bool)GetValue(IsCaptionVisibleProperty); }
            set { SetValue(IsCaptionVisibleProperty, value); }
        }

        public Visibility CaptionVisibility
        {
            get { return IsCaptionVisible ? Visibility.Visible : Visibility.Collapsed; }
        }
    }
}
