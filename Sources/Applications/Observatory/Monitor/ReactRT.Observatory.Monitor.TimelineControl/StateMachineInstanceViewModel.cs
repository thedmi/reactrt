﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using ReactRT.Observatory.Monitor.Domain;
using System.Windows;

namespace ReactRT.Observatory.Monitor.TimelineControl {

    public class ReactionViewModelCreatedEventArgs : EventArgs {
        public ReactionViewModel Reaction { get; private set; }

        public ReactionViewModelCreatedEventArgs(ReactionViewModel r) {
            Reaction = r;
        }
    }

    public class StateMachineInstanceViewModel : ViewModelBase {

        private readonly StateMachineInstance _instance;

        private readonly IStateHierarchyContainer _hierarchyContainer;

        private readonly ISessionInformationProvider _sessionInformationProvider;

        private readonly TimeConverter _timeConverter;

        private readonly StateHierarchyHelper _stateHierarchyHelper;

        public StateMachineInstanceViewModel(StateMachineInstance instance, IStateHierarchyContainer hierarchyContainer, ISessionInformationProvider sessionInformationProvider, TimeConverter timeConverter, StateHierarchyHelper stateHierarchyHelper) {
            _instance = instance;
            _hierarchyContainer = hierarchyContainer;
            _sessionInformationProvider = sessionInformationProvider;
            _timeConverter = timeConverter;
            _stateHierarchyHelper = stateHierarchyHelper;

            Reactions = new ObservableSortedSet<ReactionViewModel>();

            _instance.ReactionAdded += (object sender, ReactionAddedEventArgs e) => SafeAddReaction(e.Reaction);
        }


        public event EventHandler<ReactionViewModelCreatedEventArgs> ReactionViewModelCreated = delegate { };


        public ObservableSortedSet<ReactionViewModel> Reactions { get; private set; }

        public bool HasReactions { get; private set; }

        public string ThreadPriority {
            get {
                if (_instance.ThreadPrio != 0) {
                    return _instance.ThreadPrio.ToString();
                } else {
                    return "unknown";
                }
            }
        }

        public List<StateViewModel> StateHierarchyList {
            get {
                return _hierarchyContainer.StateHierarchyList;
            }
        }

        private bool _isStateChangeCompartmentVisible = false;
        public bool IsStateChangeCompartmentVisible {
            get { return _isStateChangeCompartmentVisible; }
            set {
                if (_isStateChangeCompartmentVisible != value) {
                    _isStateChangeCompartmentVisible = value;
                    RaisePropertyChanged("IsStateChangeCompartmentVisible");
                }
            }
        }

        public int NumberOfReactions {
            get {
                return Reactions.Count;
            }
        }

        public string Name {
            get {
                if (_instance.InstanceName != string.Empty) {
                    return _instance.InstanceName;
                } else {
                    return InstanceId;
                }
            }
        }

        public string InstanceName {
            get {
                if (_instance.InstanceName != string.Empty) {
                    return _instance.InstanceName;
                } else {
                    return "(anonymous)";
                }
            }
        }

        public string InstanceId {
            get {
                return "0x" + Convert.ToString(_instance.InstanceId, 16);
            }
        }

        public string CurrentStateName {
            get { return _instance.CurrentStateName; }
        }

        public string CurrentStateDescription {
            get { return "[ " + CurrentStateName + " ]"; }
        }

        public double ActivityEndOffset {
            get {
                if (Reactions.Count == 0) {
                    return 0;
                } else {
                    return _timeConverter.ToAbsolutePixels(AbsoluteActivityEnd);
                }
            }
        }

        public double ActivityOffset {
            get {
                if (Reactions.Count == 0) {
                    return 0;
                } else {
                    return _timeConverter.ToAbsolutePixels(AbsoluteActivityStart);
                }
            }
        }

        public double ActivityLineLength {
            get {
                return ActivityEndOffset - ActivityOffset;
            }
        }

        public string ActivityStartDescription {
            get {
                if (HasReactions) {
                    return Reactions.First().StartTime.ToString(@"h\:mm\:ss\.ffffff");
                }
                return String.Empty;
            }
        }

        public string ActivityEndDescription {
            get {
                if (HasReactions) {
                    return Reactions.Last().EndTime.ToString(@"h\:mm\:ss\.ffffff");
                }
                return String.Empty;
            }
        }

        public DateTime AbsoluteActivityStart {
            get {
                return Reactions.First().AbsoluteStartTime;
            }
        }

        public DateTime AbsoluteActivityEnd {
            get {
                return Reactions.Last().AbsoluteEndTime;
            }
        }



        private delegate void AddReactionDelegate(Reaction r);

        private void AddReaction(Reaction r) {
            ReactionViewModel rvm = new ReactionViewModel(r, Reactions.LastOrDefault(), _sessionInformationProvider, _stateHierarchyHelper, _timeConverter);
            Reactions.Add(rvm);

            ReactionViewModelCreated(this, new ReactionViewModelCreatedEventArgs(rvm));

            if (!HasReactions) {
                HasReactions = true;
                RaisePropertyChanged("HasReactions");
                RaisePropertyChanged("AbsoluteActivityStart");
                RaisePropertyChanged("ActivityOffset");
                RaisePropertyChanged("ActivityStartDescription");
            }

            if (r.IsInitialization) {
                RaisePropertyChanged("ThreadPriority");
            }

            RaisePropertyChanged("ActivityEndOffset");
            RaisePropertyChanged("ActivityLineLength");
            RaisePropertyChanged("ActivityEndDescription");
            RaisePropertyChanged("AbsoluteActivityEnd");
            RaisePropertyChanged("CurrentStateName");
            RaisePropertyChanged("CurrentStateDescription");
            RaisePropertyChanged("NumberOfReactions");

        }

        private void SafeAddReaction(Reaction r) {
            Application.Current.Dispatcher.Invoke(new AddReactionDelegate(AddReaction), new object[] { r });
        }
    }
}
