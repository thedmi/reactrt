﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;

namespace ReactRT.Observatory.Monitor.TimelineControl {

    public class AxisViewModel : ViewModelBase {

        private const int _numberOfMarkers = 50;
        private readonly TimeSpan _axisIncrement = TimeSpan.FromMilliseconds(10);

        private readonly ITimelinePositionTracker _timelinePositionTracker;
        private readonly TimeConverter _timeConverter;

        private double _currentPosition;

        private TimeSpan _previousMarkerOffset;


        public AxisViewModel(ITimelinePositionTracker timelinePositionTracker, TimeConverter timeConverter) {
            _timelinePositionTracker = timelinePositionTracker;
            _timelinePositionTracker.TimelinePositionChanged += HandleTimelinePositionChanged;

            _timeConverter = timeConverter;

            Markers = new ObservableCollection<AxisMarkerViewModel>();

            for (int i = -(_numberOfMarkers / 2); i < (_numberOfMarkers / 2); i++) {
                Markers.Add(new AxisMarkerViewModel(TimeSpan.FromMilliseconds(_axisIncrement.TotalMilliseconds * i)));
            }
        }

        public ObservableCollection<AxisMarkerViewModel> Markers { get; private set; }

        public double MarkerWidth {
            get { 
                return _timeConverter.ToPixels(_axisIncrement); 
            }
        }

        public double AxisExtent {
            get { 
                return _numberOfMarkers * MarkerWidth; 
            }
        }

        public double TranslationTransformXValue {
            get {
                return -(_currentPosition % MarkerWidth);
            }
        }

        private void HandleTimelinePositionChanged(object sender, TimelinePositionChangedEventArgs e) {
            _currentPosition = e.NewPosition;

            TimeSpan currentMarkerOffset = _timeConverter.ToTimeSpan(e.NewPosition - (e.NewPosition % MarkerWidth));

            if (currentMarkerOffset != _previousMarkerOffset) {
                UpdateMarkers(currentMarkerOffset);
                _previousMarkerOffset = currentMarkerOffset;
            }

            RaisePropertyChanged("TranslationTransformXValue");
        }

        private void UpdateMarkers(TimeSpan newPosition) {
            foreach (AxisMarkerViewModel marker in Markers) {
                marker.UpdateTime(newPosition);
            }
        }
    }
}
