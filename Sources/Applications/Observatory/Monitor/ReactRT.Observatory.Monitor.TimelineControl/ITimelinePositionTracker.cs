﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.TimelineControl
{
    public class TimelinePositionChangedEventArgs : EventArgs
    {
        public TimelinePositionChangedEventArgs(double newPosition, TimeSpan newTime)
        {
            NewTime = newTime;
            NewPosition = newPosition;
        }

        public TimeSpan NewTime { get; private set; }
        public double NewPosition { get; private set; }
    }

    public interface ITimelinePositionTracker
    {
        event EventHandler<TimelinePositionChangedEventArgs> TimelinePositionChanged;
    }
}
