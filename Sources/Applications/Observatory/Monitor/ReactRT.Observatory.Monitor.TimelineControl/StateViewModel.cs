﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.Domain;

namespace ReactRT.Observatory.Monitor.TimelineControl
{
    public class StateViewModel
    {
        private IState _state;


        public int DepthInHierarchy { get; private set; }

        public double Indentation
        {
            get { return DepthInHierarchy * 10; }
        }

        public string FullyQualifiedName
        {
            get { return _state.FullyQualifiedName; }
        }

        public string Name
        {
            get { return _state.Name; }
        }


        public StateViewModel(IState state, int depthInHierarchy)
        {
            _state = state;
            DepthInHierarchy = depthInHierarchy;
        }
    }
}
