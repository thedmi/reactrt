﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace ReactRT.Observatory.Monitor.TimelineControl
{
    /// <summary>
    /// This is an efficient collection for use with dependency properties for data binding.
    /// 
    /// Note that only add operations trigger a notification. This is because in the current
    /// use cases elements are just added, but never removed.
    /// </summary>
    /// <typeparam name="T">The type of the collection item.</typeparam>
    public class ObservableSortedSet<T> : SortedSet<T>, INotifyCollectionChanged
    {
        public new bool Add(T item)
        {
            bool retval = base.Add(item);
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            return retval;
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged = delegate { };
    }
}
