﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace ReactRT.Observatory.Monitor.Domain {

    public interface IState : IComparable<IState> {

        ISet<IState> Substates { get; }

        string Name { get; }

        string FullyQualifiedName { get; }
    }
}
