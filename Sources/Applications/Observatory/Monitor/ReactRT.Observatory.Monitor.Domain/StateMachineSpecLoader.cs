﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace ReactRT.Observatory.Monitor.Domain {

    class StateMachineSpecLoader {

        public List<StateMachine> LoadStateMachineSpec(string uri) {
            XmlDocument doc = new XmlDocument();
            doc.Load(uri);

            return ParseStateMachineSpec(doc);
        }

        private List<StateMachine> ParseStateMachineSpec(XmlDocument spec) {

            XmlNodeList stateMachineNodes = spec.SelectNodes(@"//stateMachine");

            List<StateMachine> stateMachines = new List<StateMachine>();

            foreach (XmlNode stateMachineNode in stateMachineNodes) {
                string name = stateMachineNode.Attributes["name"].Value;
                string nameSpace = stateMachineNode.Attributes["namespace"].Value;

                SortedSet<IState> stateHierarchy = ParseStateHierarchy(stateMachineNode, null);

                StateMachine sm = new StateMachine(name, nameSpace, stateHierarchy);

                stateMachines.Add(sm);
            }

            return stateMachines;
        }

        private SortedSet<IState> ParseStateHierarchy(XmlNode container, State parent) {
            XmlNodeList childNodes = container.SelectNodes(@"state");

            SortedSet<IState> children = new SortedSet<IState>();

            foreach (XmlNode childNode in childNodes) {
                State s = new State(parent, childNode.Attributes["name"].Value);
                ParseStateHierarchy(childNode, s);

                children.Add(s);
            }

            return children;
        }
    }
}
