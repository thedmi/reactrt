﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.Domain {

    public enum LogEventType {
        DispatchStarted,
        DispatchCompleted,
        StateChanged,
        TimerArmed,
        TimerDisarmed,
        PriorityAnnounce,
        Initialized
    }

    /// <summary>
    /// This class represents a single log item as captured and transmitted from the target. This class is immutable.
    /// </summary>
    public class LogItem {

        public LogItem(DateTime timestamp, string stateMachine, int instanceId, string instanceName, LogEventType eventType, string eventDescription, int sequenceNumber) {
            Timestamp = timestamp;
            StateMachine = stateMachine;
            InstanceId = instanceId;
            InstanceName = instanceName;
            EventType = eventType;
            EventDescription = eventDescription;
            SequenceNumber = sequenceNumber;
        }

        public DateTime Timestamp { get; private set; }

        public string StateMachine { get; private set; }

        public int InstanceId { get; private set; }

        public string InstanceName { get; private set; }

        public LogEventType EventType { get; private set; }

        public string EventDescription { get; private set; }

        public int SequenceNumber { get; private set; }

        public override string ToString() {
            return EventType + " " + Timestamp + " ( " + EventDescription + " )";
        }
    }
}
