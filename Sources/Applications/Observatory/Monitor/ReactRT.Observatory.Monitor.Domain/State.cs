﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.Domain {

    /// <summary>
    /// This class represents a state in the state hierarchy of a state machine. This is an immutable class.
    /// </summary>
    class State : IState {

        private readonly State _parent;

        public State(State parent, string name) {
            Substates = new SortedSet<IState>();

            _parent = parent;
            Name = name;

            if (_parent != null) {
                _parent.Substates.Add(this);
            }
        }

        public ISet<IState> Substates { get; private set; }

        public string Name { get; private set; }

        public string FullyQualifiedName {
            get {
                if (_parent == null) {
                    return "Sm::" + Name;
                } else {
                    return _parent.FullyQualifiedName + "::" + Name;
                }
            }
        }

        public override string ToString() {
            return (Substates.Count > 0 ? "Composite" : "Simple") + " state " + Name;
        }

        public int CompareTo(IState other) {
            return Name.CompareTo(other.Name);
        }
    }
}
