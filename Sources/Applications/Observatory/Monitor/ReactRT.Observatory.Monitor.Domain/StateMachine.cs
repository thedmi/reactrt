﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using log4net;
using System.ComponentModel;

namespace ReactRT.Observatory.Monitor.Domain {


    public class InstanceAddedEventArgs : EventArgs {

        public StateMachineInstance Instance { get; private set; }

        public InstanceAddedEventArgs(StateMachineInstance i) {
            Instance = i;
        }
    }

    public class StateMachine {

        private static readonly ILog log = LogManager.GetLogger(typeof(StateMachine));

        public StateMachine(string name, string nameSpace, ISet<IState> stateHierarchy) {
            Name = name;
            Namespace = nameSpace;
            StateHierarchy = stateHierarchy;

            Instances = new Dictionary<int, StateMachineInstance>();
        }


        public event EventHandler<InstanceAddedEventArgs> InstanceAdded = delegate { };


        public string Name { get; private set; }

        public string Namespace { get; private set; }

        public ISet<IState> StateHierarchy { get; private set; }

        public Dictionary<int, StateMachineInstance> Instances { get; private set; }


        public void ProcessLogItem(LogItem item) {

            if (Instances.ContainsKey(item.InstanceId)) {
                Instances[item.InstanceId].ProcessLogItem(item);
            } else {
                StateMachineInstance newInstance = new StateMachineInstance(item.InstanceId, item.InstanceName);
                Instances.Add(item.InstanceId, newInstance);
                InstanceAdded(this, new InstanceAddedEventArgs(newInstance));

                newInstance.ProcessLogItem(item);
            }
        }

        public override string ToString() {
            return Name + " containing " + Instances.Count + " reactions";
        }
    }
}
