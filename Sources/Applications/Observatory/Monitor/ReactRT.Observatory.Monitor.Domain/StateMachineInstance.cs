﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReactRT.Observatory.Monitor.Domain {

    public class ReactionAddedEventArgs : EventArgs {

        public Reaction Reaction { get; private set; }

        public ReactionAddedEventArgs(Reaction r) {
            Reaction = r;
        }
    }

    public class StateMachineInstance {

        private readonly ReactionBuilder _reactionBuilder;


        public StateMachineInstance(int instanceId, string instanceName) {
            InstanceId = instanceId;
            InstanceName = instanceName;

            ThreadPrio = 0;

            _reactionBuilder = new ReactionBuilder();
            _reactionBuilder.ReactionCompleted += HandleReactionCompleted;

            Reactions = new List<Reaction>();
        }

        public event EventHandler<ReactionAddedEventArgs> ReactionAdded = delegate { };

        public int ThreadPrio { get; private set; }

        public int InstanceId { get; private set; }

        public string InstanceName { get; private set; }

        public List<Reaction> Reactions { get; private set; }

        public string CurrentStateName {
            get {
                if (Reactions.Count != 0) {
                    string endState = Reactions.Last().EndState;
                    string[] stateNames = endState.Split(new Char[] { ':', ':' });

                    return stateNames.LastOrDefault();
                }

                return string.Empty;
            }
        }


        public void ProcessLogItem(LogItem item) {
            if (item.EventType == LogEventType.PriorityAnnounce) {
                ThreadPrio = Convert.ToInt32(item.EventDescription);
            }
            _reactionBuilder.AddLogItem(item);
        }

        private void HandleReactionCompleted(Object sender, ReactionBuilder.ReactionCompletedEventArgs e) {
            Reactions.Add(e.Reaction);

            ReactionAdded(this, new ReactionAddedEventArgs(e.Reaction));
        }
    }
}
