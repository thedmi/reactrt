﻿using System;
using System.Collections.Generic;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using log4net;

namespace ReactRT.Observatory.Monitor.Domain {

    /// <summary>
    /// This class represents a monitoring session and as such contains all state machines. This class
    /// is immutable.
    /// </summary>
    public class Session {

        private static readonly ILog log = LogManager.GetLogger(typeof(StateMachine));

        private readonly LogItemParser _logItemParser = new LogItemParser();

        private readonly StateMachineSpecLoader _stateMachineLoader = new StateMachineSpecLoader();

        private readonly Dictionary<string, StateMachine> _stateMachines = new Dictionary<string, StateMachine>();

        private readonly IRawEventLogProvider _rawEventLogProvider;


        public Session(IRawEventLogProvider rawEventLogProvider, string stateMachineSpecLocation) {
            _rawEventLogProvider = rawEventLogProvider;
            _rawEventLogProvider.RawEventLogReady += (sender, e) => ParseRawEventLog(e.Message);

            if (stateMachineSpecLocation != string.Empty) {
                _stateMachineLoader.LoadStateMachineSpec(stateMachineSpecLocation).ForEach(AddStateMachine);
            }
        }


        public List<StateMachine> StateMachines {
            get { return new List<StateMachine>(_stateMachines.Values); }
        }

        public bool IsReplaySession {
            get { return _rawEventLogProvider.IsBulkDataProvider; }
        }

        public IRawEventLogProvider RawEventLogProvider {
            get { return _rawEventLogProvider; }
        }

        private void AddStateMachine(StateMachine s) {
            _stateMachines.Add(s.Name, s);
        }

        private void ParseRawEventLog(String rawEventLog) {
            try {
                LogItem item = _logItemParser.ParseRawEventLog(rawEventLog);

                if (_stateMachines.ContainsKey(item.StateMachine)) {
                    _stateMachines[item.StateMachine].ProcessLogItem(item);
                } else {
                    log.Error("State machine " + item.StateMachine + " unknown, cannot add log item " + item);
                }
            } catch (System.Exception ex) {
                log.Error(ex);
            }
        }
    }
}
