﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using System.Text.RegularExpressions;

namespace ReactRT.Observatory.Monitor.Domain {

    public class ReactionBuilder {

        #region Reaction completed event args class

        public class ReactionCompletedEventArgs : EventArgs {

            public Reaction Reaction { get; private set; }

            public ReactionCompletedEventArgs(Reaction r) {
                Reaction = r;
            }
        }

        #endregion


        public event EventHandler<ReactionCompletedEventArgs> ReactionCompleted = delegate { };

        private Reaction _underConstruction;

        private Reaction _predecessor = null;

        private int _currentSequenceNumber = 0;

        public ReactionBuilder() {
            _underConstruction = new Reaction(_currentSequenceNumber++);
        }

        public void AddLogItem(LogItem item) {

            if (_underConstruction.LogItems.Count > 0 && item.EventType == LogEventType.DispatchStarted) {
                StartNewReaction();
            }

            _underConstruction.AddLogItem(item);

            if (IsEndOfReaction(item)) {
                StartNewReaction();
            }
        }

        private bool IsEndOfReaction(LogItem item) {
            return item.EventType == LogEventType.DispatchCompleted || item.EventType == LogEventType.Initialized;
        }

        private void StartNewReaction() {
            _underConstruction.SetPredecessorAndSetCompleted(_predecessor);

            ReactionCompleted(this, new ReactionCompletedEventArgs(_underConstruction));

            _predecessor = _underConstruction;
            _underConstruction = new Reaction(_currentSequenceNumber++);
        }
    }
}
