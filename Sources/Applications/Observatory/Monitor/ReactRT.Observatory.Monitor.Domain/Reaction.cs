﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace ReactRT.Observatory.Monitor.Domain {

    /// <summary>
    /// A reaction represents a complete run to completion step, which consists of
    /// several event log items. This class exposes only accessors publicly, so for
    /// other packages, it appears immutable.
    /// 
    /// As soon as SetPredecessorAndSetCompleted() is called, the reaction object 
    /// cannot be modified anymore and is thus immutable thereafter.
    /// </summary>
    public class Reaction {

        private readonly List<LogItem> _logItems = new List<LogItem>();

        private Reaction _predecessor;

        private bool _completed = false;

        public Reaction(int sequenceNumber) {
            SequenceNumber = sequenceNumber;
        }

        public int SequenceNumber { get; private set; }

        public DateTime Start {
            get {
                return _logItems.First().Timestamp;
            }
        }

        public DateTime End {
            get {
                return _logItems.Last().Timestamp;
            }
        }

        public TimeSpan Duration {
            get {
                return End - Start;
            }
        }

        public List<LogItem> LogItems {
            get {
                return _logItems;
            }
        }

        public bool IsInitialization {
            get {
                return _logItems.Exists(item => item.EventType == LogEventType.Initialized);
            }
        }

        public bool PerformsTransition {
            get {
                return _logItems.Exists(item => item.EventType == LogEventType.StateChanged);
            }
        }

        public string TransitionTarget {
            get {
                if (IsInitialization) {
                    return _logItems.FindLast(item => item.EventType == LogEventType.Initialized).EventDescription;
                } else if (PerformsTransition) {
                    return _logItems.FindLast(item => item.EventType == LogEventType.StateChanged).EventDescription;
                } else {
                    return string.Empty;
                }
            }
        }

        public string StartState {
            get {
                if (_predecessor != null) {
                    return _predecessor.EndState;
                } else {
                    return string.Empty;
                }
            }
        }

        public string EndState {
            get {
                if (TransitionTarget != string.Empty) {
                    return TransitionTarget;
                } else {
                    return StartState;
                }
            }
        }

        public string Trigger {
            get {
                if (IsInitialization) {
                    return "Initialization";
                }

                var logItem = _logItems.Find(item => item.EventType == LogEventType.DispatchStarted);
                if (logItem != null) {
                    return logItem.EventDescription;
                }

                return "(unknown)";
            }
        }

        internal void AddLogItem(LogItem item) {
            if (_completed) {
                throw new InvalidOperationException("The reaction has already been set complete, no further items can be added.");
            }
            _logItems.Add(item);
        }

        internal void SetPredecessorAndSetCompleted(Reaction r) {
            _predecessor = r;
            _completed = true;
        }

        public override string ToString() {
            return "Reaction with " + _logItems.Count + " items";
        }
    }
}
