﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ReactRT.Observatory.Monitor.Domain {

    class LogItemParser {

        private readonly Regex _regex = new Regex(@"^(\d+) (\w+) \(\s?([a-zA-Z0-9]+) '([^']*)'\s?\): (\w+) \[ (.*) \]");

        private readonly Dictionary<string, LogEventType> eventTypeMap = new Dictionary<string, LogEventType>();

        private int _currentSequenceNumber = -1;

        public LogItemParser() {
            eventTypeMap.Add("dispatchStarted", LogEventType.DispatchStarted);
            eventTypeMap.Add("dispatchCompleted", LogEventType.DispatchCompleted);
            eventTypeMap.Add("stateChanged", LogEventType.StateChanged);
            eventTypeMap.Add("timerArmed", LogEventType.TimerArmed);
            eventTypeMap.Add("timerDisarmed", LogEventType.TimerDisarmed);
            eventTypeMap.Add("initialized", LogEventType.Initialized);
            eventTypeMap.Add("priorityAnnounce", LogEventType.PriorityAnnounce);
        }

        public LogItem ParseRawEventLog(string rawEventLog) {

            if (_regex.IsMatch(rawEventLog)) {
                Match m = _regex.Match(rawEventLog);

                Int64 timestamp = Convert.ToInt64(m.Groups[1].Value);
                string stateMachine = m.Groups[2].Value;
                int instanceId = Convert.ToInt32(m.Groups[3].Value, 16);
                string instanceName = m.Groups[4].Value;
                string eventType = m.Groups[5].Value;
                string eventDescription = m.Groups[6].Value;

                _currentSequenceNumber++;

                return new LogItem(new DateTime(timestamp * 10), stateMachine, instanceId, instanceName, 
                    ResolveEventType(eventType), eventDescription, _currentSequenceNumber);
            }
            throw new FormatException("Raw event log format of '" + rawEventLog + "' is not supported by the parser.");
        }

        private LogEventType ResolveEventType(string eventType) {

            if (eventTypeMap.ContainsKey(eventType)) {
                return eventTypeMap[eventType];
            } else {
                throw new ArgumentException("Invalid event type specifier");
            }
        }
    }
}
