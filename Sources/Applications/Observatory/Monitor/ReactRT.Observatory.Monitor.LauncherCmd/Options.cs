﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace ReactRT.Observatory.Monitor.Launcher {

    class Options {

        [Option("u", "udp", HelpText = "Use UDP source.")]
        public bool UseUdp = false;

        [Option("p", "port", HelpText = "Listen on specified port (for UDP source only).")]
        public int UdpPort = 1984;

        [Option("f", "file", HelpText = "Use specified file as source.")]
        public string FileSourcePath = "";

        [Option("n", null, HelpText = "Number of raw event logs to wait for. Set to 0 for unlimited.")]
        public int NumberOfEventLogs = 0;

        [HelpOption(HelpText = "Display this help text.")]
        public string GetUsage() {

            HelpText help = new HelpText("ReactRT Observatory Command Line Interface");
            help.Copyright = new CopyrightInfo("Daniel Michel", 2011);
            //help.AddPreOptionsLine("Contact me at my blog: http://erikej.blogspot.com");
            //help.AddPreOptionsLine("Check for updates at: http://sqlcecmd.codeplex.com");
            help.AddOptions(this);
            return help;

        }

    }
}
