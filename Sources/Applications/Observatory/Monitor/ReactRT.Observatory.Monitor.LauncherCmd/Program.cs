﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.Launcher;
using CommandLine;
using ReactRT.Observatory.Monitor.DataSource;
using ReactRT.Observatory.Monitor.DataSource.Interface;
using System.Threading;
using System.IO;

namespace ReactRT.Observatory.Monitor.LauncherCmd {

    class Program {

        private bool _completed = false;

        public void Start(string[] args) {
            Options options = new Options();
            ICommandLineParser parser = new CommandLineParser();

            if (parser.ParseArguments(args, options)) {

                if (options.UseUdp) {
                    UdpMode(options);
                } else if (options.FileSourcePath != "") {
                    FileMode(options);
                } else {
                    ShowUsage(options);
                }
            } else {
                ShowUsage(options);
            }
        }

        private void UdpMode(Options options) {
            if (options.FileSourcePath == "") {
                if (options.UdpPort > 0 && options.UdpPort <= 0xffff) {
                    Run(new UdpEventLogReceiver(options.UdpPort), options.NumberOfEventLogs);
                } else {
                    Console.Error.WriteLine("Specified UDP port is invalid.");
                }
            } else {
                ShowUsage(options);
            }
        }

        private void FileMode(Options options) {
            if (!options.UseUdp) {
                if (File.Exists(options.FileSourcePath)) {
                    Run(new FileEventLogReader(options.FileSourcePath), options.NumberOfEventLogs);
                } else {
                    Console.Error.WriteLine("Specified source file not found.");
                }
            } else {
                ShowUsage(options);
            }
        }

        private void Run(IRawEventLogProvider provider, int numberOfEventLogs) {
            var consoleWriter = new RawLogConsoleWriter(provider, numberOfEventLogs);

            provider.Start();

            consoleWriter.Completed += delegate(object s, EventArgs e) {
                _completed = true;
            };

            WaitForCompletion();
        }

        private void WaitForCompletion() {
            while (!_completed) {
                Thread.Sleep(100);
            }
        }

        private void ShowUsage(Options options) {
            Console.Error.WriteLine(options.GetUsage());
        }

        static void Main(string[] args) {
            Program p = new Program();
            p.Start(args);
        }
    }
}
