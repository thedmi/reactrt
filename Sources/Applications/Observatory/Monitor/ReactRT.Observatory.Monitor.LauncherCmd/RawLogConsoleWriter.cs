﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReactRT.Observatory.Monitor.DataSource.Interface;

namespace ReactRT.Observatory.Monitor.LauncherCmd {

    class RawLogConsoleWriter {

        private readonly int _eventThreshold;

        private readonly bool _isThresholdMode = false;

        private int _processedEventLogs = 0;

        private bool _shouldPrint = true;

        public event EventHandler<EventArgs> Completed = delegate { };

        public RawLogConsoleWriter(IRawEventLogProvider rawEventLogProvider, int eventThreshold) {
            _eventThreshold = eventThreshold;
            _isThresholdMode = eventThreshold > 0;
            rawEventLogProvider.RawEventLogReady += HandleRawEventLog;

            if (!_isThresholdMode && rawEventLogProvider.IsBulkDataProvider) {
                rawEventLogProvider.BulkDataCompleted += (sender, x) => Completed(this, new EventArgs());
            }
        }

        private void HandleRawEventLog(object sender, RawEventLogEventArgs e) {
            if (_shouldPrint) {
                _processedEventLogs++;
                System.Console.WriteLine(e.Message);
            }

            if (_isThresholdMode && _processedEventLogs >= _eventThreshold) {
                _shouldPrint = false;
                Completed(this, new EventArgs());
            }
        }
    }
}
