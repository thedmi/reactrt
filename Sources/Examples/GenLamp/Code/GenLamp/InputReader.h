#pragma once

#include <boost/smart_ptr.hpp>

#include <Windows.h>


class Lamp;


class InputReader {

public:

	InputReader(boost::shared_ptr<Lamp> s);

	void start();

	typedef boost::shared_ptr<InputReader> Ptr;

private:

	static DWORD threadMain(LPVOID par);

	boost::shared_ptr<Lamp> dest;

	HANDLE readerThread;
	char readChar();

	void run();


};