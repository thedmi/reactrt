#include "InputReader.h"

#include "gen/Lamp.h"

#include <iostream>



InputReader::InputReader( boost::shared_ptr<Lamp> lamp ) : dest(lamp)
{
}

void InputReader::start()
{
	readerThread = CreateThread(NULL, 1024, (LPTHREAD_START_ROUTINE)&InputReader::threadMain, this, 0, NULL);
}

char InputReader::readChar()
{
	using namespace std;

	cout << "Command: ";
	cout.flush();

	char command;
	char garbage[100];

	cin >> command;
	cin.getline(garbage, 100);

	return command;
}

void InputReader::run()
{
	while(1) {
		char c = readChar();
		if(c == '1') {
			dest->post(Lamp::evOn);
		}
		if(c == '0') {
			dest->post(Lamp::evOff);
		}
		Sleep(100);
	}
}

DWORD InputReader::threadMain( LPVOID self )
{
	assert(self);
	static_cast<InputReader *>(self)->run();
	return 0; // success
}

