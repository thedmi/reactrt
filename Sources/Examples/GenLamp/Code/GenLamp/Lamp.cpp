#include "gen/Lamp.h"

#include <iostream>


using namespace std;


Lamp::Lamp()
{
}

Lamp::~Lamp() 
{
}

void Lamp::printEntryOff()
{
	cout << "(Entry: Off)" << endl;
	cout << "The lamp is off" << endl << endl;
}

void Lamp::printEntryOn()
{
	cout << "(Entry: On)" << endl;
	cout << "The lamp is on" << endl << endl;
}

void Lamp::printExitOn()
{
	cout << "(Exit: On)" << endl;
}

void Lamp::printExitOff()
{
	cout << "(Exit: Off)" << endl;
}

void Lamp::printTransitionOn()
{
	cout << "(Transition: Off->On)" << endl;
}

void Lamp::printTransitionOff()
{
	cout << "(Transition: On->Off)" << endl;
}
