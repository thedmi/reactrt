#include <iostream>

#include <RrtReactor.h>

#include "InputReader.h"
#include "gen/Lamp.h"

#include <tchar.h>



int main(int argc, _TCHAR* argv[]) 
{

	using namespace std;
	using namespace rrt;

	ReactorConfigurator configurator;

	cout << "LAMP DEMO" << endl;
	cout << "~~~~~~~~~" << endl;
	cout << "Type 1 <Enter> to turn on the lamp, 0 <Enter> to turn it off" << endl << endl;

	Lamp::Ptr lamp(new Lamp());
	InputReader::Ptr ir(new InputReader(lamp));

	lamp->start();
	ir->start();

	Reactor::run();    
}

