#pragma once

#include <boost/shared_ptr.hpp>

#include "gen/CommandReceiver.h"

#include <windows.h>



/*
 *	This is an active helper class that converts keyboard inputs to events.
 */
class InputReader {

public:

	typedef boost::shared_ptr<InputReader> Ptr;

	InputReader(CommandReceiver::Ptr dest);

	void start();

private:

	static DWORD threadMain(LPVOID par);

	CommandReceiver::Ptr dest;

	HANDLE readerThread;

	char readChar();

	void run();


};