#include "stdafx.h"

#include "gen/CandyMachine.h"

#include "gen/Lamp.h"


CandyMachine::CandyMachine(boost::shared_ptr<Lamp> lamp) : readyLamp(lamp) 
{
}

CandyMachine::~CandyMachine() 
{
}


void CandyMachine::abort()
{
	using namespace std;
	cout << "[ ";
	
	for(int i = 0; i < coinCount; i++) {
		cout << "o "; 
	}

	cout << "]" << endl;
}

void CandyMachine::releaseCandy()
{
	using namespace std;
	cout << "[ >0< ]" << endl;
}

void CandyMachine::reset()
{
	coinCount = 0;
	readyLamp->off();
}

void CandyMachine::showPaymentComplete()
{
	readyLamp->on();
}

void CandyMachine::processCommand( const Command& command )
{
	switch(command) {
		case CommandReceiver::Abort:
			post(evAbort);
			break;
		case CommandReceiver::InsertCoin:
			post(evCoin);
			break;
		case CommandReceiver::ReleaseCandy:
			post(evRelease);
			break;
	}
}
