
#include <RrtReactor.h>

#include <iostream>

#include "gen/CandyMachine.h"
#include "gen/IndicatorLamp.h"

#include "InputReader.h"

#include <Windows.h>
#include <tchar.h>


int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;
	using namespace rrt;

	ReactorConfigurator configurator;

	cout << "CANDY MACHINE DEMO" << endl;
	cout << "~~~~~~~~~~~~~~~~~~" << endl;

	IndicatorLamp::Ptr lamp(new IndicatorLamp());
	CandyMachine::Ptr candyMachine(new CandyMachine(lamp));

	InputReader::Ptr inputReader(new InputReader(candyMachine));


	lamp->start();
	candyMachine->start();

	inputReader->start();

	Reactor::run();

	return 0;
}

