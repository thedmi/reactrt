#include "stdafx.h"
#include "InputReader.h"

#include <iostream>
#include <Reactor.h>


InputReader::InputReader( CommandReceiver::Ptr dest ) : dest(dest)
{
}

void InputReader::start()
{
	readerThread = CreateThread(NULL, 1024, (LPTHREAD_START_ROUTINE)&InputReader::threadMain, this, 0, NULL);
}

char InputReader::readChar()
{
	using namespace std;

	cout << "Command: ";
	cout.flush();

	char command;
	char garbage[100];

	cin >> command;
	cin.getline(garbage, 100);

	return command;
}

void InputReader::run()
{
	using namespace std;

	bool keepRunning = true;

	cout << endl;
	cout << "Type <Command><Enter> to execute the specified command." << endl;
	cout << "Available commands are" << endl;
	cout << " c\tInsert coin" << endl;
	cout << " r\tRelease candy" << endl;
	cout << " a\tAbort and return coins" << endl;
	cout << " q\tQuit" << endl;
	cout << endl;

	while(keepRunning) {
		char c = readChar();
		switch(c) {
			case 'c':
				dest->processCommand(CommandReceiver::InsertCoin);
				break;
			case 'r':
				dest->processCommand(CommandReceiver::ReleaseCandy);
				break;
			case 'a':
				dest->processCommand(CommandReceiver::Abort);
				break;
			case 'q':
				keepRunning = false;
				break;
		}
		Sleep(100);
	}

	rrt::Reactor::stop();
}

DWORD InputReader::threadMain( LPVOID self )
{
	static_cast<InputReader *>(self)->run();
	return 0;
}

