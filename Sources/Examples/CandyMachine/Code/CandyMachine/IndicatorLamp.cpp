
#include "stdafx.h"

#include "gen/IndicatorLamp.h"

#include <iostream>



IndicatorLamp::IndicatorLamp() 
{
}

IndicatorLamp::~IndicatorLamp() 
{
}


void IndicatorLamp::setOn()
{
	using namespace std;

	cout << "{ Indicator Lamp On }" << endl;
}

void IndicatorLamp::setOff()
{
	using namespace std;

	cout << "{ Indicator Lamp Off }" << endl;
}

void IndicatorLamp::off()
{
	post(evOff);
}

void IndicatorLamp::on()
{
	post(evOn);
}
