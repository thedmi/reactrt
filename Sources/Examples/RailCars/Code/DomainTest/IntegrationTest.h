#pragma once

#include <boost/smart_ptr.hpp>
#include <gmock/gmock.h>

#include <ReactRtAsyncTest.h>

namespace testing 
{
	class IntegrationTest : public ReactRtAsyncTest
	{
	public:
		IntegrationTest() : ReactRtAsyncTest(10000) {}
	};
}