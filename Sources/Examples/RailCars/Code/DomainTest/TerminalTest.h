#pragma once

#include <boost/smart_ptr.hpp>
#include <gmock/gmock.h>

#include <ReactRtSyncTest.h>

namespace testing 
{
	class LoggerMock;


	class TerminalTest : public ReactRtSyncTest
	{
	protected:
		virtual void SetUp();

		boost::shared_ptr<LoggerMock> logger;

	};
}