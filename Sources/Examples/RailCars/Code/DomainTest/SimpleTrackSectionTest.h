#pragma once

#include <boost/smart_ptr.hpp>
#include <gmock/gmock.h>

#include <ReactRtSyncTest.h>

namespace testing 
{
	class LoggerMock;


	class SimpleTrackSectionTest : public ReactRtSyncTest
	{
	protected:
		virtual void SetUp();

		boost::shared_ptr<LoggerMock> logger;

	};
}