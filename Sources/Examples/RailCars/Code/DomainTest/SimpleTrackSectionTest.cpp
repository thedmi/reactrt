#include "SimpleTrackSectionTest.h"

#include "gen/testing/LoggerMock.h"
#include "gen/testing/TrackUserMock.h"
#include "gen/testing/TrackObserverMock.h"

#include "gen/SimpleTrackSection.h"

using namespace testing;
using namespace boost;


void testing::SimpleTrackSectionTest::SetUp()
{
	logger.reset(new NiceMock<LoggerMock>());
}


TEST_F(SimpleTrackSectionTest, SingleCar)
{
	TrackUserMock::Ptr car(new TrackUserMock());

	TrackObserverMock::Ptr observer(new TrackObserverMock());

	SimpleTrackSection::Ptr section(new SimpleTrackSection(logger));
	section->setTrackObserver(observer);
	section->start();

	EXPECT_CALL(*car, getName() ).Times(AnyNumber());
	EXPECT_CALL(*car, notifyNextSectionClear() );

	{
		InSequence;
		EXPECT_CALL(*observer, entered(Eq(car)));
		EXPECT_CALL(*observer, left(Eq(car)));
	}

	section->requestAccess(car);
	section->enter(car);
	section->leave(car);
}


TEST_F(SimpleTrackSectionTest, WaitingCars)
{
	TrackUserMock::Ptr car1(new TrackUserMock());
	TrackUserMock::Ptr car2(new TrackUserMock());
	TrackUserMock::Ptr car3(new TrackUserMock());

	registerForIncrementalVerification(car1);
	registerForIncrementalVerification(car2);
	registerForIncrementalVerification(car3);

	SimpleTrackSection::Ptr section(new SimpleTrackSection(logger));
	section->start();

	// Three cars request access, the first one can enter

	EXPECT_CALL(*car1, notifyNextSectionClear());

	EXPECT_CALL(*car2, notifyNextSectionClear()).Times(Exactly(0));
	EXPECT_CALL(*car3, notifyNextSectionClear()).Times(Exactly(0));

	section->requestAccess(car1);
	section->requestAccess(car2);

	section->enter(car1);

	section->requestAccess(car3);

	verifyAndClearIncrementalMockExpectations();

	// As soon as one leaves the section, the next in the waiting list 
	// must be given permission to enter

	EXPECT_CALL(*car2, notifyNextSectionClear());
	EXPECT_CALL(*car3, notifyNextSectionClear()).Times(Exactly(0));

	section->leave(car1);
	section->enter(car2);

	verifyAndClearIncrementalMockExpectations();

	// Finally, also the last one will enter

	EXPECT_CALL(*car3, notifyNextSectionClear());

	section->leave(car2);
	section->enter(car3);
}

