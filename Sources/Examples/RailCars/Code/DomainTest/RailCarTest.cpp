#include "RailCarTest.h"

#include <boost/foreach.hpp>

#include "gen/testing/LoggerMock.h"
#include "gen/testing/TrackSectionMock.h"

#include "gen/RailCar.h"

#include <TimeEventSchedulerMock.h>
#include <SynchronousFakeThread.h>
#include <ReactorRegistry.h>

using namespace testing;
using namespace rrt;
using namespace boost;



TEST_F(RailCarTest, AutoPilotForward)
{
	TimeEventSchedulerMock::Ptr scheduler = getTimeEventSchedulerMock();

	LoggerMock::Ptr logger(new NiceMock<LoggerMock>());

	TrackSectionMock::Ptr section1(new TrackSectionMock());
	TrackSectionMock::Ptr section2(new TrackSectionMock());
	TrackSectionMock::Ptr section3(new TrackSectionMock());
	TrackSectionMock::Ptr section4(new TrackSectionMock());

	registerForIncrementalVerification(scheduler);
	registerForIncrementalVerification(section1);
	registerForIncrementalVerification(section2);
	registerForIncrementalVerification(section3);
	registerForIncrementalVerification(section4);

	ON_CALL(*section1, getNext()).WillByDefault(Return(section2));
	ON_CALL(*section2, getNext()).WillByDefault(Return(section3));
	ON_CALL(*section3, getNext()).WillByDefault(Return(section4));

	RailCar::Ptr railCar(new RailCar(section1, "TestRailCar", 1000, logger));


	// Leave the depot and request access to next section

	EXPECT_CALL(*section1, getNext()).Times(AtLeast(1));

	{
		InSequence;
		EXPECT_CALL(*section1, requestAccess(_));
		EXPECT_CALL(*section1, enter(_));
		EXPECT_CALL(*scheduler, scheduleTimeout(_, _, _));
		EXPECT_CALL(*section2, requestAccess(_));
	}

	railCar->start();
	railCar->startAutoPilot(true);
	railCar->notifyNextSectionClear();

	verifyAndClearIncrementalMockExpectations();

	// Proceed to section 2

	EXPECT_CALL(*section1, getNext()).Times(AnyNumber());
	EXPECT_CALL(*section2, getNext()).Times(AtLeast(1)); 

	{
		InSequence;
		EXPECT_CALL(*section1, leave(_));
		EXPECT_CALL(*section2, enter(_));
		EXPECT_CALL(*section3, requestAccess(_));
	}

	railCar->notifyNextSectionClear();

	verifyAndClearIncrementalMockExpectations();

	// Proceed to section 3

	EXPECT_CALL(*section2, getNext()).Times(AnyNumber()); 
	EXPECT_CALL(*section3, getNext()).Times(AtLeast(1));

	{
		InSequence;
		EXPECT_CALL(*section2, leave(_));
		EXPECT_CALL(*section3, enter(_));
		EXPECT_CALL(*section4, requestAccess(_));
	}

	railCar->notifyNextSectionClear();

	verifyAndClearIncrementalMockExpectations();
	
}
