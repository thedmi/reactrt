#include "TerminalTest.h"

#include "gen/testing/LoggerMock.h"
#include "gen/testing/TrackUserMock.h"
#include "gen/testing/TrackObserverMock.h"

#include "gen/Terminal.h"

using namespace testing;
using namespace boost;


void testing::TerminalTest::SetUp()
{
	logger.reset(new NiceMock<LoggerMock>());
}


TEST_F(TerminalTest, SingleCar)
{
	TrackUserMock::Ptr car(new TrackUserMock());

	TrackObserverMock::Ptr observer(new TrackObserverMock());

	Terminal::Ptr terminal(new Terminal(1, logger));
	terminal->setTrackObserver(observer);
	terminal->start();

	EXPECT_CALL(*car, getName() ).Times(AnyNumber());
	EXPECT_CALL(*car, notifyNextSectionClear() );

	{
		InSequence;
		EXPECT_CALL(*observer, entered(Eq(car)));
		EXPECT_CALL(*observer, left(Eq(car)));
	}

	terminal->requestAccess(car);
	terminal->enter(car);
	terminal->leave(car);
}


TEST_F(TerminalTest, TerminalFull) 
{
	TrackUserMock::Ptr car1(new NiceMock<TrackUserMock>());
	TrackUserMock::Ptr car2(new NiceMock<TrackUserMock>());
	TrackUserMock::Ptr car3(new NiceMock<TrackUserMock>());
	TrackUserMock::Ptr car4(new NiceMock<TrackUserMock>());
	TrackUserMock::Ptr car5(new NiceMock<TrackUserMock>());

	registerForIncrementalVerification(car1);
	registerForIncrementalVerification(car2);
	registerForIncrementalVerification(car3);
	registerForIncrementalVerification(car4);
	registerForIncrementalVerification(car5);

	Terminal::Ptr terminal(new Terminal(0, logger));
	terminal->start();

	// Let three cars enter, keep the others in the waiting list

	{
		InSequence;
		EXPECT_CALL(*car1, notifyNextSectionClear());
		EXPECT_CALL(*car2, notifyNextSectionClear());
		EXPECT_CALL(*car3, notifyNextSectionClear());
	}

	EXPECT_CALL(*car4, notifyNextSectionClear()).Times(Exactly(0));
	EXPECT_CALL(*car5, notifyNextSectionClear()).Times(Exactly(0));

	terminal->requestAccess(car1);
	terminal->requestAccess(car2);

	terminal->enter(car1);

	terminal->requestAccess(car3);
	terminal->requestAccess(car4);
	terminal->requestAccess(car5);

	terminal->enter(car2);
	terminal->enter(car3);

	verifyAndClearIncrementalMockExpectations();

	// As soon as one leaves the terminal, the next in the waiting list 
	// must be given permission to enter
	
	EXPECT_CALL(*car4, notifyNextSectionClear());
	EXPECT_CALL(*car5, notifyNextSectionClear()).Times(Exactly(0));

	terminal->leave(car2);
	terminal->enter(car4);

	verifyAndClearIncrementalMockExpectations();

	// Finally, also the last one will enter

	EXPECT_CALL(*car5, notifyNextSectionClear());

	terminal->leave(car1);
	terminal->enter(car5);
}