#include "IntegrationTest.h"

#include "gen/testing/LoggerMock.h"
#include "gen/testing/TrackObserverMock.h"

#include "gen/Terminal.h"
#include "gen/SimpleTrackSection.h"
#include "gen/RailCar.h"

#include <Reactor.h>


using namespace testing;
using namespace rrt;
using namespace boost;



TEST_F(IntegrationTest, Test1)
{
	LoggerMock::Ptr logger(new NiceMock<LoggerMock>());

	TrackObserverMock::Ptr terminal1Observer(new TrackObserverMock());
	TrackObserverMock::Ptr section1Observer(new TrackObserverMock());
	TrackObserverMock::Ptr terminal2Observer(new TrackObserverMock());
	TrackObserverMock::Ptr section2Observer(new TrackObserverMock());

	Terminal::Ptr terminal1(new Terminal(1, logger));
	Terminal::Ptr terminal2(new Terminal(2, logger));

	SimpleTrackSection::Ptr section1(new SimpleTrackSection(logger));
	SimpleTrackSection::Ptr section2(new SimpleTrackSection(logger));

	terminal1->setNextSection(section1);
	section1->setNextSection(terminal2);
	terminal2->setNextSection(section2);
	section2->setNextSection(terminal1);

	section1->setPreviousSection(terminal1);
	terminal2->setPreviousSection(section1);
	section2->setPreviousSection(terminal2);
	terminal1->setPreviousSection(section2);

	terminal1->setTrackObserver(terminal1Observer);
	terminal2->setTrackObserver(terminal2Observer);
	section1->setTrackObserver(section1Observer);
	section2->setTrackObserver(section2Observer);

	RailCar::Ptr car(new RailCar(terminal1, "TestRailCar 1", 100, logger));

	terminal1->start();
	terminal2->start();
	section1->start();
	section2->start();

	EXPECT_CALL(*terminal1Observer, entered(Eq(car)))
		.WillOnce(Return())															// at the beginning
		.WillOnce(InvokeWithoutArgs(this, &ReactRtAsyncTest::signalCompletion));	// at the end

	{
		InSequence;
		EXPECT_CALL(*terminal1Observer, left(Eq(car)));
		EXPECT_CALL(*section1Observer, entered(Eq(car)));
		EXPECT_CALL(*section1Observer, left(Eq(car)));
		EXPECT_CALL(*terminal2Observer, entered(Eq(car)));
		EXPECT_CALL(*terminal2Observer, left(Eq(car)));
		EXPECT_CALL(*section2Observer, entered(Eq(car)));
		EXPECT_CALL(*section2Observer, left(Eq(car)));
	}

	car->start();
	car->startAutoPilot(true);
	
	waitForCompletion();
}