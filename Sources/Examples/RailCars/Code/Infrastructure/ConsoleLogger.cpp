#include "gen/ConsoleLogger.h"

#include <iostream>

#include <ReactorRegistry.h>
#include <ITimestampProvider.h>

ConsoleLogger::ConsoleLogger()
{

}

ConsoleLogger::~ConsoleLogger()
{

}

void ConsoleLogger::log( const std::string& message )
{
	LogEvent::Ptr ev(new LogEvent());
	ev->message = message;
	post(ev);
}

void ConsoleLogger::handleLog( const std::string& message )
{
	using namespace std;
	using namespace rrt;

	cout << ReactorRegistry::Instance().getTimestampProvider()->unixTimestamp() << " " << message << endl;
}
