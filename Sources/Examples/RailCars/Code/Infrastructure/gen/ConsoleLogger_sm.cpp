#include "ConsoleLogger.h"

#include <gen/StateMachineObserver.h>
#include <gen/ObservatoryRegistry.h>
#include <sstream>

using namespace rrt;

void ConsoleLogger::dispatch(ConstEventPtr ev) {
	observatoryNotifyDispatchStarted(ev);
	(*currentState)(this, ev);
	observatoryNotifyDispatchCompleted();
}

void ConsoleLogger::init() {
	observatoryConfigureStateMachine();
	Context context = this;

	currentState = Sm::Idle;
	observatoryNotifyPriorityAnnounce();
	observatoryNotifyInitialized();
}

inline void ConsoleLogger::transition_Idle_evLog(LogEvent::ConstPtr ev) {
	handleLog(ev->message);
}

void ConsoleLogger::Sm::Idle(Context context, ConstEventPtr ev) {
	switch (ev->sig) {
		case sigLog:
			// Internal transition of Idle
			context->transition_Idle_evLog(boost::shared_polymorphic_downcast<const LogEvent>(ev));
			break;
	}
}

/* -- ReactRT Observatory instrumentation begin -- */

void ConsoleLogger::observatoryConfigureStateMachine() {
	using namespace std;

	observatoryObserver = ObservatoryRegistry::Instance().getStateMachineObserver();

	string name = getName();
	if (name.find_first_of('\'') != string::npos) {
		name = "(invalid object name: the name may not contain apostrophes)";
	}

	stringstream s;
	s << "ConsoleLogger" << " ( " << hex << reinterpret_cast<unsigned int> (this) << " '" << name << "' )";
	observatoryStateMachineName = s.str();
}

inline void ConsoleLogger::observatoryNotifyDispatchStarted(ConstEventPtr ev) const {
	ObservatoryObserverPtr o = observatoryObserver.lock();
	if (o) {
		o->dispatchStarted(observatoryStateMachineName, ev, observatorySignalName(ev));
	}
}

inline void ConsoleLogger::observatoryNotifyDispatchCompleted() const {
	ObservatoryObserverPtr o = observatoryObserver.lock();
	if (o) {
		o->dispatchCompleted(observatoryStateMachineName);
	}
}

inline void ConsoleLogger::observatoryNotifyInitialized() const {
	ObservatoryObserverPtr o = observatoryObserver.lock();
	if (o) {
		o->initialized(observatoryStateMachineName, "Sm::Idle");
	}
}

inline void ConsoleLogger::observatoryNotifyPriorityAnnounce() const {
	ObservatoryObserverPtr o = observatoryObserver.lock();
	if (o) {
		o->priorityAnnounce(observatoryStateMachineName, getUrgency());
	}
}

inline void ConsoleLogger::observatoryNotifyStateChanged(const char * newState) const {
	ObservatoryObserverPtr o = observatoryObserver.lock();
	if (o) {
		o->stateChanged(observatoryStateMachineName, newState);
	}
}

inline void ConsoleLogger::observatoryNotifyTimerArmed(const char * timeout) const {
	ObservatoryObserverPtr o = observatoryObserver.lock();
	if (o) {
		o->timerArmed(observatoryStateMachineName, timeout);
	}
}

inline void ConsoleLogger::observatoryNotifyTimerDisarmed(const char * timeout) const {
	ObservatoryObserverPtr o = observatoryObserver.lock();
	if (o) {
		o->timerDisarmed(observatoryStateMachineName, timeout);
	}
}

inline const char * ConsoleLogger::observatorySignalName(ConstEventPtr ev) const {
	switch (ev->sig) {
		case sigLog:
			return "sigLog";
	}
	return "";
}

/* -- ReactRT Observatory instrumentation end -- */

