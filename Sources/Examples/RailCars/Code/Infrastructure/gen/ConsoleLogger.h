#pragma once
/*
 *	ReactRT 1.0
 *
 *	This file is generated and should not be modified. Any changes made
 *	to this file will be overwritten on regeneration.
 */

#include <boost/smart_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <Reactive.h>
#include <TimeEvent.h>
#include <string>
#include "Logger.h"

class StateMachineObserver;

class ConsoleLogger : public Logger, public rrt::Reactive, public boost::enable_shared_from_this<ConsoleLogger> {

public:
	typedef boost::shared_ptr<ConsoleLogger> Ptr;
	typedef boost::shared_ptr<const ConsoleLogger> ConstPtr;

public:
	enum Signals {
		sigLog
	};

	// Custom event types

	class LogEvent : public rrt::Event {
	public:

		LogEvent() :
			rrt::Event(sigLog) {
		}

		virtual ~LogEvent() {
		}

		std::string message;

		typedef boost::shared_ptr<LogEvent> Ptr;
		typedef boost::shared_ptr<const LogEvent> ConstPtr;
	};

	// Static events


protected:

	// Event dispatcher method
	virtual void dispatch(boost::shared_ptr<const rrt::Event> ev);

	// Method that performs the initial transition
	virtual void init();

public:
	ConsoleLogger();

public:
	virtual ~ConsoleLogger();

public:
	// Public methods

	virtual void log(const std::string& message);

private:
	// Private methods

	void handleLog(const std::string& message);

private:
	typedef ConsoleLogger * Context;
	typedef boost::shared_ptr<const rrt::Event> ConstEventPtr;

	// Time events


	// State and initial transition action wrappers


	// Primary transition action wrappers

	void transition_Idle_evLog(LogEvent::ConstPtr ev);

	// Non-primary transition action wrappers (arbitrary names)


	// Guard wrappers (arbitrary names)


	// The state hierarchy with the state handler functions

	struct Sm {

		static void Idle(Context context, ConstEventPtr ev);

	};

	void (*currentState)(Context context, ConstEventPtr ev);

	/* -- ReactRT Observatory instrumentation begin -- */
private:

	typedef boost::shared_ptr<StateMachineObserver> ObservatoryObserverPtr;

	boost::weak_ptr<StateMachineObserver> observatoryObserver;

	std::string observatoryStateMachineName;

	void observatoryConfigureStateMachine();

	void observatoryNotifyDispatchStarted(ConstEventPtr ev) const;
	void observatoryNotifyDispatchCompleted() const;
	void observatoryNotifyInitialized() const;
	void observatoryNotifyPriorityAnnounce() const;
	void observatoryNotifyStateChanged(const char * newState) const;
	void observatoryNotifyTimerArmed(const char * timeout) const;
	void observatoryNotifyTimerDisarmed(const char * timeout) const;

	const char * observatorySignalName(ConstEventPtr sig) const;

	/* -- ReactRT Observatory instrumentation end -- */

};

