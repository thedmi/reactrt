#pragma once
/*
 *	ReactRT 1.0
 *
 *	This file is generated and should not be modified. Any changes made
 *	to this file will be overwritten on regeneration.
 */

#include <boost/smart_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <string>

class Logger : private boost::noncopyable {

public:
	typedef boost::shared_ptr<Logger> Ptr;
	typedef boost::shared_ptr<const Logger> ConstPtr;

protected:
	Logger() {
	}

public:
	virtual ~Logger() {
	}

public:

	virtual void log(const std::string& message) = 0;

};

