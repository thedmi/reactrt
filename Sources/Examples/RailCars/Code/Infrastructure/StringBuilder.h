#pragma once

#include <sstream>

#ifndef S
#define S StringBuilder()
#endif

/*
 *	This class enables stringstream one-liners, it originated from a discussion on comp.lang.c++
 *	see ( http://groups.google.com/group/comp.lang.c++/msg/77e566c11ce93120 ).
 */
class StringBuilder
{
private:
	std::ostringstream os_stream;
public:
	StringBuilder() {}
	StringBuilder( const char* s ): os_stream(s) {}
	StringBuilder( const std::string& s ): os_stream(s) {}

	template<class T> StringBuilder& operator<<(const T& t) {
		os_stream << t;
		return *this;
	}

	StringBuilder& operator<<(std::ostream& (*f) (std::ostream&)) {
		os_stream << f;
		return *this;
	}

	operator std::string() const {
		return os_stream.str();
	}

};