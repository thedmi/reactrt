#include "SectionView.h"

#include <QStandardItemModel>
#include <QStandardItem>

#include "gen/ITrackUser.h"

#include "gen/AbstractTrackSection.h"


SectionView::SectionView(QString name, QWidget *parent)
	: QWidget(parent), name(name)
{
	ui.setupUi(this);

	ui.title->setText(name);

	QObject::connect(this, SIGNAL(removedFromWaitListSignal(QString)), this, SLOT(removedFromWaitList(QString)));
	QObject::connect(this, SIGNAL(addedToWaitListSignal(QString)), this, SLOT(addedToWaitList(QString)));
	QObject::connect(this, SIGNAL(enteredSignal(QString)), this, SLOT(entered(QString)));
	QObject::connect(this, SIGNAL(leftSignal(QString)), this, SLOT(left(QString)));

	waitingListModel = new QStandardItemModel(this);
	parkedListModel = new QStandardItemModel(this);
	ui.waitingList->setModel(waitingListModel);
	ui.parkedList->setModel(parkedListModel);

	adapter.reset(new TrackObserverAdapter(this));

}

SectionView::~SectionView()
{

}

void SectionView::removedFromWaitList( QString carName )
{
	waitingSet.remove(carName);
	updateWaiting();
}

void SectionView::entered( QString carName )
{
	parkedSet.insert(carName);
	updateParked();
}

void SectionView::addedToWaitList( QString carName )
{
	waitingSet.insert(carName);
	updateWaiting();
}

void SectionView::left( QString carName )
{
	parkedSet.remove(carName);
	updateParked();
}

void SectionView::setDataContext( boost::shared_ptr<AbstractTrackSection> section )
{
	section->setTrackObserver(adapter);
}

QIcon SectionView::railCarIcon( QString carName )
{
	switch(carName.at(carName.length() - 1).toAscii() % 4) {
		case 0:
			return QIcon("car_compact_blue.png");
		case 1:
			return QIcon("car_compact_red.png");
		case 2:
			return QIcon("car_compact_green.png");
		default:
			return QIcon("car_compact_grey.png");
	}
}

void SectionView::updateParked()
{
	parkedListModel->clear();
	for(QSet<QString>::iterator it = parkedSet.begin(); it != parkedSet.end(); it++) {
		parkedListModel->appendRow(new QStandardItem(railCarIcon(*it), *it));
	}
}

void SectionView::updateWaiting()
{
	waitingListModel->clear();
	for(QSet<QString>::iterator it = waitingSet.begin(); it != waitingSet.end(); it++) {
		waitingListModel->appendRow(new QStandardItem(railCarIcon(*it), *it));
	}
}

void SectionView::setTerminalBackground()
{
	ui.parkedList->setStyleSheet("background-color: rgb(211, 227, 255)");
}

void SectionView::setSimpleSectionBackground()
{
	ui.parkedList->setStyleSheet("background-color: rgb(255, 225, 221)");
}


void SectionView::TrackObserverAdapter::removedFromWaitList( boost::shared_ptr<const ITrackUser> car )
{
	QString name = QString::fromStdString(car->getName());
	emit parent->removedFromWaitListSignal(name);
}

void SectionView::TrackObserverAdapter::entered( boost::shared_ptr<const ITrackUser> car )
{
	QString name = QString::fromStdString(car->getName());
	emit parent->enteredSignal(name);
}

void SectionView::TrackObserverAdapter::addedToWaitList( boost::shared_ptr<const ITrackUser> car )
{
	QString name = QString::fromStdString(car->getName());
	emit parent->addedToWaitListSignal(name);
}

void SectionView::TrackObserverAdapter::left( boost::shared_ptr<const ITrackUser> car )
{
	QString name = QString::fromStdString(car->getName());
	emit parent->leftSignal(name);
}

SectionView::TrackObserverAdapter::TrackObserverAdapter( SectionView * parent ) : parent(parent)
{
}
