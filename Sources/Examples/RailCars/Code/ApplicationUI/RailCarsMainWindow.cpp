#include "RailCarsMainWindow.h"

#include "SectionView.h"

#include "gen/Terminal.h"
#include "gen/SimpleTrackSection.h"

#include <QSvgWidget>

RailCarsMainWindow::RailCarsMainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	terminalContainers[0] = ui.terminalContainer0;
	terminalContainers[1] = ui.terminalContainer1;
	terminalContainers[2] = ui.terminalContainer2;
	terminalContainers[3] = ui.terminalContainer3;

	simpleSectionContainers[0] = ui.simpleSectionContainer0;
	simpleSectionContainers[1] = ui.simpleSectionContainer1;
	simpleSectionContainers[2] = ui.simpleSectionContainer2;
	simpleSectionContainers[3] = ui.simpleSectionContainer3;

	QSvgWidget * trackCourse = new QSvgWidget("course.svg", this);
	trackCourse->setGeometry(340, 220, 300, 300);
}

RailCarsMainWindow::~RailCarsMainWindow()
{

}

void RailCarsMainWindow::createTerminalView( boost::shared_ptr<Terminal> terminal, int terminalId )
{
	QString name = "Terminal " + QString::number(terminalId);

	SectionView * sv = new SectionView(name, terminalContainers[terminalId]);
	sv->setDataContext(terminal);
	sv->setTerminalBackground();
}

void RailCarsMainWindow::createSimpleSectionView( boost::shared_ptr<SimpleTrackSection> section, int sectionId )
{
	QString name = "Section " + QString::number(sectionId);

	SectionView * sv = new SectionView(name, simpleSectionContainers[sectionId]);
	sv->setDataContext(section);
	sv->setSimpleSectionBackground();
}
