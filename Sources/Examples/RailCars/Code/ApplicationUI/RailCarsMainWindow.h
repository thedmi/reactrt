#ifndef RAILCARSMAINWINDOW_H
#define RAILCARSMAINWINDOW_H

#include <QMainWindow>
#include "ui_RailCarsMainWindow.h"

#include <boost/smart_ptr.hpp>


class Terminal;
class SimpleTrackSection;


class RailCarsMainWindow : public QMainWindow
{
	Q_OBJECT


public:

	RailCarsMainWindow(QWidget *parent = 0);
	~RailCarsMainWindow();

	void createTerminalView(boost::shared_ptr<Terminal> terminal, int terminalId );
	void createSimpleSectionView(boost::shared_ptr<SimpleTrackSection> section, int sectionId);

private:

	Ui::RailCarsMainWindow ui;

	QFrame * terminalContainers[4];
	QFrame * simpleSectionContainers[4];
};

#endif // RAILCARSMAINWINDOW_H
