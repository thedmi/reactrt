#ifndef TERMINALVIEW_H
#define TERMINALVIEW_H

#include <QWidget>
#include "ui_SectionView.h"

#include <QSet>

#include <boost/smart_ptr.hpp>

#include "gen/ITrackObserver.h"


class QStandardItemModel;

class AbstractTrackSection;


class SectionView : public QWidget
{
	Q_OBJECT

public:
	SectionView(QString name, QWidget *parent = 0);
	~SectionView();

	void setDataContext(boost::shared_ptr<AbstractTrackSection> section);

	void setTerminalBackground();
	void setSimpleSectionBackground();

private:
	
	Ui::TerminalView ui;

	QString name;


public slots:

	void removedFromWaitList( QString carName );

	void entered( QString carName );

	void addedToWaitList( QString carName );

	void left( QString carName );

signals:
	void removedFromWaitListSignal(QString carName);
	void addedToWaitListSignal(QString carName);
	void enteredSignal(QString carName);
	void leftSignal(QString carName);


private:

	void updateParked();
	void updateWaiting();

	QIcon railCarIcon(QString carName);


private:
	class TrackObserverAdapter : public ITrackObserver 
	{

	public:
		TrackObserverAdapter(SectionView * parent);

		virtual void removedFromWaitList( boost::shared_ptr<const ITrackUser> car );

		virtual void entered( boost::shared_ptr<const ITrackUser> car );

		virtual void addedToWaitList( boost::shared_ptr<const ITrackUser> car );

		virtual void left( boost::shared_ptr<const ITrackUser> car );

	private:
		SectionView * parent;



	};

	boost::shared_ptr<TrackObserverAdapter> adapter;

	QStandardItemModel * waitingListModel;
	QStandardItemModel * parkedListModel;

	QSet<QString> parkedSet;
	QSet<QString> waitingSet;
};

#endif // TERMINALVIEW_H
