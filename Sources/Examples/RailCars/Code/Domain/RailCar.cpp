#include "gen/RailCar.h"

#include "gen/ITrackSection.h"

#include "gen/ILogger.h"

#include <StringBuilder.h>


RailCar::RailCar( boost::shared_ptr<ITrackSection> initialTrackSection, const std::string& name, const int slowness, boost::shared_ptr<ILogger> logger ):
	currentSection(initialTrackSection), name(name), slowness(slowness), logger(logger)
{
	logger->log(S << name << ": Starting, slowness is " << slowness);
}

RailCar::~RailCar()
{

}

void RailCar::requestNextSectionAccess()
{
	logger->log(S << name << ": Requesting access to next section");
	getFollowingSection()->requestAccess(shared_from_this());
}

void RailCar::notifyNextSectionClear()
{
	post(evNextSectionAccessGranted);
}

void RailCar::moveToNextSection()
{
	logger->log(S << name << ": Moving to next section");
	currentSection->leave(shared_from_this());
	currentSection = getFollowingSection();
	currentSection->enter(shared_from_this());
}

void RailCar::startAutoPilot(bool goForwards)
{
	if (goForwards) {
		post(evStartAutoPilotForwards);
	} else {
		post(evStartAutoPilotBackwards);
	}
}

boost::shared_ptr<ITrackSection> RailCar::getFollowingSection() const
{
	if (isDirectionForward) {
		return currentSection->getNext();
	} else {
		return currentSection->getPrevious();
	}
}

std::string RailCar::getName() const
{
	return name;
}
