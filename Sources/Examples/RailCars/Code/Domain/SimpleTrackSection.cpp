#include "gen/SimpleTrackSection.h"

#include "gen/ITrackUser.h"

#include "gen/ILogger.h"
#include "StringBuilder.h"

#include "gen/ITrackObserver.h"

#include <assert.h>


using namespace std;



SimpleTrackSection::SimpleTrackSection( boost::shared_ptr<ILogger> logger ) : logger(logger)
{

}

SimpleTrackSection::~SimpleTrackSection()
{

}

void SimpleTrackSection::enter( boost::shared_ptr<ITrackUser> car )
{
	EnterEvent::Ptr ev(new EnterEvent());
	ev->car = car;

	post(ev);
}

void SimpleTrackSection::leave( boost::shared_ptr<ITrackUser> car )
{
	LeaveEvent::Ptr ev(new LeaveEvent());
	ev->car = car;

	post(ev);
}

void SimpleTrackSection::readyForTransfer( boost::shared_ptr<ITrackUser> car )
{
	assert(false);
}

void SimpleTrackSection::requestAccess( boost::shared_ptr<ITrackUser> car )
{
	RequestAccessEvent::Ptr ev(new RequestAccessEvent());
	ev->car = car;

	post(ev);
}

void SimpleTrackSection::grantAccessTo( boost::shared_ptr<ITrackUser> car )
{
	car->notifyNextSectionClear();
}

void SimpleTrackSection::grantAccessToNextWaiting()
{
	ITrackUser::Ptr nextCar = waitingCars.front();
	waitingCars.pop_front();

	grantAccessTo(nextCar);

	if (ITrackObserver::Ptr o = trackObserver.lock()) {
		o->removedFromWaitList(nextCar);
	}
}

boost::shared_ptr<AbstractTrackSection> SimpleTrackSection::getSharedFromThis()
{
	return shared_from_this();
}

void SimpleTrackSection::carEntered( boost::shared_ptr<ITrackUser> car )
{
	carOnTrack = car;

	if (ITrackObserver::Ptr o = trackObserver.lock()) {
		o->entered(car);
	}
}

void SimpleTrackSection::carLeft( boost::shared_ptr<ITrackUser> car )
{
	assert(carOnTrack == car);
	carOnTrack.reset();

	if (ITrackObserver::Ptr o = trackObserver.lock()) {
		o->left(car);
	}
}

void SimpleTrackSection::addToWaitList( boost::shared_ptr<ITrackUser> car )
{
	waitingCars.push_back(car);

	if (ITrackObserver::Ptr o = trackObserver.lock()) {
		o->addedToWaitList(car);
	}
}

