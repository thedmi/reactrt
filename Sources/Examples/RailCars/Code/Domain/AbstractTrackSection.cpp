#include "gen/AbstractTrackSection.h"



AbstractTrackSection::AbstractTrackSection()
{
	
}

AbstractTrackSection::~AbstractTrackSection()
{

}

void AbstractTrackSection::setPreviousSection( boost::shared_ptr<AbstractTrackSection> section )
{
	previousSection = section;
}

void AbstractTrackSection::setNextSection( boost::shared_ptr<AbstractTrackSection> section )
{
	nextSection = section;
}

void AbstractTrackSection::releaseAdjacentSections()
{
	releaseAdjacentSections(getSharedFromThis());
}

void AbstractTrackSection::releaseAdjacentSections( boost::shared_ptr<AbstractTrackSection> releaseOrigin )
{
	if(nextSection != releaseOrigin) {
		nextSection->releaseAdjacentSections(releaseOrigin);
	}
	if(previousSection != releaseOrigin) {
		previousSection->releaseAdjacentSections(releaseOrigin);
	}

	nextSection.reset();
	previousSection.reset();
}

boost::shared_ptr<ITrackSection> AbstractTrackSection::getNext() const
{
	return nextSection;
}

boost::shared_ptr<ITrackSection> AbstractTrackSection::getPrevious() const
{
	return previousSection;
}
