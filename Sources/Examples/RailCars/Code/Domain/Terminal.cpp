#include "gen/Terminal.h"

#include "gen/SimpleTrackSection.h"
#include "gen/ITrackUser.h"
#include "gen/ITrackObserver.h"

#include "gen/ILogger.h"
#include "StringBuilder.h"

#include <assert.h>


using namespace std;


const unsigned int Terminal::CAPACITY = 3;



Terminal::Terminal( const int terminalId, boost::shared_ptr<ILogger> logger ) : terminalId(terminalId), logger(logger)
{

}

Terminal::~Terminal()
{

}

void Terminal::enter( boost::shared_ptr<ITrackUser> car )
{
	EnterEvent::Ptr ev(new EnterEvent());
	ev->car = car;

	post(ev);
}

void Terminal::leave( boost::shared_ptr<ITrackUser> car )
{
	LeaveEvent::Ptr ev(new LeaveEvent());
	ev->car = car;

	post(ev);
}

void Terminal::readyForTransfer( boost::shared_ptr<ITrackUser> car )
{
	ReadyForTransferEvent::Ptr ev(new ReadyForTransferEvent());
	ev->car = car;

	post(ev);
}

void Terminal::requestAccess( boost::shared_ptr<ITrackUser> car )
{
	RequestAccessEvent::Ptr ev(new RequestAccessEvent());
	ev->car = car;

	post(ev);
}

void Terminal::handleLeave( boost::shared_ptr<ITrackUser> car )
{
	logger->log(S << getName() << ": Car " << car->getName() << " leaves");
	carsInTerminal.erase(car);

	if (ITrackObserver::Ptr o = trackObserver.lock()) {
		o->left(car);
	}
}

void Terminal::handleReadyForTransfer( boost::shared_ptr<ITrackUser> car )
{

}

std::string Terminal::getName() const {
	std::stringstream s;
	s << "Terminal " << terminalId;
	return s.str();
}

void Terminal::grantAccessTo( boost::shared_ptr<ITrackUser> car )
{
	logger->log(S << getName() << ": Access granted to " << car->getName());
	car->notifyNextSectionClear();
}

void Terminal::park( boost::shared_ptr<ITrackUser> car )
{
	logger->log(S << getName() << ": Car " << car->getName() << " is now parked");
	carsInTerminal.insert(car);

	if (ITrackObserver::Ptr o = trackObserver.lock()) {
		o->entered(car);
	}
}

void Terminal::grantAccessToNextWaiting()
{
	assert(waitingCars.size() > 0);

	ITrackUser::Ptr nextCar = waitingCars.front();
	waitingCars.pop_front();

	grantAccessTo(nextCar);

	if (ITrackObserver::Ptr o = trackObserver.lock()) {
		o->removedFromWaitList(nextCar);
	}
}

void Terminal::addToWaitList( boost::shared_ptr<ITrackUser> car )
{
	logger->log(S << getName() << ": Car " << car->getName() << " added to wait list");
	waitingCars.push_back(car);

	if (ITrackObserver::Ptr o = trackObserver.lock()) {
		o->addedToWaitList(car);
	}
}

void Terminal::crash()
{
	logger->log(S << "Crash in terminal " << terminalId);
}

boost::shared_ptr<AbstractTrackSection> Terminal::getSharedFromThis()
{
	return shared_from_this();
}
