
#include <tchar.h>


#include <boost/smart_ptr.hpp>

#include "gen/RailCar.h"
#include "gen/SimpleTrackSection.h"
#include "gen/Terminal.h"
#include "gen/ConsoleLogger.h"

#include <RrtReactor.h>
#include <gen/Observatory.h>


#include <sstream>
#include <iostream>


#include <Windows.h>


int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;
	using namespace rrt;
	
	ReactorConfigurator configurator;
	Observatory observatory;

	//SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
	//SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_IDLE);

	IThread::Ptr loggerThread = ReactorRegistry::Instance().getReactorFactory()->createThread(0, "logger");
	IThread::Ptr simpleSectionThread = ReactorRegistry::Instance().getReactorFactory()->createThread(150, "simpleSection");
	IThread::Ptr terminalThread = ReactorRegistry::Instance().getReactorFactory()->createThread(200, "terminal");
	IThread::Ptr railCarThread = ReactorRegistry::Instance().getReactorFactory()->createThread(250, "railCar");
	
	ConsoleLogger::Ptr logger(new ConsoleLogger());
	logger->start(loggerThread);
	
	const int numberOfTerminals = 4;

	Terminal::Ptr terminals[numberOfTerminals];
	SimpleTrackSection::Ptr simpleTrackSections[numberOfTerminals];
	
	for(int i = 0; i < numberOfTerminals; i++) {
		terminals[i].reset(new Terminal(i, logger));
		simpleTrackSections[i].reset(new SimpleTrackSection(logger));
	}

	// Create a circle of terminals with simple sections in between:
	//
	//  T1 -- T2
	//  |      |
	//  T4 -- T3 

	for(int i = 0; i < numberOfTerminals; i++) {

		int following = (i + 1) % numberOfTerminals;

		terminals[i]->setNextSection(simpleTrackSections[i]);
		simpleTrackSections[i]->setNextSection(terminals[following]);

		terminals[following]->setPreviousSection(simpleTrackSections[i]);
		simpleTrackSections[i]->setPreviousSection(terminals[i]);
	}

	for(int i = 0; i < numberOfTerminals; i++) {
		terminals[i]->start(terminalThread);
		simpleTrackSections[i]->start(simpleSectionThread);
	}
	

	const int numberOfRailCars = 4;

	RailCar::Ptr railCars[numberOfRailCars];

	for(int i = 0; i < numberOfRailCars; i++) {

		std::stringstream railCarName;
		railCarName << "RailCar " << (char)(i + 65);

		railCars[i].reset(new RailCar(terminals[i], railCarName.str(), 1000 + i * 100, logger));
		railCars[i]->start(railCarThread);

		railCars[i]->startAutoPilot(i % 2 == 0);
	}

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);

	Reactor::run();   
	

}

